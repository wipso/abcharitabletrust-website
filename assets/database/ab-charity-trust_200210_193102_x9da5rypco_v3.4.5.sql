-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: aoSvDcEU
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-1~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assetindexdata`
--

DROP TABLE IF EXISTS `assetindexdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  KEY `assetindexdata_volumeId_idx` (`volumeId`),
  CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assets_filename_folderId_idx` (`filename`,`folderId`),
  KEY `assets_folderId_idx` (`folderId`),
  KEY `assets_volumeId_idx` (`volumeId`),
  CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assettransformindex`
--

DROP TABLE IF EXISTS `assettransformindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assettransforms`
--

DROP TABLE IF EXISTS `assettransforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_idx` (`groupId`),
  KEY `categories_parentId_fk` (`parentId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `categories` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categorygroups`
--

DROP TABLE IF EXISTS `categorygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categorygroups_name_idx` (`name`),
  KEY `categorygroups_handle_idx` (`handle`),
  KEY `categorygroups_structureId_idx` (`structureId`),
  KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `categorygroups_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categorygroups_sites`
--

DROP TABLE IF EXISTS `categorygroups_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  KEY `categorygroups_sites_siteId_idx` (`siteId`),
  CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_noFollow` tinyint(1) DEFAULT NULL,
  `field_seoMetaDescription` text,
  `field_seoMetaTitle` text,
  `field_tagManagerJavascript` text,
  `field_tagManagerNoscript` text,
  `field_sharingDescription` text,
  `field_sharingTitle` text,
  `field_copy` text,
  `field_role` text,
  `field_staffCopy` text,
  `field_recipientOrganisation` text,
  `field_description` text,
  `field_priorityArea` text,
  `field_amountAwarded` text,
  `field_priorities` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `content_siteId_idx` (`siteId`),
  KEY `content_title_idx` (`title`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `craftidtokens`
--

DROP TABLE IF EXISTS `craftidtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craftidtokens_userId_fk` (`userId`),
  CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deprecationerrors`
--

DROP TABLE IF EXISTS `deprecationerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dolphiq_redirects`
--

DROP TABLE IF EXISTS `dolphiq_redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dolphiq_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceUrl` varchar(255) DEFAULT NULL,
  `destinationUrl` varchar(255) DEFAULT NULL,
  `statusCode` varchar(255) DEFAULT NULL,
  `hitCount` int(11) unsigned NOT NULL DEFAULT '0',
  `hitAt` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  CONSTRAINT `dolphiq_redirects_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dolphiq_redirects_catch_all_urls`
--

DROP TABLE IF EXISTS `dolphiq_redirects_catch_all_urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dolphiq_redirects_catch_all_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL DEFAULT '',
  `uid` char(36) NOT NULL DEFAULT '0',
  `siteId` int(11) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `hitCount` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `drafts`
--

DROP TABLE IF EXISTS `drafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `drafts_creatorId_fk` (`creatorId`),
  KEY `drafts_sourceId_fk` (`sourceId`),
  CONSTRAINT `drafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `drafts_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elementindexsettings`
--

DROP TABLE IF EXISTS `elementindexsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_dateDeleted_idx` (`dateDeleted`),
  KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  KEY `elements_draftId_fk` (`draftId`),
  KEY `elements_revisionId_fk` (`revisionId`),
  CONSTRAINT `elements_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `drafts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `elements_revisionId_fk` FOREIGN KEY (`revisionId`) REFERENCES `revisions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elements_sites`
--

DROP TABLE IF EXISTS `elements_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `elements_sites_siteId_idx` (`siteId`),
  KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  KEY `elements_sites_enabled_idx` (`enabled`),
  KEY `elements_sites_uri_siteId_idx` (`uri`,`siteId`),
  CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_idx` (`authorId`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  KEY `entries_parentId_fk` (`parentId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `entries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entrytypes`
--

DROP TABLE IF EXISTS `entrytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  KEY `entrytypes_handle_sectionId_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_idx` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `entrytypes_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feedme_feeds`
--

DROP TABLE IF EXISTS `feedme_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedme_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `feedUrl` text NOT NULL,
  `feedType` varchar(255) DEFAULT NULL,
  `primaryElement` varchar(255) DEFAULT NULL,
  `elementType` varchar(255) NOT NULL,
  `elementGroup` text,
  `siteId` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `duplicateHandle` text,
  `paginationNode` text,
  `fieldMapping` text,
  `fieldUnique` text,
  `passkey` varchar(255) NOT NULL,
  `backup` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldgroups`
--

DROP TABLE IF EXISTS `fieldgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldlayoutfields`
--

DROP TABLE IF EXISTS `fieldlayoutfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  KEY `fieldlayoutfields_fieldId_idx` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldlayouts`
--

DROP TABLE IF EXISTS `fieldlayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_dateDeleted_idx` (`dateDeleted`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldlayouttabs`
--

DROP TABLE IF EXISTS `fieldlayouttabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_idx` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_groupId_idx` (`groupId`),
  KEY `fields_context_idx` (`context`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_crm_fields`
--

DROP TABLE IF EXISTS `freeform_crm_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_crm_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_crm_fields_type_idx` (`type`),
  KEY `freeform_crm_fields_integrationId_fk` (`integrationId`),
  CONSTRAINT `freeform_crm_fields_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_export_profiles`
--

DROP TABLE IF EXISTS `freeform_export_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_export_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `limit` int(11) DEFAULT NULL,
  `dateRange` varchar(255) DEFAULT NULL,
  `fields` text NOT NULL,
  `filters` text,
  `statuses` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `freeform_export_profiles_formId_fk` (`formId`),
  CONSTRAINT `freeform_export_profiles_formId_fk` FOREIGN KEY (`formId`) REFERENCES `freeform_forms` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_export_settings`
--

DROP TABLE IF EXISTS `freeform_export_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_export_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `setting` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_export_settings_userId_fk` (`userId`),
  CONSTRAINT `freeform_export_settings_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_fields`
--

DROP TABLE IF EXISTS `freeform_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `instructions` text,
  `metaProperties` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`),
  KEY `freeform_fields_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_forms`
--

DROP TABLE IF EXISTS `freeform_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `handle` varchar(100) NOT NULL,
  `spamBlockCount` int(11) unsigned NOT NULL DEFAULT '0',
  `submissionTitleFormat` varchar(255) NOT NULL,
  `description` text,
  `layoutJson` mediumtext,
  `returnUrl` varchar(255) DEFAULT NULL,
  `defaultStatus` int(11) unsigned DEFAULT NULL,
  `formTemplateId` int(11) unsigned DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `optInDataStorageTargetHash` varchar(20) DEFAULT NULL,
  `limitFormSubmissions` varchar(20) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `extraPostUrl` varchar(255) DEFAULT NULL,
  `extraPostTriggerPhrase` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_integrations`
--

DROP TABLE IF EXISTS `freeform_integrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_integrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `settings` text,
  `forceUpdate` tinyint(1) DEFAULT '0',
  `lastUpdate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`),
  KEY `freeform_integrations_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_integrations_queue`
--

DROP TABLE IF EXISTS `freeform_integrations_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_integrations_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submissionId` int(11) NOT NULL,
  `integrationType` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `fieldHash` varchar(20) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_integrations_queue_status_unq_idx` (`status`),
  KEY `freeform_integrations_queue_submissionId_fk` (`submissionId`),
  CONSTRAINT `freeform_integrations_queue_id_fk` FOREIGN KEY (`id`) REFERENCES `freeform_mailing_list_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_integrations_queue_submissionId_fk` FOREIGN KEY (`submissionId`) REFERENCES `freeform_submissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_mailing_list_fields`
--

DROP TABLE IF EXISTS `freeform_mailing_list_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_mailing_list_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailingListId` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_mailing_list_fields_type_idx` (`type`),
  KEY `freeform_mailing_list_fields_mailingListId_fk` (`mailingListId`),
  CONSTRAINT `freeform_mailing_list_fields_mailingListId_fk` FOREIGN KEY (`mailingListId`) REFERENCES `freeform_mailing_lists` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_mailing_lists`
--

DROP TABLE IF EXISTS `freeform_mailing_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_mailing_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `resourceId` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `memberCount` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_mailing_lists_integrationId_resourceId_unq_idx` (`integrationId`,`resourceId`),
  CONSTRAINT `freeform_mailing_lists_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_notifications`
--

DROP TABLE IF EXISTS `freeform_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` text,
  `fromName` varchar(255) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  `replyToEmail` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `bodyHtml` text,
  `bodyText` text,
  `includeAttachments` tinyint(1) DEFAULT '1',
  `presetAssets` varchar(255) DEFAULT NULL,
  `sortOrder` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_payment_gateway_fields`
--

DROP TABLE IF EXISTS `freeform_payment_gateway_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_payment_gateway_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_payment_gateway_fields_type_idx` (`type`),
  KEY `freeform_payment_gateway_fields_integrationId_fk` (`integrationId`),
  CONSTRAINT `freeform_payment_gateway_fields_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_payments_payments`
--

DROP TABLE IF EXISTS `freeform_payments_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_payments_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `submissionId` int(11) NOT NULL,
  `subscriptionId` int(11) DEFAULT NULL,
  `resourceId` varchar(50) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `last4` smallint(6) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `metadata` mediumtext,
  `errorCode` varchar(20) DEFAULT NULL,
  `errorMessage` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_payments_payments_integrationId_resourceId_unq_idx` (`integrationId`,`resourceId`),
  KEY `freeform_payments_payments_submissionId_fk` (`submissionId`),
  KEY `freeform_payments_payments_subscriptionId_fk` (`subscriptionId`),
  CONSTRAINT `freeform_payments_payments_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `freeform_integrations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_payments_payments_submissionId_fk` FOREIGN KEY (`submissionId`) REFERENCES `freeform_submissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_payments_payments_subscriptionId_fk` FOREIGN KEY (`subscriptionId`) REFERENCES `freeform_payments_subscriptions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_payments_subscription_plans`
--

DROP TABLE IF EXISTS `freeform_payments_subscription_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_payments_subscription_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `resourceId` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_payments_subscription_plans_integrationId_fk` (`integrationId`),
  CONSTRAINT `freeform_payments_subscription_plans_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_payments_subscriptions`
--

DROP TABLE IF EXISTS `freeform_payments_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_payments_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `submissionId` int(11) NOT NULL,
  `planId` int(11) NOT NULL,
  `resourceId` varchar(50) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `interval` varchar(20) DEFAULT NULL,
  `intervalCount` smallint(6) DEFAULT NULL,
  `last4` smallint(6) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `metadata` mediumtext,
  `errorCode` varchar(20) DEFAULT NULL,
  `errorMessage` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_payments_subscriptions_integrationId_resourceId_unq_idx` (`integrationId`,`resourceId`),
  KEY `freeform_payments_subscriptions_submissionId_fk` (`submissionId`),
  KEY `freeform_payments_subscriptions_planId_fk` (`planId`),
  CONSTRAINT `freeform_payments_subscriptions_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `freeform_integrations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_payments_subscriptions_planId_fk` FOREIGN KEY (`planId`) REFERENCES `freeform_payments_subscription_plans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_payments_subscriptions_submissionId_fk` FOREIGN KEY (`submissionId`) REFERENCES `freeform_submissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_statuses`
--

DROP TABLE IF EXISTS `freeform_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `color` varchar(30) DEFAULT NULL,
  `isDefault` tinyint(1) DEFAULT NULL,
  `sortOrder` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_submission_notes`
--

DROP TABLE IF EXISTS `freeform_submission_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_submission_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submissionId` int(11) NOT NULL,
  `note` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_submission_notes_submissionId_fk` (`submissionId`),
  CONSTRAINT `freeform_submission_notes_submissionId_fk` FOREIGN KEY (`submissionId`) REFERENCES `freeform_submissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_submissions`
--

DROP TABLE IF EXISTS `freeform_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incrementalId` int(11) NOT NULL,
  `statusId` int(11) DEFAULT NULL,
  `formId` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `ip` varchar(46) DEFAULT NULL,
  `isSpam` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_1` varchar(100) DEFAULT NULL,
  `field_2` varchar(100) DEFAULT NULL,
  `field_3` text,
  `field_4` varchar(100) DEFAULT NULL,
  `field_5` varchar(100) DEFAULT NULL,
  `field_6` varchar(100) DEFAULT NULL,
  `field_7` varchar(100) DEFAULT NULL,
  `field_8` text,
  `field_9` varchar(100) DEFAULT NULL,
  `field_10` varchar(100) DEFAULT NULL,
  `field_11` varchar(100) DEFAULT NULL,
  `field_12` text,
  `field_13` varchar(100) DEFAULT NULL,
  `field_14` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_submissions_incrementalId_unq_idx` (`incrementalId`),
  UNIQUE KEY `freeform_submissions_token_unq_idx` (`token`),
  KEY `freeform_submissions_formId_fk` (`formId`),
  KEY `freeform_submissions_statusId_fk` (`statusId`),
  CONSTRAINT `freeform_submissions_formId_fk` FOREIGN KEY (`formId`) REFERENCES `freeform_forms` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_submissions_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_submissions_statusId_fk` FOREIGN KEY (`statusId`) REFERENCES `freeform_statuses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_unfinalized_files`
--

DROP TABLE IF EXISTS `freeform_unfinalized_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_unfinalized_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_webhooks`
--

DROP TABLE IF EXISTS `freeform_webhooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `webhook` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_webhooks_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeform_webhooks_form_relations`
--

DROP TABLE IF EXISTS `freeform_webhooks_form_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeform_webhooks_form_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webhookId` int(11) NOT NULL,
  `formId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_webhooks_form_relations_webhookId_idx` (`webhookId`),
  KEY `freeform_webhooks_form_relations_formId_idx` (`formId`),
  CONSTRAINT `freeform_webhooks_form_relations_formId_fk` FOREIGN KEY (`formId`) REFERENCES `freeform_forms` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_webhooks_form_relations_webhookId_fk` FOREIGN KEY (`webhookId`) REFERENCES `freeform_webhooks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `globalsets`
--

DROP TABLE IF EXISTS `globalsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `globalsets_name_idx` (`name`),
  KEY `globalsets_handle_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gqlschemas`
--

DROP TABLE IF EXISTS `gqlschemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gqlschemas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `scope` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gqlschemas_accessToken_unq_idx` (`accessToken`),
  UNIQUE KEY `gqlschemas_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `config` mediumtext,
  `configMap` mediumtext,
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixblocks`
--

DROP TABLE IF EXISTS `matrixblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixblocktypes`
--

DROP TABLE IF EXISTS `matrixblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixcontent_banner`
--

DROP TABLE IF EXISTS `matrixcontent_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_slide_copy` text,
  `field_slide_button` text,
  `field_slide_heading` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_banner_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_banner_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_banner_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_banner_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixcontent_contentblocks`
--

DROP TABLE IF EXISTS `matrixcontent_contentblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_contentblocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_headingIntro_copy` text,
  `field_headingIntro_heading` text,
  `field_callToActions_heading` text,
  `field_plainText_layout` varchar(255) DEFAULT NULL,
  `field_plainText_copy` text,
  `field_priorityArea_heading` text,
  `field_priorFunding_fundingTotalHeading` text,
  `field_priorFunding_fundingTotal` text,
  `field_priorFunding_planTotal` text,
  `field_priorFunding_heading` text,
  `field_priorFunding_planHeading` text,
  `field_apply_copy` text,
  `field_apply_button` text,
  `field_fundingProgrammes_heading` text,
  `field_fundingProgrammes_copy` text,
  `field_fundingProgrammes_button` text,
  `field_priorFunding_fundingHeading` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_contentblocks_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_contentblocks_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_contentblocks_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_contentblocks_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixcontent_eligibility`
--

DROP TABLE IF EXISTS `matrixcontent_eligibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_eligibility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_addContent_heading` text,
  `field_addContent_copy` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_eligibility_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_eligibility_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_eligibility_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_eligibility_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixcontent_statstics`
--

DROP TABLE IF EXISTS `matrixcontent_statstics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_statstics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_addContent_heading` text,
  `field_addContent_eligibleApplications` text,
  `field_addContent_subheading` text,
  `field_addContent_grantsAwarded` text,
  `field_addContent_totalApplications` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_statstics_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_statstics_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_statstics_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_statstics_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixcontent_whatwefund`
--

DROP TABLE IF EXISTS `matrixcontent_whatwefund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_whatwefund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_addContent_topCopy` text,
  `field_addContent_bottomHeading` text,
  `field_addContent_bottomCopy` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_whatwefund_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_whatwefund_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_whatwefund_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_whatwefund_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `migrations_pluginId_idx` (`pluginId`),
  KEY `migrations_type_pluginId_idx` (`type`,`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `navigation_navs`
--

DROP TABLE IF EXISTS `navigation_navs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navigation_navs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `instructions` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `propagateNodes` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `navigation_navs_structureId_idx` (`structureId`),
  KEY `navigation_navs_dateDeleted_idx` (`dateDeleted`),
  KEY `navigation_navs_handle_idx` (`handle`),
  CONSTRAINT `navigation_navs_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `navigation_nodes`
--

DROP TABLE IF EXISTS `navigation_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navigation_nodes` (
  `id` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `navId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `classes` varchar(255) DEFAULT NULL,
  `newWindow` tinyint(1) DEFAULT '0',
  `deletedWithNav` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `navigation_nodes_navId_idx` (`navId`),
  KEY `navigation_nodes_elementId_fk` (`elementId`),
  CONSTRAINT `navigation_nodes_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE SET NULL,
  CONSTRAINT `navigation_nodes_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `navigation_nodes_navId_fk` FOREIGN KEY (`navId`) REFERENCES `navigation_navs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugins_handle_unq_idx` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  KEY `queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `relations_sourceId_idx` (`sourceId`),
  KEY `relations_targetId_idx` (`targetId`),
  KEY `relations_sourceSiteId_idx` (`sourceSiteId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resourcepaths`
--

DROP TABLE IF EXISTS `resourcepaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `revisions`
--

DROP TABLE IF EXISTS `revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `revisions_sourceId_num_unq_idx` (`sourceId`,`num`),
  KEY `revisions_creatorId_fk` (`creatorId`),
  CONSTRAINT `revisions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `revisions_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `searchindex`
--

DROP TABLE IF EXISTS `searchindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) NOT NULL DEFAULT 'all',
  `previewTargets` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sections_handle_idx` (`handle`),
  KEY `sections_name_idx` (`name`),
  KEY `sections_structureId_idx` (`structureId`),
  KEY `sections_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sections_sites`
--

DROP TABLE IF EXISTS `sections_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  KEY `sections_sites_siteId_idx` (`siteId`),
  CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sequences`
--

DROP TABLE IF EXISTS `sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_idx` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shunnedmessages`
--

DROP TABLE IF EXISTS `shunnedmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sitegroups`
--

DROP TABLE IF EXISTS `sitegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sitegroups_name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sites_dateDeleted_idx` (`dateDeleted`),
  KEY `sites_handle_idx` (`handle`),
  KEY `sites_sortOrder_idx` (`sortOrder`),
  KEY `sites_groupId_fk` (`groupId`),
  CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stc_3_ctas`
--

DROP TABLE IF EXISTS `stc_3_ctas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_3_ctas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_button` text,
  `field_copy` text,
  `field_heading` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_3_ctas_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_3_ctas_siteId_idx` (`siteId`),
  CONSTRAINT `stc_3_ctas_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_3_ctas_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stc_7_grants`
--

DROP TABLE IF EXISTS `stc_7_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_7_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_anchorProgramme` text,
  `field_specialInitiatives` text,
  `field_openProgramme` text,
  `field_grantsAwarded` text,
  `field_totalApplications` text,
  `field_eligibleApplications` text,
  `field_total` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_7_grants_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_7_grants_siteId_idx` (`siteId`),
  CONSTRAINT `stc_7_grants_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_7_grants_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stc_9_programmes`
--

DROP TABLE IF EXISTS `stc_9_programmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_9_programmes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_copy` text,
  `field_subheading` text,
  `field_heading` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_9_programmes_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_9_programmes_siteId_idx` (`siteId`),
  CONSTRAINT `stc_9_programmes_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_9_programmes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stc_footer`
--

DROP TABLE IF EXISTS `stc_footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_address` text,
  `field_contact` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_footer_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_footer_siteId_fk` (`siteId`),
  CONSTRAINT `stc_footer_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_footer_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structureelements`
--

DROP TABLE IF EXISTS `structureelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_idx` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `structures_dateDeleted_idx` (`dateDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supertableblocks`
--

DROP TABLE IF EXISTS `supertableblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertableblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `supertableblocks_ownerId_idx` (`ownerId`),
  KEY `supertableblocks_fieldId_idx` (`fieldId`),
  KEY `supertableblocks_typeId_idx` (`typeId`),
  KEY `supertableblocks_sortOrder_idx` (`sortOrder`),
  CONSTRAINT `supertableblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `supertableblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supertableblocktypes`
--

DROP TABLE IF EXISTS `supertableblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertableblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `supertableblocktypes_fieldId_idx` (`fieldId`),
  KEY `supertableblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `supertableblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemmessages`
--

DROP TABLE IF EXISTS `systemmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  KEY `systemmessages_language_idx` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taggroups`
--

DROP TABLE IF EXISTS `taggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `taggroups_name_idx` (`name`),
  KEY `taggroups_handle_idx` (`handle`),
  KEY `taggroups_dateDeleted_idx` (`dateDeleted`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_idx` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `templatecacheelements`
--

DROP TABLE IF EXISTS `templatecacheelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  KEY `templatecacheelements_elementId_idx` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `templatecachequeries`
--

DROP TABLE IF EXISTS `templatecachequeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  KEY `templatecachequeries_type_idx` (`type`),
  CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `templatecaches`
--

DROP TABLE IF EXISTS `templatecaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `templatecaches_siteId_idx` (`siteId`),
  CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  UNIQUE KEY `usergroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usergroups_users`
--

DROP TABLE IF EXISTS `usergroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_idx` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpermissions`
--

DROP TABLE IF EXISTS `userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpermissions_usergroups`
--

DROP TABLE IF EXISTS `userpermissions_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_idx` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpermissions_users`
--

DROP TABLE IF EXISTS `userpermissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_idx` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpreferences`
--

DROP TABLE IF EXISTS `userpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text,
  PRIMARY KEY (`userId`),
  CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_email_idx` (`email`),
  KEY `users_username_idx` (`username`),
  KEY `users_photoId_fk` (`photoId`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `volumefolders`
--

DROP TABLE IF EXISTS `volumefolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  KEY `volumefolders_parentId_idx` (`parentId`),
  KEY `volumefolders_volumeId_idx` (`volumeId`),
  CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `volumes_name_idx` (`name`),
  KEY `volumes_handle_idx` (`handle`),
  KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `volumes_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_idx` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'aoSvDcEU'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-10 19:31:02
-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: aoSvDcEU
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-1~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `assets` VALUES (11,1,3,'icon-abct.png','image',230,230,19789,NULL,NULL,NULL,'2019-09-25 14:16:05','2019-09-25 14:16:05','2019-09-25 14:16:05','3ffca891-8bb9-439f-a189-5c30e62780d9');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `assettransforms`
--

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `categories` VALUES (38,1,NULL,NULL,'2019-09-25 14:56:09','2019-09-25 14:56:09','2563506e-e6a4-4c3c-8d34-5fcb2b6cb1ae'),(39,1,NULL,NULL,'2019-09-25 14:56:16','2019-09-25 14:56:16','4a96e3e3-5071-4780-a997-28cb88bdf9a6'),(40,1,NULL,NULL,'2019-09-25 14:56:21','2019-09-25 14:56:21','7b46527f-62a1-4acf-bfa8-ea28422ef708');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `categorygroups`
--

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `categorygroups` VALUES (1,4,NULL,'Priorities','priorities','2019-09-25 14:35:12','2019-09-25 14:35:12',NULL,'89827e85-e074-46eb-b2d7-1a2ca2a6be33');
/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `categorygroups_sites`
--

LOCK TABLES `categorygroups_sites` WRITE;
/*!40000 ALTER TABLE `categorygroups_sites` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `categorygroups_sites` VALUES (1,1,1,0,NULL,NULL,'2019-09-25 14:35:12','2019-09-25 14:35:12','b29ade87-91df-44f4-9a7c-22bb6c517637');
/*!40000 ALTER TABLE `categorygroups_sites` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `content` VALUES (1,1,1,NULL,'2019-09-23 17:41:02','2019-09-25 14:16:06','c7315326-8d7f-47c2-a217-2a55f59feb5a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,1,'Home','2019-09-23 18:30:14','2019-12-16 11:57:28','d0471964-0136-4a0b-a839-c3eff593cfa5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,3,1,'Home','2019-09-23 18:30:14','2019-09-23 18:30:14','5e56739f-4d18-4142-a8d9-e50bdce9ac07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,4,1,'Home','2019-09-23 18:30:22','2019-09-23 18:30:22','06dbc244-af23-44c4-9b05-c5f5d2e7d8c8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,5,1,NULL,'2019-09-25 13:43:56','2019-09-25 13:43:56','3f7f6413-3b5f-416a-8e44-ccb7a7a17461',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,6,1,NULL,'2019-09-25 13:44:12','2019-09-25 13:44:12','8d2bf53f-cb15-45ba-b58b-ee5dc3e3a0f9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,7,1,NULL,'2019-09-25 13:44:31','2019-09-25 13:44:31','ca7acc23-a7d6-4ea6-aa55-c3267384311c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,8,1,'Home','2019-09-25 13:46:03','2019-09-25 13:46:03','fe39f7ce-4a72-4d0f-be46-77e69b4b3778',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,9,1,NULL,'2019-09-25 14:06:39','2019-12-13 15:23:35','08fe5a49-adac-4876-bf4a-bff0f92faa1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,10,1,NULL,'2019-09-25 14:10:36','2019-09-25 14:10:36','26de64be-c9fb-4cab-875a-9f4f917e05c3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,11,1,'Icon abct','2019-09-25 14:16:05','2019-09-25 14:16:05','30a5bf7e-a0c9-48d3-a34f-7790bd8c4779',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,13,1,'About us','2019-09-25 14:23:53','2019-10-17 10:25:33','3076e9b0-ff68-4652-a5b0-ff4c600fd3d2',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,14,1,'About Us','2019-09-25 14:23:53','2019-09-25 14:23:53','4bb529cd-667d-4af6-9bd4-3242d5d1669b',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,16,1,'Our priorities','2019-09-25 14:24:14','2019-10-17 10:25:33','e644e5a6-b9ed-4af3-bea2-c4ff5a81e7a9',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,17,1,'Our Priorities','2019-09-25 14:24:14','2019-09-25 14:24:14','3970be86-811d-4502-b0ce-107371798057',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,18,1,'Our priorities','2019-09-25 14:24:20','2019-09-25 14:24:20','1ce5a011-004f-4869-8868-46c94ca36c6b',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,19,1,'About us','2019-09-25 14:24:27','2019-09-25 14:24:27','a0380480-6257-40c4-851e-f27619de9135',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,21,1,'How we fund','2019-09-25 14:24:35','2019-10-17 10:25:33','68e098a5-ec8f-4a44-9f7e-592563020bdd',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,22,1,'How we fund','2019-09-25 14:24:35','2019-09-25 14:24:35','52155502-8a24-4ad3-becf-38fae727e871',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,24,1,'Grantees','2019-09-25 14:24:51','2019-10-17 10:25:33','cade3925-1e30-4354-a832-5fe3d713ce21',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,25,1,'Grantees','2019-09-25 14:24:51','2019-09-25 14:24:51','5bae8c21-8479-4384-b1c1-ea72a1896948',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,27,1,'Apply','2019-09-25 14:24:57','2019-10-17 10:36:53','60c7339b-7c56-46bf-8100-5d52b07e668d',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,28,1,'Apply','2019-09-25 14:24:57','2019-09-25 14:24:57','225e76af-0e77-4fa4-af9e-2e4fc7338cb7',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,30,1,'Help & Advice','2019-09-25 14:34:00','2019-10-17 10:25:33','b304f5f1-5cd4-4fd9-b194-c7eb7e713e3d',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,31,1,'Help & Advice','2019-09-25 14:34:00','2019-09-25 14:34:00','fa061c5c-adae-4e7f-95de-7b262338f93f',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,33,1,'FAQs','2019-09-25 14:34:05','2019-10-17 10:25:33','0c822fd7-ebba-4d0c-95da-6ef7e7d67153',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,34,1,'FAQs','2019-09-25 14:34:05','2019-09-25 14:34:05','b6de0241-a6d1-49e0-a4d0-5af3c7f4f68c',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,36,1,'Contact','2019-09-25 14:34:11','2019-10-17 10:25:34','f59dc48c-b8da-49a3-a6fd-2d94bb7f9e88',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,37,1,'Contact','2019-09-25 14:34:11','2019-09-25 14:34:11','d35a2cbd-c49e-4827-87b1-6c7db091fed6',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,38,1,'Migrants, refugees and asylum seekers','2019-09-25 14:56:09','2019-09-25 14:56:09','e6e782a4-dda7-4751-bea3-00f7dab7dfc3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,39,1,'Criminal justice and penal reform','2019-09-25 14:56:16','2019-09-25 14:56:16','8c90cc3b-8e5c-4b9c-b981-1268cd6eef35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,40,1,'Access to justice','2019-09-25 14:56:21','2019-09-25 14:56:21','46947341-4329-448a-812e-c8e3226867a2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,42,1,'Trustees','2019-09-25 14:57:05','2019-10-17 10:25:34','135933c2-46e3-4e02-b944-e1ed7053c83e',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,43,1,'Trustees','2019-09-25 14:57:05','2019-09-25 14:57:05','fda202bf-e7a6-498a-b1b4-fba19434814f',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(45,45,1,'Staff','2019-09-25 14:57:14','2019-10-17 10:25:34','c4b2fd8b-18ef-4f49-81d9-250087b13921',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,46,1,'Staff','2019-09-25 14:57:14','2019-09-25 14:57:14','1a31e95a-287a-4fe7-b13d-e72995391f4c',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,48,1,'Grants awarded','2019-09-25 14:57:45','2019-10-17 10:25:34','06439892-2c2d-464a-889c-57b79fefb661',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,49,1,'Grants awarded','2019-09-25 14:57:45','2019-09-25 14:57:45','1adee4b7-919f-489e-9a04-fab082f39266',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,51,1,'Information for grantees','2019-09-25 14:57:56','2019-10-17 10:25:34','e360832c-d060-42b7-aa92-fd1597d47ca3',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,52,1,'Information for grantees','2019-09-25 14:57:56','2019-09-25 14:57:56','6a714cd0-5108-4f1b-9e0d-f3f26ec48d3c',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,54,1,'Bonavero Institute','2019-09-25 14:58:01','2019-10-17 10:25:34','f120a4f3-8430-4894-8713-597c3548361e',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(55,55,1,'Bonavero Institute','2019-09-25 14:58:01','2019-09-25 14:58:01','8e4cc49f-3267-40fd-9f03-5c5ae802038c',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,56,1,'Home','2019-09-26 17:16:46','2019-12-16 11:57:28','36075946-23d6-4e8b-aa9c-fb94f990247e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,57,1,'About us','2019-09-26 17:17:00','2019-10-17 10:25:33','613dd6b8-9c9f-4b44-926e-808ec5d888fc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(58,58,1,'Apply','2019-09-26 17:17:00','2019-10-17 10:36:53','c6297f57-3cc8-40c2-90d4-410579edf795',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(59,59,1,'How we fund','2019-09-26 17:17:01','2019-10-17 10:25:33','fa85d618-bca6-48a1-8385-2b64780e1905',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(60,60,1,'Grantees','2019-09-26 17:17:01','2019-10-17 10:25:33','2b3c4316-248f-4465-b1de-b534008fb542',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(61,61,1,'Our priorities','2019-09-26 17:17:01','2019-10-17 10:25:33','aafc11f7-ee30-4965-8d8b-137b77f49cf9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(62,62,1,'Help & Advice','2019-09-26 17:17:59','2019-10-17 10:25:33','b6666281-1d5c-40d0-bf88-1ace54fcedbd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(63,63,1,'FAQs','2019-09-26 17:18:00','2019-10-17 10:25:33','e239446c-753c-483f-8433-e39b39434ae2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(64,64,1,'Contact','2019-09-26 17:18:00','2019-10-17 10:25:34','7ac943fb-0463-4400-9bbc-de664ca495df',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(65,66,1,'Apply','2019-10-17 10:36:53','2019-10-17 10:36:53','c4a6874d-aa9c-4a5e-b449-acc1a4a5c830',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(67,68,1,'Migrants, refugees and asylum seekers','2019-12-12 17:53:01','2019-12-17 16:22:21','bff825bd-f402-45d4-9dcd-7cdf575227d1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(68,69,1,'Migrants, refugees and asylum seekers','2019-12-12 17:53:01','2019-12-12 17:53:01','a3633af2-3111-401a-b8a9-cd340102cd52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(70,71,1,'Criminal justice and penal reform','2019-12-12 17:53:07','2019-12-17 16:22:21','23ae75f3-6283-4e8c-9cf0-5efbcfb1e114',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(71,72,1,'Criminal justice and penal reform','2019-12-12 17:53:07','2019-12-12 17:53:07','57db497f-5c48-4c04-a568-b74d919af5f3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(73,74,1,'Human rights, particularly access to justice','2019-12-12 17:53:16','2019-12-17 16:22:21','109715d0-65e6-4a76-9660-a6ecb7ef89e0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(74,75,1,'Human rights, particularly access to justice','2019-12-12 17:53:16','2019-12-12 17:53:16','8bad3e36-6d24-4522-a7c6-480b49fde27a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(75,76,1,'Home','2019-12-12 18:56:24','2019-12-12 18:56:24','21745858-a5e0-4123-9100-8522000533c2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(76,77,1,'Home','2019-12-12 18:56:31','2019-12-12 18:56:31','3642ef1f-8282-4374-b998-166c9cc1c699',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(77,80,1,'Home','2019-12-12 19:01:06','2019-12-12 19:01:06','a972a67f-0d2f-452c-a1a3-0fc8c6c3dd86',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(78,83,1,'Home','2019-12-12 19:01:17','2019-12-12 19:01:17','493f506c-1a87-4748-89c7-55ca166e6871',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(79,87,1,'Home','2019-12-12 19:01:48','2019-12-12 19:01:48','7c4db7f8-de78-44c0-84db-f0d37504fda9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(80,94,1,'Home','2019-12-12 19:03:40','2019-12-12 19:03:40','5e21ea09-49f9-403e-a64c-4c5f25ca2f23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(81,105,1,'Home','2019-12-12 19:05:20','2019-12-12 19:05:20','12693969-eae4-412f-ab3a-9ec92b3bc9ee',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(82,117,1,'Home','2019-12-12 19:05:53','2019-12-12 19:05:53','6a823c6a-d6ef-4b2b-a911-806f776736ec',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(83,129,1,'Home','2019-12-16 11:57:28','2019-12-16 11:57:28','a5a36b23-3f0d-4861-be3b-c482b3f1a56f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `craftidtokens`
--

LOCK TABLES `craftidtokens` WRITE;
/*!40000 ALTER TABLE `craftidtokens` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `craftidtokens` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `deprecationerrors`
--

LOCK TABLES `deprecationerrors` WRITE;
/*!40000 ALTER TABLE `deprecationerrors` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `dolphiq_redirects`
--

LOCK TABLES `dolphiq_redirects` WRITE;
/*!40000 ALTER TABLE `dolphiq_redirects` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `dolphiq_redirects` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `dolphiq_redirects_catch_all_urls`
--

LOCK TABLES `dolphiq_redirects_catch_all_urls` WRITE;
/*!40000 ALTER TABLE `dolphiq_redirects_catch_all_urls` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `dolphiq_redirects_catch_all_urls` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `drafts`
--

LOCK TABLES `drafts` WRITE;
/*!40000 ALTER TABLE `drafts` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `drafts` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `elementindexsettings`
--

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `elementindexsettings` VALUES (1,'craft\\elements\\Entry','{\"sourceOrder\":[[\"key\",\"*\"],[\"heading\",\"Website Pages\"],[\"key\",\"singles\"],[\"key\",\"section:ee328c08-2719-4360-a27f-4d3fc4ca60ab\"],[\"heading\",\"Channels\"],[\"key\",\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],[\"key\",\"section:40245d37-4cf6-4080-b358-9aa027534703\"],[\"key\",\"section:5f9ddfa5-840d-422b-9d15-69d77baa68fd\"],[\"key\",\"section:3c91ce22-6de2-4079-9273-1472dd23dc27\"]],\"sources\":{\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"author\",\"4\":\"link\"}}}}','2019-12-12 17:52:28','2019-12-12 17:52:28','4120b8a5-9dd8-4f93-8f9e-45785c9121ad');
/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `elements` VALUES (1,NULL,NULL,NULL,'craft\\elements\\User',1,0,'2019-09-23 17:41:02','2019-09-25 14:16:06',NULL,'746427a2-f2bb-46c0-b136-c48de035c418'),(2,NULL,NULL,6,'craft\\elements\\Entry',1,0,'2019-09-23 18:30:14','2019-12-16 11:57:28',NULL,'3ced4b9b-bee0-4075-a44d-2dac27e67308'),(3,NULL,1,NULL,'craft\\elements\\Entry',1,0,'2019-09-23 18:30:14','2019-09-23 18:30:14',NULL,'630e15f6-1058-4dec-948b-be648b619ebd'),(4,NULL,2,NULL,'craft\\elements\\Entry',1,0,'2019-09-23 18:30:22','2019-09-23 18:30:22',NULL,'ea0b8dc5-7eac-4535-898f-7484a7b79b90'),(5,NULL,NULL,2,'craft\\elements\\GlobalSet',1,0,'2019-09-25 13:43:56','2019-09-25 13:43:56',NULL,'d155571f-f1d3-463b-bffa-bdc5b6e39f94'),(6,NULL,NULL,3,'craft\\elements\\GlobalSet',1,0,'2019-09-25 13:44:12','2019-09-25 13:44:12',NULL,'0715cf79-f9be-4d71-b893-bd2989d0bac2'),(7,NULL,NULL,4,'craft\\elements\\GlobalSet',1,0,'2019-09-25 13:44:31','2019-09-25 13:44:31',NULL,'4d498ec6-fb1a-4e31-8c25-e227f5e1d7eb'),(8,NULL,3,NULL,'craft\\elements\\Entry',1,0,'2019-09-25 13:46:03','2019-09-25 13:46:03',NULL,'be304397-4d08-4d9f-b68a-7b7a084663a1'),(9,NULL,NULL,8,'craft\\elements\\GlobalSet',1,0,'2019-09-25 14:06:39','2019-12-13 15:23:35',NULL,'69a7e8fa-2d1a-47ed-83fb-e044dfd1debf'),(10,NULL,NULL,9,'craft\\elements\\GlobalSet',1,0,'2019-09-25 14:10:36','2019-09-25 14:10:36',NULL,'ed6bfda2-03b4-4bef-97fb-c160cbb23caa'),(11,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-09-25 14:16:05','2019-09-25 14:16:05',NULL,'dc7a5b09-f99e-4647-a43d-93d2455e964e'),(13,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:23:53','2019-09-25 14:24:27',NULL,'dfcedd55-be98-4326-9756-d4a5c2352937'),(14,NULL,4,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:23:53','2019-09-25 14:23:53',NULL,'9682d06b-32c7-41de-9449-53cf52a1e954'),(16,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:14','2019-09-25 14:24:20',NULL,'3e6a6f31-3813-487d-8d85-4866be9e163c'),(17,NULL,5,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:14','2019-09-25 14:24:14',NULL,'c85f3453-fc9b-4a8a-bf87-755c180b990d'),(18,NULL,6,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:20','2019-09-25 14:24:20',NULL,'e4b43981-1bfc-44c7-a6eb-918598cc7ac5'),(19,NULL,7,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:27','2019-09-25 14:24:27',NULL,'f718ebc9-4c77-4ea5-8ee7-e7c4fdbb1ea3'),(21,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:35','2019-09-25 14:24:35',NULL,'289cdf62-69a4-44e2-a32c-40ffcea07759'),(22,NULL,8,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:35','2019-09-25 14:24:35',NULL,'a0b5c489-2509-4d8f-a056-7c35cd414460'),(24,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:51','2019-09-25 14:24:51',NULL,'895c0ab1-be97-400e-928e-3e04078b1f98'),(25,NULL,9,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:51','2019-09-25 14:24:51',NULL,'a5eb191f-9a95-4c19-8ead-23bd4aaec3af'),(27,NULL,NULL,14,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:57','2019-10-17 10:36:53',NULL,'432dfe2d-3b30-4bda-a7d8-dd234a551e84'),(28,NULL,10,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:24:57','2019-09-25 14:24:57',NULL,'c683216a-f803-40c1-a865-9da8bc4d2e54'),(30,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:34:00','2019-09-25 14:34:00',NULL,'211092fe-d48a-4f21-9433-d292c2d5b2f6'),(31,NULL,11,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:34:00','2019-09-25 14:34:00',NULL,'956ca502-575c-43d8-bf78-fc7f01e12a68'),(33,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:34:05','2019-09-25 14:34:05',NULL,'a0e1fd67-593e-4dd2-8fa8-de242e77d400'),(34,NULL,12,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:34:05','2019-09-25 14:34:05',NULL,'b8244955-26e2-47e3-a714-a6515c03dd82'),(36,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:34:11','2019-09-25 14:34:11',NULL,'594195b7-bce3-4c0e-a019-72ea1a71b3a1'),(37,NULL,13,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:34:11','2019-09-25 14:34:11',NULL,'368ef82e-bc8c-410a-952b-12d01b3a0e31'),(38,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-09-25 14:56:09','2019-09-25 14:56:09',NULL,'21b2229e-2600-45cb-a45d-6141ad645cf5'),(39,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-09-25 14:56:16','2019-09-25 14:56:16',NULL,'c7c47572-6577-474d-8fa7-007901c66c4a'),(40,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-09-25 14:56:21','2019-09-25 14:56:21',NULL,'b5f34de2-da5d-4fbb-b9a3-54f65a4b4989'),(42,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:05','2019-09-25 14:57:05',NULL,'c7435992-9e6e-47e5-b5ae-d0914d09ae02'),(43,NULL,14,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:05','2019-09-25 14:57:05',NULL,'f3824382-7b97-4795-a64c-d32944e4ebf3'),(45,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:14','2019-09-25 14:57:14',NULL,'e89dee5b-0197-4cf0-8de7-3da1a0bc5602'),(46,NULL,15,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:14','2019-09-25 14:57:14',NULL,'4a61d1db-1119-4917-8fce-ed5b02158a65'),(48,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:45','2019-09-25 14:57:45',NULL,'e86e6e63-da3c-4d3e-8734-1ad38dec6c6a'),(49,NULL,16,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:45','2019-09-25 14:57:45',NULL,'f4397cf5-80a9-4de5-bc7f-9455debc7e46'),(51,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:56','2019-09-25 14:57:56',NULL,'37235e8c-4314-462a-8c55-87857e7850d5'),(52,NULL,17,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:57:56','2019-09-25 14:57:56',NULL,'152698d2-31be-4edb-b396-3b24abe8c750'),(54,NULL,NULL,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:58:01','2019-09-25 14:58:01',NULL,'ec84735c-01f0-45ea-abc7-f21b9824c9ee'),(55,NULL,18,5,'craft\\elements\\Entry',1,0,'2019-09-25 14:58:01','2019-09-25 14:58:01',NULL,'c58921a5-6e65-4953-83ff-1897c4294bd3'),(56,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:16:46','2019-12-16 11:57:28',NULL,'be8ef282-2287-44c1-9d83-1f3fdfacb796'),(57,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:17:00','2019-10-17 10:25:33',NULL,'84002dff-b567-4f70-af92-15eddcc495ed'),(58,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:17:00','2019-10-17 10:36:53',NULL,'74d1005f-1a17-4763-98af-7a6a4337fe55'),(59,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:17:01','2019-10-17 10:25:33',NULL,'6814be23-579e-4c08-a188-057e93ff4ac5'),(60,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:17:01','2019-10-17 10:25:33',NULL,'82f3dc71-d747-4472-8768-07fac5f5f639'),(61,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:17:01','2019-10-17 10:25:33',NULL,'ba46b2d4-06f8-4bc9-a8d4-978bf96acd23'),(62,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:17:59','2019-10-17 10:25:33',NULL,'2831c8a2-c302-4299-81dd-8451d0bc7c73'),(63,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:18:00','2019-10-17 10:25:33',NULL,'62334673-711c-46d9-a092-7aa4c8f112c2'),(64,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-09-26 17:18:00','2019-10-17 10:25:34',NULL,'11416423-2943-4b81-bc6d-9418c7523b15'),(65,NULL,NULL,7,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-09-26 17:19:47','2019-12-13 15:23:35',NULL,'689c7fc9-b60d-4fb7-bf0b-a84765ac3e70'),(66,NULL,19,14,'craft\\elements\\Entry',1,0,'2019-10-17 10:36:53','2019-10-17 10:36:53',NULL,'4e22b34d-8854-4a8a-9a84-a340ac4ef2ce'),(68,NULL,NULL,26,'craft\\elements\\Entry',1,0,'2019-12-12 17:53:01','2019-12-12 17:53:01',NULL,'1620f2a1-0303-4b8a-aaf0-885ed315e990'),(69,NULL,20,NULL,'craft\\elements\\Entry',1,0,'2019-12-12 17:53:01','2019-12-12 17:53:01',NULL,'9b428a8e-4d19-4855-a405-1106f0d74d5e'),(71,NULL,NULL,26,'craft\\elements\\Entry',1,0,'2019-12-12 17:53:07','2019-12-12 17:53:07',NULL,'3b8407e4-cc39-4a9b-a99c-f01a11060946'),(72,NULL,21,NULL,'craft\\elements\\Entry',1,0,'2019-12-12 17:53:07','2019-12-12 17:53:07',NULL,'79928ee3-d01d-4c10-8eae-fadab59a9d33'),(74,NULL,NULL,26,'craft\\elements\\Entry',1,0,'2019-12-12 17:53:16','2019-12-12 17:53:16',NULL,'413c25d8-91f7-4543-b9d4-da022c744877'),(75,NULL,22,NULL,'craft\\elements\\Entry',1,0,'2019-12-12 17:53:16','2019-12-12 17:53:16',NULL,'be89f335-69bb-4728-99f4-26e9fd1dbca0'),(76,NULL,23,6,'craft\\elements\\Entry',1,0,'2019-12-12 18:56:24','2019-12-12 18:56:24',NULL,'3430e759-b072-465d-8f41-26f3dcda2d05'),(77,NULL,24,6,'craft\\elements\\Entry',1,0,'2019-12-12 18:56:31','2019-12-12 18:56:31',NULL,'682d7167-62e3-46c2-805d-abfadb95043e'),(78,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:06','2019-12-16 11:57:28',NULL,'f41b6596-60ab-428a-ab78-467a59ae84db'),(79,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:06','2019-12-16 11:57:28',NULL,'757fcecb-7b55-4fcc-ac24-806df0d042d7'),(80,NULL,25,6,'craft\\elements\\Entry',1,0,'2019-12-12 19:01:06','2019-12-12 19:01:06',NULL,'d142c317-8395-4650-806f-713be933ef92'),(81,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:06','2019-12-12 19:01:06',NULL,'2361e414-260d-47d3-a6eb-a21e374359c7'),(82,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:06','2019-12-12 19:01:06',NULL,'168333ad-9624-46b5-8406-34fc4ae16d5d'),(83,NULL,26,6,'craft\\elements\\Entry',1,0,'2019-12-12 19:01:17','2019-12-12 19:01:17',NULL,'5021ca2e-ee47-4ed2-bb2a-bf185f11c5c0'),(84,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:17','2019-12-12 19:01:17',NULL,'f5b979d8-ee4f-4450-909e-d29eb16b4199'),(85,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:17','2019-12-12 19:01:17',NULL,'cd07fc30-aec3-4b7d-9b9e-5214716af7b6'),(86,NULL,NULL,24,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:48','2019-12-16 11:57:28',NULL,'3724a069-fe1d-4fcf-9818-a94858a3fa25'),(87,NULL,27,6,'craft\\elements\\Entry',1,0,'2019-12-12 19:01:48','2019-12-12 19:01:48',NULL,'2c772f49-231f-4a1f-a9ce-cb60cedaa541'),(88,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:48','2019-12-12 19:01:48',NULL,'f8e9c26b-ba17-4ab9-9351-821af82efc13'),(89,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:48','2019-12-12 19:01:48',NULL,'c0a32d47-3197-49b9-a97c-5057fa881510'),(90,NULL,NULL,24,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:01:48','2019-12-12 19:01:48',NULL,'2d63e850-7375-4ca1-84a9-ca02a00292b0'),(91,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:03:40','2019-12-16 11:57:28',NULL,'35ecf6e2-e108-44cf-ad4b-da0bc27c41ad'),(92,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:03:40','2019-12-16 11:57:28',NULL,'73429d55-642f-4e57-ab95-de3a8231be42'),(93,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:03:40','2019-12-16 11:57:28',NULL,'4a160dec-3487-4a6e-8f82-4785c6586b90'),(94,NULL,28,6,'craft\\elements\\Entry',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'5bf3a15c-1b0d-4758-b391-43cdda1840cd'),(95,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'e6b8ac55-05df-4366-ae8f-d48fb4edb84e'),(96,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'b9553856-2eb0-4362-8334-d0324ea45318'),(97,NULL,NULL,24,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'5c6c7561-4451-4060-9843-0525b522276b'),(98,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'5679f342-3b96-42c0-abcc-1f67afde50a5'),(99,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'d36d735c-b74f-43ad-9fa7-f1c584b03d09'),(100,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:03:40','2019-12-12 19:03:40',NULL,'13222065-137f-4f38-87dc-82debf4e1ce6'),(101,NULL,NULL,21,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:20','2019-12-16 11:57:28',NULL,'7e00a9f2-542b-44de-823a-7b6e595e8a54'),(102,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-16 11:57:28',NULL,'bc6aded3-f0d7-4d6a-8848-aef964f691f6'),(103,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-16 11:57:28',NULL,'03bc464d-0db5-4a5d-b015-1477b7276ea8'),(104,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-16 11:57:28',NULL,'0629d0f5-c2c3-47fa-a56c-2639106c04c2'),(105,NULL,29,6,'craft\\elements\\Entry',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'582b86f1-206f-46b0-9c46-ad292c96b91b'),(106,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'105c41be-4cb9-432d-8d5d-1a99dd73c190'),(107,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'c8211bdb-4d96-4a76-a2a7-902bf474b9c4'),(108,NULL,NULL,24,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'1cc92e31-5875-4a25-81a6-55492ec67a38'),(109,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'23e183df-8c89-4c1c-a61c-7a63842f1dd2'),(110,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'2427e52e-efe7-422f-bcd3-385034291164'),(111,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'be2f88af-8974-4b93-84ec-c923286b976d'),(112,NULL,NULL,21,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'a103fd9a-58fb-4ea3-bb7b-f8fcdbb4c73d'),(113,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'361e22cc-9867-4a54-b7ec-6043926041cd'),(114,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'b31ecf85-3f91-499a-b33b-4b5cffc22b71'),(115,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:20','2019-12-12 19:05:20',NULL,'8d4f6c9e-4114-4472-bdfa-bb8224221c0f'),(116,NULL,NULL,22,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:53','2019-12-16 11:57:28',NULL,'b1affe8c-62de-4bb3-963f-619c9e47ae42'),(117,NULL,30,6,'craft\\elements\\Entry',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'a2147561-2671-481f-8fa9-97a4c7dfe09d'),(118,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'0908d5f9-1908-47cf-a1b6-09fffab3138f'),(119,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'3aab7f25-dd59-4706-a368-e9d9f1db0d68'),(120,NULL,NULL,24,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'58319c0c-abb7-491a-8184-03624a21cd51'),(121,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'5f127947-3cde-4acf-b174-776bd39daa04'),(122,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'7e81d339-27cd-45ce-b2e7-5f49096f5115'),(123,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'80d0265c-4e94-4a1f-af1b-37d88e391473'),(124,NULL,NULL,21,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'5e083b34-9b6c-458b-9d90-bd0fa9f7204f'),(125,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'bc69a598-b634-497d-b828-f7a16fc4cda8'),(126,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'49ded748-2fbc-4e70-9688-1790e7001b60'),(127,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'ec35c0d8-89b7-48b4-af76-43c1f78c9710'),(128,NULL,NULL,22,'craft\\elements\\MatrixBlock',1,0,'2019-12-12 19:05:53','2019-12-12 19:05:53',NULL,'56e5c968-96e9-464e-abfa-bc189aeccb6d'),(129,NULL,31,6,'craft\\elements\\Entry',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'f08a9307-5809-49f0-a2a2-b43acf280b07'),(130,NULL,NULL,18,'craft\\elements\\MatrixBlock',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'1d35ac31-f3e3-4cdd-b51a-820f43a753d4'),(131,NULL,NULL,19,'craft\\elements\\MatrixBlock',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'7fd30d40-1c13-4889-80c8-4169c73703b9'),(132,NULL,NULL,24,'craft\\elements\\MatrixBlock',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'988cfff0-df0d-4e57-b1cd-c07e73f2cf69'),(133,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'0cc5cfc7-660d-44c9-9ea5-2d020022014e'),(134,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'72db80ec-0dcd-462c-be23-1c87e8638dcc'),(135,NULL,NULL,23,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'efb77cfc-05bc-4b08-b7ac-3cc6567115fc'),(136,NULL,NULL,21,'craft\\elements\\MatrixBlock',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'47672889-c1a6-44f0-bbb8-2e465b5987f1'),(137,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'c546f499-01ce-4d3b-aafd-d70d00c4c234'),(138,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'fe73889b-2cbb-49f1-afcf-97ae026866df'),(139,NULL,NULL,20,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'19247918-db1b-42c2-854a-e8985238cc23'),(140,NULL,NULL,22,'craft\\elements\\MatrixBlock',1,0,'2019-12-16 11:57:28','2019-12-16 11:57:28',NULL,'741b0295-ca68-47d3-a079-04b4733d0c9a');
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `elements_sites`
--

LOCK TABLES `elements_sites` WRITE;
/*!40000 ALTER TABLE `elements_sites` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `elements_sites` VALUES (1,1,1,NULL,NULL,1,'2019-09-23 17:41:02','2019-09-23 17:41:02','1be60074-242b-487c-95cf-f25caf3599e1'),(2,2,1,'home','__home__',1,'2019-09-23 18:30:14','2019-09-23 18:30:22','2a437aaa-216b-42d1-a6b3-4c15b54ca61d'),(3,3,1,'home',NULL,1,'2019-09-23 18:30:14','2019-09-23 18:30:14','e1808cb2-9a7f-4894-8095-69ce461f39b5'),(4,4,1,'home','__home__',1,'2019-09-23 18:30:22','2019-09-23 18:30:22','706bbe09-0ebb-4c2c-86a4-dd2a1dee52d5'),(5,5,1,NULL,NULL,1,'2019-09-25 13:43:56','2019-09-25 13:43:56','84233134-01f8-48ab-8208-4b20d1d7a7d3'),(6,6,1,NULL,NULL,1,'2019-09-25 13:44:12','2019-09-25 13:44:12','a377bf73-537d-4e2a-8465-1da0a5d2bdab'),(7,7,1,NULL,NULL,1,'2019-09-25 13:44:31','2019-09-25 13:44:31','5162cd52-9036-4391-9b65-84630b095714'),(8,8,1,'home','__home__',1,'2019-09-25 13:46:03','2019-09-25 13:46:03','a5a37fdc-2a02-4bf1-a379-541cb2a7d8d3'),(9,9,1,NULL,NULL,1,'2019-09-25 14:06:39','2019-09-25 14:06:39','6e7b3c8d-6eb8-4ad3-80ce-769ab2254fa8'),(10,10,1,NULL,NULL,1,'2019-09-25 14:10:36','2019-09-25 14:10:36','8d432330-f22c-4d55-9fb8-0a47f008d451'),(11,11,1,NULL,NULL,1,'2019-09-25 14:16:05','2019-09-25 14:16:05','7d1a3a95-5613-46cd-b654-7463e02b4f61'),(13,13,1,'about-us','about-us',1,'2019-09-25 14:23:53','2019-09-25 14:23:55','6a8fcb87-71b1-402b-8c56-c59526848470'),(14,14,1,'about-us','about-us',1,'2019-09-25 14:23:53','2019-09-25 14:23:53','6e8f5dbe-f51b-4ac2-ab1e-4613df0e31b0'),(16,16,1,'our-priorities','our-priorities',1,'2019-09-25 14:24:14','2019-09-25 14:24:15','f868e345-7b92-4537-9236-ab71f0ac58ca'),(17,17,1,'our-priorities','our-priorities',1,'2019-09-25 14:24:14','2019-09-25 14:24:14','caae6324-fc72-405c-b58b-6e9fb180c82e'),(18,18,1,'our-priorities','our-priorities',1,'2019-09-25 14:24:20','2019-09-25 14:24:20','c1edc206-6907-4c1b-b309-a0f9916cb1f5'),(19,19,1,'about-us','about-us',1,'2019-09-25 14:24:27','2019-09-25 14:24:27','f3669047-cc32-4d51-8389-f2a12381d70a'),(21,21,1,'how-we-fund','how-we-fund',1,'2019-09-25 14:24:35','2019-09-25 14:24:36','d606757c-707c-49b4-b452-4dd5cdff4179'),(22,22,1,'how-we-fund','how-we-fund',1,'2019-09-25 14:24:35','2019-09-25 14:24:35','58ed0622-b6d1-4012-b357-5b728af87d3f'),(24,24,1,'grantees','grantees',1,'2019-09-25 14:24:51','2019-09-25 14:24:53','c23669aa-588d-44e8-a885-476753ebfd49'),(25,25,1,'grantees','grantees',1,'2019-09-25 14:24:51','2019-09-25 14:24:51','712add39-afdc-4ebf-8825-3fcda77b8337'),(27,27,1,'apply','apply',1,'2019-09-25 14:24:57','2019-09-25 14:24:58','e38fb5c5-72e1-46b7-b427-e9f9e28498e1'),(28,28,1,'apply','apply',1,'2019-09-25 14:24:57','2019-09-25 14:24:57','0b9337e5-89ca-43ce-8fa4-1f5719d12fee'),(30,30,1,'help-advice','help-advice',1,'2019-09-25 14:34:00','2019-09-25 14:34:01','937d01cc-d995-4cf9-99dc-23cb07b34a1c'),(31,31,1,'help-advice','help-advice',1,'2019-09-25 14:34:00','2019-09-25 14:34:00','168e8280-3452-4d1d-846c-299245fb5a8f'),(33,33,1,'faqs','faqs',1,'2019-09-25 14:34:05','2019-09-25 14:34:06','af7fea0e-14d2-47e6-8ebb-6448e2bef50e'),(34,34,1,'faqs','faqs',1,'2019-09-25 14:34:05','2019-09-25 14:34:05','0af27c05-2680-4246-8df1-d9837346f93b'),(36,36,1,'contact','contact',1,'2019-09-25 14:34:11','2019-09-25 14:34:13','e82bd90c-2d07-4acb-88fe-14fd9ac9e092'),(37,37,1,'contact','contact',1,'2019-09-25 14:34:11','2019-09-25 14:34:11','22b4a7f8-3a3b-488f-9246-4417e047e4a3'),(38,38,1,'migrants-refugees-and-asylum-seekers',NULL,1,'2019-09-25 14:56:09','2019-09-25 14:56:10','47aa7c27-e05c-4b77-bc72-416f1694689c'),(39,39,1,'criminal-justice-and-penal-reform',NULL,1,'2019-09-25 14:56:16','2019-09-25 14:56:17','32768e78-e346-42a2-b017-666163daac12'),(40,40,1,'access-to-justice',NULL,1,'2019-09-25 14:56:21','2019-09-25 14:56:22','af494831-50b2-412a-b729-2be67c1c0632'),(42,42,1,'trustees','trustees',1,'2019-09-25 14:57:05','2019-10-17 10:25:34','eef7ab4f-3e76-483f-a35e-c38e6db4f61f'),(43,43,1,'trustees','trustees',1,'2019-09-25 14:57:05','2019-09-25 14:57:05','eedb8133-cc72-4721-9d00-418a336bbcfd'),(45,45,1,'staff','staff',1,'2019-09-25 14:57:14','2019-10-17 10:25:34','9cb526c6-6674-4452-8ebf-e49b435c0453'),(46,46,1,'staff','staff',1,'2019-09-25 14:57:14','2019-09-25 14:57:14','d2c5cc37-3423-4c01-a3f8-08cbf54e18af'),(48,48,1,'grants-awarded','grants-awarded',1,'2019-09-25 14:57:45','2019-10-17 10:25:34','86b696af-71a4-4e65-abd9-1529ab687181'),(49,49,1,'grants-awarded','grants-awarded',1,'2019-09-25 14:57:45','2019-09-25 14:57:45','998325ee-5a17-4e68-b0d2-212dd7bd0a43'),(51,51,1,'information-for-grantees','information-for-grantees',1,'2019-09-25 14:57:56','2019-10-17 10:25:34','61ded30d-2313-4ba0-9560-f8d9b0a76a32'),(52,52,1,'information-for-grantees','information-for-grantees',1,'2019-09-25 14:57:56','2019-09-25 14:57:56','f950d46e-0bd4-4a71-8f89-25c1f7972b9a'),(54,54,1,'bonavero-institute','bonavero-institute',1,'2019-09-25 14:58:01','2019-10-17 10:25:34','bcaa186f-6115-4a40-8cf9-60b4ebba3ad5'),(55,55,1,'bonavero-institute','bonavero-institute',1,'2019-09-25 14:58:01','2019-09-25 14:58:01','b6b443ec-f128-431a-80aa-22cea70aeca2'),(56,56,1,'1',NULL,1,'2019-09-26 17:16:46','2019-09-26 17:16:46','92f8c99c-1979-4217-82b3-dc03b1a0715b'),(57,57,1,'1',NULL,1,'2019-09-26 17:17:00','2019-09-26 17:17:00','54d02ab1-500b-458c-b340-042a3aaac3d9'),(58,58,1,'1',NULL,1,'2019-09-26 17:17:00','2019-09-26 17:17:00','6e97fce0-ac0b-48c8-b0c5-b84e0755f86e'),(59,59,1,'1',NULL,1,'2019-09-26 17:17:01','2019-09-26 17:17:01','afef4ded-c943-4a8a-9d23-f79c946f0855'),(60,60,1,'1',NULL,1,'2019-09-26 17:17:01','2019-09-26 17:17:01','62471652-84e6-4ff3-825f-a51ea193510f'),(61,61,1,'1',NULL,1,'2019-09-26 17:17:01','2019-09-26 17:17:01','c8c4feae-20d5-45ee-8742-39898ab978dc'),(62,62,1,'1',NULL,1,'2019-09-26 17:17:59','2019-09-26 17:17:59','4e0a44b0-0d5f-4f72-bffb-61f242769046'),(63,63,1,'1',NULL,1,'2019-09-26 17:18:00','2019-09-26 17:18:00','ec73a2a0-3d97-44ff-b021-f3f4601f0b71'),(64,64,1,'1',NULL,1,'2019-09-26 17:18:00','2019-09-26 17:18:00','3c8bc99a-a92e-4e89-88f5-0e607cec347e'),(65,65,1,NULL,NULL,1,'2019-09-26 17:19:47','2019-09-26 17:19:47','64adabe5-769b-47d2-a007-a141abd329d2'),(66,66,1,'apply','apply',1,'2019-10-17 10:36:53','2019-10-17 10:36:53','8b1e8304-4e21-4320-8afe-1690745f99cc'),(68,68,1,'migrants-refugees-and-asylum-seekers','grants/migrants-refugees-and-asylum-seekers',1,'2019-12-12 17:53:01','2019-12-12 17:53:01','2fbf05f9-92cc-424d-9be3-359141b1046a'),(69,69,1,'migrants-refugees-and-asylum-seekers','grants/migrants-refugees-and-asylum-seekers',1,'2019-12-12 17:53:01','2019-12-12 17:53:01','96b9b26c-5d4e-4633-a017-35f5d5ea6a38'),(71,71,1,'criminal-justice-and-penal-reform','grants/criminal-justice-and-penal-reform',1,'2019-12-12 17:53:07','2019-12-12 17:53:07','7eb556ca-e97f-4c39-bf73-56bd2e7351ca'),(72,72,1,'criminal-justice-and-penal-reform','grants/criminal-justice-and-penal-reform',1,'2019-12-12 17:53:07','2019-12-12 17:53:07','19d167e9-3aa7-4162-868c-6e9b112f5fea'),(74,74,1,'human-rights-particularly-access-to-justice','grants/human-rights-particularly-access-to-justice',1,'2019-12-12 17:53:16','2019-12-12 17:53:16','122e3b33-83a6-4018-82da-38cb07150606'),(75,75,1,'human-rights-particularly-access-to-justice','grants/human-rights-particularly-access-to-justice',1,'2019-12-12 17:53:16','2019-12-12 17:53:16','d7294081-c51e-480e-bb95-49a39f292043'),(76,76,1,'home','__home__',1,'2019-12-12 18:56:24','2019-12-12 18:56:24','42ddbe93-9029-42ef-8b8d-16b330596e9b'),(77,77,1,'home','__home__',1,'2019-12-12 18:56:31','2019-12-12 18:56:31','5d9fcb57-7ad0-4459-874c-37dc620116e4'),(78,78,1,NULL,NULL,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','3fb474a7-bef9-4db5-91bb-4390fd184c48'),(79,79,1,NULL,NULL,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','977c41d0-0aad-4cc8-af60-1094ecdda51e'),(80,80,1,'home','__home__',1,'2019-12-12 19:01:06','2019-12-12 19:01:06','cce1cbed-7031-4a22-9829-29ac1f79176a'),(81,81,1,NULL,NULL,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','5eb35b70-bbba-421c-a8a2-5d055cbbab4c'),(82,82,1,NULL,NULL,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','dc23573e-1dd5-40c1-9c71-2e7a8553a387'),(83,83,1,'home','__home__',1,'2019-12-12 19:01:17','2019-12-12 19:01:17','72193ec0-f305-44fb-b659-6c0d2d28a084'),(84,84,1,NULL,NULL,1,'2019-12-12 19:01:17','2019-12-12 19:01:17','d674728d-180a-45b8-b3d9-39fac53a718b'),(85,85,1,NULL,NULL,1,'2019-12-12 19:01:17','2019-12-12 19:01:17','4528a8b0-3a8a-4e24-aac7-84b6d1ecca7d'),(86,86,1,NULL,NULL,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','1f1017f1-9d16-4a28-b71a-334144090ff3'),(87,87,1,'home','__home__',1,'2019-12-12 19:01:48','2019-12-12 19:01:48','56fdab24-1470-4ab4-a6c6-b129e2269a89'),(88,88,1,NULL,NULL,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','e7f08231-57e4-49cb-8e2f-cf34ac33b4e1'),(89,89,1,NULL,NULL,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','487277c3-2e35-47be-8260-5172f16b4c9f'),(90,90,1,NULL,NULL,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','dbf1843d-bbd8-4c60-b1ba-d622bb7a191e'),(91,91,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','445227b0-111b-4f90-8afc-2cf701f7cff7'),(92,92,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','a85cca75-4b18-4cd7-b66c-2692dada6511'),(93,93,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','0b64fba1-a824-4db7-b356-9feb2cb33eb2'),(94,94,1,'home','__home__',1,'2019-12-12 19:03:40','2019-12-12 19:03:40','dc97c51e-f9a5-402d-9f5a-e0715229f53a'),(95,95,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','3ae5dcd0-3e4d-47bd-bac8-603e5ba3e8e3'),(96,96,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','bf6dd6c4-5597-48b7-854c-350c20ad5d54'),(97,97,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','3b3c7caf-9fdb-41ed-aa1b-8f58de12ccc9'),(98,98,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','b0812841-2506-4e73-9131-ed67256cb347'),(99,99,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','eab25368-4aae-46e8-86f4-f394e719d5cb'),(100,100,1,NULL,NULL,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','19de6c80-fb50-43ef-95e9-714e10a1413b'),(101,101,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','8d520746-973c-4391-bc95-b264ff489643'),(102,102,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','fd876ea0-ce05-4fa5-a289-2a1ef66ce989'),(103,103,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','0798c00c-f60e-4fc5-89d5-488c554ead72'),(104,104,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','adb03c65-19a8-49e7-85b1-a365fea54000'),(105,105,1,'home','__home__',1,'2019-12-12 19:05:20','2019-12-12 19:05:20','b8c7a0a0-aa2e-4fd5-bdf8-b8ecd875a0f9'),(106,106,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','67ee594d-5631-4e5f-9140-7294a6113a35'),(107,107,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','b6268447-6930-4ec6-94ff-463899c5b292'),(108,108,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','916da33b-b65d-49d8-9562-641074d5d2f4'),(109,109,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','1ed81be4-1441-42c3-935b-8ae3a4be20a1'),(110,110,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','1d275223-3a25-48e1-bd91-52d5a7566bdb'),(111,111,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','e5ce82f3-8abe-47b9-920a-dbb691c31b3d'),(112,112,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','94db125f-48e3-4293-8f9b-716aef8f69d6'),(113,113,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','b5ac736f-0e91-49da-b375-490b24cef0c7'),(114,114,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','5a7c75d5-9e42-473b-b836-4425a186e78b'),(115,115,1,NULL,NULL,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','0b94d63f-2e80-4324-95b5-2809c695d82b'),(116,116,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','c4c082aa-d3c8-4f7d-bff9-e75fb1ec1870'),(117,117,1,'home','__home__',1,'2019-12-12 19:05:53','2019-12-12 19:05:53','b4f2bf6e-0546-44e7-af85-9916606e63bd'),(118,118,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','3de052dd-8fdc-47c1-946a-607a043f9acf'),(119,119,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','784e605c-0c08-4d8e-a3b2-515f12fe2b66'),(120,120,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','d75f8895-eb30-4816-8648-13709a6efbb9'),(121,121,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','b29bb0f2-c238-41a0-bf2f-e1ba5ced280b'),(122,122,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','24a07ef9-3c4f-4e35-b06c-bc43aa4c4237'),(123,123,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','1702bc5a-d30c-46e3-9e15-5d250125c69f'),(124,124,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','711a15af-7ffd-4389-8c18-6715471e9c77'),(125,125,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','ff025152-f4b9-46b9-9fbc-8c11a2a5c820'),(126,126,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','a88dbfbc-30f4-4b78-a309-771875b08b57'),(127,127,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','75cfeae3-7fcb-4d25-a8e1-3b665b468905'),(128,128,1,NULL,NULL,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','8f57581d-5eed-4798-bd6c-0e5f0edeeb71'),(129,129,1,'home','__home__',1,'2019-12-16 11:57:28','2019-12-16 11:57:28','5b097b53-c442-4b23-9610-053443b7c333'),(130,130,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','5fbcc2ca-f650-4397-9a1c-f8f2ceb5f52e'),(131,131,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','af1f4820-6e91-41f1-bb58-55cfe2653983'),(132,132,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','ab723c65-5a3d-421f-84de-f8fdf6ae3d44'),(133,133,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','10889878-1c0c-4144-a1dd-90ebf520fd3e'),(134,134,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','f28feddc-dcde-44a4-ab31-9499a0dd18be'),(135,135,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','c411744b-df21-4e21-9196-785dd34ce99b'),(136,136,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','84962838-cbc7-4685-9bfe-931174077be9'),(137,137,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','10533a4b-670e-4b7c-98fa-2d56465717bc'),(138,138,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','355a77bb-d0b5-4a83-99df-8e942bbf0221'),(139,139,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','bdca1269-3a46-4def-a323-4fc1bf4ba083'),(140,140,1,NULL,NULL,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','fb8a9f1b-679a-4e4e-8051-477dad3a9dbd');
/*!40000 ALTER TABLE `elements_sites` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `entries` VALUES (2,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-09-23 18:30:14','2019-09-23 18:30:14','6150d5a6-18cd-4f96-aee0-d6f9697a3eba'),(3,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-09-23 18:30:14','2019-09-23 18:30:14','31ea64a5-564b-46f7-a8e1-ae51dfb9ee1a'),(4,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-09-23 18:30:22','2019-09-23 18:30:22','b5b7daa3-f420-47dc-bb7c-366de56511cd'),(8,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-09-25 13:46:03','2019-09-25 13:46:03','bdc08521-27c8-4350-ae18-941d2bd8446d'),(13,1,NULL,1,1,'2019-09-25 14:23:00',NULL,NULL,'2019-09-25 14:23:53','2019-09-25 14:23:53','dcb96d67-46ae-4faa-8c43-b8af2961acc3'),(14,1,NULL,1,1,'2019-09-25 14:23:00',NULL,NULL,'2019-09-25 14:23:53','2019-09-25 14:23:53','b6d7b84e-cbf5-43a4-b6c9-8360668aa4ea'),(16,1,NULL,1,1,'2019-09-25 14:23:00',NULL,NULL,'2019-09-25 14:24:14','2019-09-25 14:24:14','c3724f61-1612-4b68-9d02-f51691e4caad'),(17,1,NULL,1,1,'2019-09-25 14:23:00',NULL,NULL,'2019-09-25 14:24:14','2019-09-25 14:24:14','5a0f14fd-36fa-428b-a2a0-311b6cb98b06'),(18,1,NULL,1,1,'2019-09-25 14:23:00',NULL,NULL,'2019-09-25 14:24:20','2019-09-25 14:24:20','7f03101e-cea9-420e-a45c-094599ba215e'),(19,1,NULL,1,1,'2019-09-25 14:23:00',NULL,NULL,'2019-09-25 14:24:27','2019-09-25 14:24:27','bd524969-5f75-42a3-94f9-ddb2164aa9f6'),(21,1,NULL,1,1,'2019-09-25 14:24:00',NULL,NULL,'2019-09-25 14:24:35','2019-09-25 14:24:35','331d1b89-ec50-4265-9e46-093625aa00aa'),(22,1,NULL,1,1,'2019-09-25 14:24:00',NULL,NULL,'2019-09-25 14:24:35','2019-09-25 14:24:35','28c22830-0fab-4819-9d37-f5908970553b'),(24,1,NULL,1,1,'2019-09-25 14:24:00',NULL,NULL,'2019-09-25 14:24:51','2019-09-25 14:24:51','154ac023-3ab9-43a6-bca8-3c7b8993838f'),(25,1,NULL,1,1,'2019-09-25 14:24:00',NULL,NULL,'2019-09-25 14:24:51','2019-09-25 14:24:51','be17daaf-7f81-4ad7-b8aa-769e618b7cbb'),(27,1,NULL,3,1,'2019-09-25 14:24:00',NULL,NULL,'2019-09-25 14:24:57','2019-10-17 10:36:53','0b71d365-bcc1-400e-ac3e-9e8b2b4d7e7f'),(28,1,NULL,1,1,'2019-09-25 14:24:00',NULL,NULL,'2019-09-25 14:24:57','2019-09-25 14:24:57','2c7844dd-565a-4818-8aac-21d76f6c5de8'),(30,1,NULL,1,1,'2019-09-25 14:33:00',NULL,NULL,'2019-09-25 14:34:00','2019-09-25 14:34:00','f4ebc3dc-f0fb-4657-9e48-16dcd65ebd8f'),(31,1,NULL,1,1,'2019-09-25 14:33:00',NULL,NULL,'2019-09-25 14:34:00','2019-09-25 14:34:00','242d583c-3b03-49e9-88df-a1a8c9abf832'),(33,1,NULL,1,1,'2019-09-25 14:34:00',NULL,NULL,'2019-09-25 14:34:05','2019-09-25 14:34:05','0a382fa8-18cd-45ad-a08d-d80f89e668e1'),(34,1,NULL,1,1,'2019-09-25 14:34:00',NULL,NULL,'2019-09-25 14:34:05','2019-09-25 14:34:05','ec4543ec-0b64-49e4-a7f9-aa12777a81fd'),(36,1,NULL,1,1,'2019-09-25 14:34:00',NULL,NULL,'2019-09-25 14:34:11','2019-09-25 14:34:11','99b782f7-831b-4eef-9afe-a99cd3c95db8'),(37,1,NULL,1,1,'2019-09-25 14:34:00',NULL,NULL,'2019-09-25 14:34:11','2019-09-25 14:34:11','da7b39d0-74bf-437d-bb3a-e1f1a395a4c2'),(42,1,NULL,1,1,'2019-09-25 14:56:00',NULL,NULL,'2019-09-25 14:57:05','2019-09-25 14:57:05','34ea7996-6cee-4ce6-95a9-3d2d0579e7b5'),(43,1,NULL,1,1,'2019-09-25 14:56:00',NULL,NULL,'2019-09-25 14:57:05','2019-09-25 14:57:05','037d72db-d82b-4c75-9e0e-df939e46a49d'),(45,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:57:14','2019-09-25 14:57:14','b86a9e59-1c9e-4e71-b3e6-792055b9049d'),(46,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:57:14','2019-09-25 14:57:14','f7e7d18f-d4a6-4cd5-8688-e44386969963'),(48,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:57:45','2019-09-25 14:57:45','8f08e431-7df6-4e52-a927-9b9e22e67399'),(49,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:57:45','2019-09-25 14:57:45','f4403709-793a-4b54-80d0-be0caf259963'),(51,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:57:56','2019-09-25 14:57:56','56c0ae09-7c4c-45a7-8012-2d26e132fe42'),(52,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:57:56','2019-09-25 14:57:56','9345974e-24d1-4a7d-abb8-4e0f9a9c6ead'),(54,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:58:01','2019-09-25 14:58:01','7f536dee-5260-4d1e-bfa7-2fb58960414c'),(55,1,NULL,1,1,'2019-09-25 14:57:00',NULL,NULL,'2019-09-25 14:58:01','2019-09-25 14:58:01','009a131e-d551-4fa5-9613-2a3e3a49a763'),(66,1,NULL,3,1,'2019-09-25 14:24:00',NULL,NULL,'2019-10-17 10:36:53','2019-10-17 10:36:53','99f9cec9-1cd6-45b8-b955-59a577fdbecc'),(68,6,NULL,7,1,'2019-12-12 17:52:00',NULL,NULL,'2019-12-12 17:53:01','2019-12-12 17:53:01','6c85e5cd-69d5-417a-9345-54808c0aa926'),(69,6,NULL,7,1,'2019-12-12 17:52:00',NULL,NULL,'2019-12-12 17:53:01','2019-12-12 17:53:01','a5edd560-6078-47da-9795-cca75db88194'),(71,6,NULL,7,1,'2019-12-12 17:53:00',NULL,NULL,'2019-12-12 17:53:07','2019-12-12 17:53:07','2a155318-615a-4262-b6af-56352d49535d'),(72,6,NULL,7,1,'2019-12-12 17:53:00',NULL,NULL,'2019-12-12 17:53:07','2019-12-12 17:53:07','6c9d3c26-5643-4abf-83e6-fa521f179917'),(74,6,NULL,7,1,'2019-12-12 17:53:00',NULL,NULL,'2019-12-12 17:53:16','2019-12-12 17:53:16','d0bb5110-1359-490e-b0d8-4451b4f184a7'),(75,6,NULL,7,1,'2019-12-12 17:53:00',NULL,NULL,'2019-12-12 17:53:16','2019-12-12 17:53:16','2e1ba3f9-b65f-4afe-bf52-c48991a01334'),(76,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 18:56:24','2019-12-12 18:56:24','affc263f-9d55-49e4-b610-191b88801061'),(77,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 18:56:31','2019-12-12 18:56:31','a6284c2d-b3f4-4468-985f-6ac005b43225'),(80,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 19:01:06','2019-12-12 19:01:06','fe79890b-0b2f-4354-9008-48f82d18fc49'),(83,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 19:01:17','2019-12-12 19:01:17','f92d43fa-3961-4a47-a0e1-84a25484a72e'),(87,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 19:01:48','2019-12-12 19:01:48','976084f3-7518-435c-aafb-f670af86ebb1'),(94,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','db6488eb-b249-4da4-b879-29ab8f112672'),(105,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','0afcc300-6438-41e1-b260-348c76336d59'),(117,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','43dcc7f7-8c68-4b31-8644-912e75e5fffd'),(129,2,NULL,2,NULL,'2019-09-23 18:30:00',NULL,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','ca2e8594-7089-4727-86e6-393ba9bc1ef6');
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `entrytypes`
--

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `entrytypes` VALUES (1,1,5,'Default','default',1,'Title','',1,'2019-09-23 18:29:58','2019-10-17 10:25:32',NULL,'c9a31744-8fcd-4d59-a850-2fd1bfbd4df8'),(2,2,6,'Home','home',0,'','{section.name|raw}',1,'2019-09-23 18:30:14','2019-12-12 18:56:31',NULL,'a93b64d5-1fb1-49f2-9b47-07f9cb3b608d'),(3,1,14,'Apply','apply',1,'Title','',2,'2019-10-17 10:25:58','2019-10-17 10:25:58',NULL,'22415549-5355-4cc9-a0db-3aadd2b6b23d'),(4,3,17,'Grants Awarded','grantsAwarded',1,'Title','',1,'2019-12-12 17:38:35','2019-12-12 18:32:13',NULL,'634ccb16-716f-4e6a-be95-bdd57734a9c5'),(5,4,15,'Staff','staff',1,'Title','',1,'2019-12-12 17:38:56','2019-12-12 17:56:20',NULL,'8ce3cc02-02ed-4ef4-b20a-5082a3395f46'),(6,5,16,'Trustees','trustees',1,'Title','',1,'2019-12-12 17:39:09','2019-12-12 18:15:03',NULL,'17aec3eb-db2b-488d-8fde-0aaf9c2b3c1e'),(7,6,26,'Grants','grants',1,'Title','',1,'2019-12-12 17:40:10','2019-12-17 17:25:47',NULL,'ccdc057b-8c99-44bf-9cc1-560510bb41c1');
/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `feedme_feeds`
--

LOCK TABLES `feedme_feeds` WRITE;
/*!40000 ALTER TABLE `feedme_feeds` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `feedme_feeds` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `fieldgroups`
--

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `fieldgroups` VALUES (2,'SEO','2019-09-23 18:21:17','2019-09-23 18:21:17','76483afb-3281-40c6-987f-d0c1a94bfcf0'),(3,'Sharing','2019-09-23 18:21:21','2019-09-23 18:21:21','5941ef4b-875a-44b2-8fdf-7c54bbcdb5be'),(4,'Default','2019-09-23 18:34:07','2019-09-23 18:34:07','e113262b-f370-415c-bca6-a906fba2f07c'),(5,'Global','2019-09-25 14:08:25','2019-09-25 14:08:25','028f4556-1943-4e08-bb16-417176659e00'),(6,'Staff','2019-12-12 17:55:18','2019-12-12 17:55:18','7976b1a3-64c2-4ad1-bde0-359d215514e8'),(7,'Grants','2019-12-12 18:20:32','2019-12-12 18:20:32','c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc');
/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `fieldlayoutfields`
--

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `fieldlayoutfields` VALUES (3,2,3,2,0,2,'2019-09-25 13:43:56','2019-09-25 13:43:56','39f60424-35c5-49d1-9b5a-92b4ba501f29'),(4,2,3,3,0,1,'2019-09-25 13:43:56','2019-09-25 13:43:56','9adbf28b-c976-4d85-bc24-e7849d34718a'),(5,3,4,8,0,1,'2019-09-25 13:44:12','2019-09-25 13:44:12','a6e2f0d3-b2d8-404d-ae92-87817601e211'),(6,3,4,7,0,3,'2019-09-25 13:44:12','2019-09-25 13:44:12','dd074607-a96e-48fd-aba0-89e12d414044'),(7,3,4,6,0,2,'2019-09-25 13:44:12','2019-09-25 13:44:12','99f35ba2-152a-4d23-98a9-338944e78b54'),(8,4,5,5,0,2,'2019-09-25 13:44:31','2019-09-25 13:44:31','cb4ab227-55f3-4258-8995-617e576c20eb'),(9,4,5,4,0,1,'2019-09-25 13:44:31','2019-09-25 13:44:31','cc7ea160-57db-4d39-a476-d176ed06ed24'),(25,8,13,12,0,1,'2019-09-25 14:09:21','2019-09-25 14:09:21','8c5ce105-bdf7-49df-840e-7c6581636db7'),(26,9,14,15,0,1,'2019-09-25 14:10:36','2019-09-25 14:10:36','0c440ed0-ecc7-4823-9248-3216cf45690c'),(29,7,16,13,0,1,'2019-09-26 17:18:35','2019-09-26 17:18:35','fa38764c-887e-4347-b19e-1451d65d8b98'),(30,7,16,14,0,2,'2019-09-26 17:18:35','2019-09-26 17:18:35','2ae2d1e0-ad58-480b-a20b-3878d07de277'),(60,5,31,9,0,1,'2019-10-17 10:25:32','2019-10-17 10:25:32','9546dbc6-8218-44ee-b162-4d284b2762a6'),(61,5,32,2,0,2,'2019-10-17 10:25:32','2019-10-17 10:25:32','d66bfe66-88a2-48a1-93d3-aa7a30846118'),(62,5,32,3,0,1,'2019-10-17 10:25:32','2019-10-17 10:25:32','d79f366d-664f-4b1d-b7d6-c63614a6d9c6'),(63,5,32,1,0,3,'2019-10-17 10:25:32','2019-10-17 10:25:32','886fe581-4951-4f53-9b69-99e38aa7df50'),(64,5,33,8,0,1,'2019-10-17 10:25:32','2019-10-17 10:25:32','17e042ed-891b-4df2-af0b-419502129746'),(65,5,33,7,0,3,'2019-10-17 10:25:32','2019-10-17 10:25:32','7908aad2-7422-4039-bd21-3eb11a6ad93f'),(66,5,33,6,0,2,'2019-10-17 10:25:32','2019-10-17 10:25:32','fd01b507-a546-457d-885a-268fb86b3439'),(67,14,34,9,0,1,'2019-10-17 10:25:58','2019-10-17 10:25:58','cd5b56ad-f2c5-4a5b-a6b2-f83eb8ff08b0'),(68,14,35,2,0,2,'2019-10-17 10:25:58','2019-10-17 10:25:58','0862357e-6585-4a08-9c16-6d0d88140a6b'),(69,14,35,3,0,1,'2019-10-17 10:25:58','2019-10-17 10:25:58','54284ee7-8b16-465c-928d-0e672a927912'),(70,14,35,1,0,3,'2019-10-17 10:25:58','2019-10-17 10:25:58','f6dde111-048c-4ed2-b42e-821ef69878cc'),(71,14,36,8,0,1,'2019-10-17 10:25:58','2019-10-17 10:25:58','ee25973f-e3de-4462-9ae2-dc8ea5b0ec68'),(72,14,36,7,0,3,'2019-10-17 10:25:58','2019-10-17 10:25:58','fb7189df-cf15-414f-b0d7-53a7e97f5698'),(73,14,36,6,0,2,'2019-10-17 10:25:58','2019-10-17 10:25:58','93cb3659-bff0-4791-8881-85006a9de749'),(75,15,38,29,0,3,'2019-12-12 17:56:20','2019-12-12 17:56:20','63cd5203-bd22-434d-9027-ebf3f8bc9554'),(76,15,38,28,0,2,'2019-12-12 17:56:20','2019-12-12 17:56:20','9c314cfa-d7a9-4f62-acc1-f09091cfbd6b'),(77,15,38,15,0,1,'2019-12-12 17:56:20','2019-12-12 17:56:20','a7f0b5d7-e03f-4bd2-95b8-e6c7e9efd19e'),(78,16,39,29,0,3,'2019-12-12 18:15:03','2019-12-12 18:15:03','787a9079-f2d2-4c42-9411-c80d3af76f21'),(79,16,39,28,0,2,'2019-12-12 18:15:03','2019-12-12 18:15:03','5180dba3-c541-4748-98b5-9b7fb6e1762a'),(80,16,39,15,0,1,'2019-12-12 18:15:03','2019-12-12 18:15:03','8cf4cabd-ff26-41a0-a776-06650504db47'),(81,17,40,30,0,3,'2019-12-12 18:32:13','2019-12-12 18:32:13','f9d27b42-c5ff-4b6a-b3ef-9bb801ef3dd7'),(82,17,40,34,0,5,'2019-12-12 18:32:13','2019-12-12 18:32:13','dd07f867-8e4f-4b63-ab11-c5dcd03087e8'),(83,17,40,33,0,4,'2019-12-12 18:32:13','2019-12-12 18:32:13','fecee174-d316-4738-ae63-7004e0ae2fe9'),(84,17,40,31,0,1,'2019-12-12 18:32:13','2019-12-12 18:32:13','4c77c796-beae-4538-8bc1-70c04d571d27'),(85,17,40,32,0,2,'2019-12-12 18:32:13','2019-12-12 18:32:13','5bbd2bab-d663-4a20-a52a-f16b7cfe50e0'),(86,18,41,37,0,2,'2019-12-12 18:42:29','2019-12-12 18:42:29','841c2338-70cf-4cd8-b4a0-0750a794dbec'),(87,18,41,38,0,3,'2019-12-12 18:42:29','2019-12-12 18:42:29','cb4d9800-5706-4446-8cfe-410b87cb3b5f'),(88,18,41,39,0,1,'2019-12-12 18:42:29','2019-12-12 18:42:29','724c8563-f92b-48d0-996c-afbd451af5ca'),(138,6,60,9,0,2,'2019-12-12 18:56:31','2019-12-12 18:56:31','7e679917-7dc9-4c79-8a0c-b9aade1dbb70'),(139,6,60,36,0,1,'2019-12-12 18:56:31','2019-12-12 18:56:31','cb5537e8-85de-4d30-9f56-d44b42fb4c28'),(140,6,61,2,0,2,'2019-12-12 18:56:31','2019-12-12 18:56:31','51358c3e-63ab-40ab-b8fa-9f0f7de4d33d'),(141,6,61,3,0,1,'2019-12-12 18:56:31','2019-12-12 18:56:31','a353c63f-97a2-416d-b170-371d1abf955b'),(142,6,62,8,0,1,'2019-12-12 18:56:31','2019-12-12 18:56:31','c0b6e8ad-6d2d-4a48-9bb4-c25e2ecb192a'),(143,6,62,7,0,3,'2019-12-12 18:56:31','2019-12-12 18:56:31','e2e81979-bbd5-4251-99c5-7d733e889274'),(144,6,62,6,0,2,'2019-12-12 18:56:31','2019-12-12 18:56:31','5da00923-f950-4765-886b-43a84a626222'),(360,19,127,40,0,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','3a9c9b22-1275-4642-83d7-0d439b1f1164'),(361,19,127,41,0,2,'2019-12-16 11:56:32','2019-12-16 11:56:32','5d3b7af6-719b-456e-87e4-dbb05ce3c707'),(362,23,128,61,0,3,'2019-12-16 11:56:32','2019-12-16 11:56:32','dc71c917-e5db-4c22-b23f-b7a823a14169'),(363,23,128,62,0,2,'2019-12-16 11:56:32','2019-12-16 11:56:32','bcf5ee7b-5f6b-411a-9eef-39a2afacfdcc'),(364,23,128,63,0,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','ca91e0e4-c0be-4507-9129-939cedc971c9'),(365,24,129,59,0,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','f44a7b33-856e-48c3-9d2a-aea821398d28'),(366,24,129,60,0,3,'2019-12-16 11:56:32','2019-12-16 11:56:32','f3d1ecad-d516-4de4-ab0a-ef210ef2e6f9'),(367,24,129,64,0,2,'2019-12-16 11:56:32','2019-12-16 11:56:32','df1fbf9c-008a-4d3e-bd3d-e0e27928c0bb'),(368,24,129,65,0,4,'2019-12-16 11:56:32','2019-12-16 11:56:32','35998c0e-8b86-4088-9d3a-160e1c868443'),(369,20,130,45,0,8,'2019-12-16 11:56:32','2019-12-16 11:56:32','be4589f0-af80-4a00-a32e-9dbfe5269525'),(370,20,130,46,0,7,'2019-12-16 11:56:32','2019-12-16 11:56:32','c2ef3eda-f537-4948-a30a-212f2940f758'),(371,20,130,47,0,3,'2019-12-16 11:56:32','2019-12-16 11:56:32','bf084a3c-f711-46f3-8bb5-38261c1d8bd0'),(372,20,130,48,1,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','35b82171-027f-4dad-81fb-81aa6ab5156f'),(373,20,130,49,0,4,'2019-12-16 11:56:32','2019-12-16 11:56:32','c66d2d99-1fbf-4dc6-ba81-32aaef6a9ed3'),(374,20,130,50,0,6,'2019-12-16 11:56:32','2019-12-16 11:56:32','94da047c-8344-4c82-86d5-f3f532bcbfc9'),(375,20,130,51,0,5,'2019-12-16 11:56:32','2019-12-16 11:56:32','ca444aa5-5e2d-4b3b-9d98-470c1f88ab13'),(376,20,130,52,0,2,'2019-12-16 11:56:32','2019-12-16 11:56:32','36c0b107-6199-495f-b653-4e5bfc542751'),(377,21,131,66,0,2,'2019-12-16 11:56:32','2019-12-16 11:56:32','2d5ec789-0d30-4d13-9c55-0db2a93b3885'),(378,21,131,42,0,4,'2019-12-16 11:56:32','2019-12-16 11:56:32','a91500e2-2c71-41f4-98a5-3f9edfde2737'),(379,21,131,43,0,5,'2019-12-16 11:56:32','2019-12-16 11:56:32','9abd1a4e-76bc-41f3-b935-a38565d03ded'),(380,21,131,44,0,3,'2019-12-16 11:56:32','2019-12-16 11:56:32','4c0da44b-f4e4-4a6a-90b8-faf253ad4f6e'),(381,21,131,53,0,7,'2019-12-16 11:56:32','2019-12-16 11:56:32','33480139-5188-47d6-9ba8-7bb342abe9b7'),(382,21,131,54,0,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','8eaee903-51d2-4777-a4eb-76e2023aef47'),(383,21,131,55,0,6,'2019-12-16 11:56:32','2019-12-16 11:56:32','bc1f9c69-6fcd-4b98-8ae4-43c9a773781c'),(384,22,132,56,0,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','9e4f0a5f-c5c3-4d9f-a52e-5ae340dfa96f'),(385,22,132,57,0,3,'2019-12-16 11:56:32','2019-12-16 11:56:32','e462cca9-b276-46c1-8f47-aa35e772075e'),(386,22,132,58,0,2,'2019-12-16 11:56:32','2019-12-16 11:56:32','85174e46-e4d3-432d-91cd-ad17c18a30d5'),(387,1,133,10,0,1,'2019-12-16 11:56:32','2019-12-16 11:56:32','7a98a9a1-38d5-4df4-a16f-310f46d84529'),(388,10,134,16,0,2,'2019-12-16 11:56:33','2019-12-16 11:56:33','b4ce4c29-d88c-447f-97c6-4fefc11467fb'),(389,10,134,17,0,1,'2019-12-16 11:56:33','2019-12-16 11:56:33','64ebbdd3-295f-40ad-9cbc-372cdaf01c7f'),(390,11,135,25,0,4,'2019-12-16 11:56:33','2019-12-16 11:56:33','f44639ae-133c-4e57-9708-f708919cc03e'),(391,11,135,26,0,3,'2019-12-16 11:56:33','2019-12-16 11:56:33','901ebdc5-bf49-496d-8aff-e25e9ad49ae9'),(392,11,135,27,0,2,'2019-12-16 11:56:33','2019-12-16 11:56:33','e897de53-04a8-4dd0-a2ce-82b85cfa8063'),(393,11,135,22,0,1,'2019-12-16 11:56:33','2019-12-16 11:56:33','a16c5264-4ac9-46ce-8c23-825f7ff571fd'),(394,12,136,18,0,1,'2019-12-16 11:56:33','2019-12-16 11:56:33','131d1270-6aeb-42c2-afb0-7906d0da1105'),(395,12,136,21,0,2,'2019-12-16 11:56:33','2019-12-16 11:56:33','a5a27108-ab96-4a52-a046-9e3a50d8aba3'),(396,13,137,23,0,2,'2019-12-16 11:56:33','2019-12-16 11:56:33','6e067612-bec0-468f-92b3-b72c365c91de'),(397,13,137,24,0,1,'2019-12-16 11:56:33','2019-12-16 11:56:33','b9e07e6a-6091-44f7-a47e-33432b341670'),(398,25,138,68,0,1,'2019-12-17 16:16:59','2019-12-17 16:16:59','9fb51f01-28d5-4d67-a03e-6b4892ff298a'),(399,25,138,69,0,2,'2019-12-17 16:16:59','2019-12-17 16:16:59','aa2f1788-1d8e-4a5e-8f8f-8f6fe71ebe56'),(400,25,138,70,0,3,'2019-12-17 16:16:59','2019-12-17 16:16:59','63325afc-72ae-4904-b911-290bdbd60dc9'),(403,27,141,72,0,1,'2019-12-17 16:22:58','2019-12-17 16:22:58','c1c485a7-1451-4a03-b893-468402ca0689'),(404,27,141,73,0,2,'2019-12-17 16:22:58','2019-12-17 16:22:58','ba86832b-d3c7-4468-8a5c-744bd01e4764'),(408,28,145,75,0,1,'2019-12-17 17:25:35','2019-12-17 17:25:35','b30cf129-55fb-40b0-8de0-b527db1d3d6a'),(409,28,145,76,0,4,'2019-12-17 17:25:35','2019-12-17 17:25:35','0f713190-3c2a-44f1-a1db-6cc05e6b2d3e'),(410,28,145,77,0,2,'2019-12-17 17:25:35','2019-12-17 17:25:35','51f1e96c-7a80-42f1-9bc4-31cc21a92c0a'),(411,28,145,78,0,5,'2019-12-17 17:25:35','2019-12-17 17:25:35','dc79d02f-3990-4cd8-a83f-58f7d14ea39e'),(412,28,145,79,0,3,'2019-12-17 17:25:35','2019-12-17 17:25:35','8e9c4904-04c7-4dac-a1ac-00c262708d0d'),(416,26,149,67,0,1,'2019-12-17 17:25:47','2019-12-17 17:25:47','8f5ffcf6-0c47-4398-a196-5888dbfe51ee'),(417,26,150,71,0,1,'2019-12-17 17:25:47','2019-12-17 17:25:47','de1be08c-9633-4408-9810-a66e6c4dfedf'),(418,26,151,74,0,1,'2019-12-17 17:25:47','2019-12-17 17:25:47','6744b07b-82d0-4692-a473-35b179fd556a');
/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `fieldlayouts`
--

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `fieldlayouts` VALUES (1,'craft\\elements\\MatrixBlock','2019-09-23 18:36:27','2019-09-23 18:36:27',NULL,'2472b3fb-ea06-497f-9cb3-65e6ab5dcac0'),(2,'craft\\elements\\GlobalSet','2019-09-25 13:43:56','2019-09-25 13:43:56',NULL,'eae20891-5e05-422e-9beb-b4dca976d1ef'),(3,'craft\\elements\\GlobalSet','2019-09-25 13:44:12','2019-09-25 13:44:12',NULL,'e06d29f7-eee8-4a9f-a285-bd5a598eabdb'),(4,'craft\\elements\\GlobalSet','2019-09-25 13:44:31','2019-09-25 13:44:31',NULL,'ae427693-7c58-44fc-aee3-86a048f3e7c3'),(5,'craft\\elements\\Entry','2019-09-25 13:45:38','2019-09-25 13:45:38',NULL,'fbf9461b-5f08-4bcf-804e-27ae8fa856c6'),(6,'craft\\elements\\Entry','2019-09-25 13:46:03','2019-09-25 13:46:03',NULL,'bbc19fb3-2cbb-4ea6-b19f-9db818075b4f'),(7,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-09-25 14:09:12','2019-09-25 14:09:12',NULL,'142eedd9-6032-4808-9f46-0bb81a9ba721'),(8,'craft\\elements\\GlobalSet','2019-09-25 14:09:21','2019-09-25 14:09:21',NULL,'ded61cda-d38a-4764-b23d-ba184c20784b'),(9,'craft\\elements\\GlobalSet','2019-09-25 14:10:36','2019-09-25 14:10:36',NULL,'31c0ca5d-ce9c-4266-a818-daca2fc979bc'),(10,'craft\\elements\\MatrixBlock','2019-09-26 17:23:45','2019-09-26 17:23:45',NULL,'1b3ed2ea-891b-451f-bdc4-02c7e5c13b99'),(11,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-09-26 17:23:45','2019-09-26 17:23:45',NULL,'0c4e1b75-dc36-453e-a835-10b7aebcafaf'),(12,'craft\\elements\\MatrixBlock','2019-09-26 17:23:45','2019-09-26 17:23:45',NULL,'51fd2220-8d81-4cd1-a53a-a0c03ca05c19'),(13,'craft\\elements\\MatrixBlock','2019-09-26 17:24:40','2019-09-26 17:24:40',NULL,'06ad3361-a9dc-4884-85fe-97f420b4e366'),(14,'craft\\elements\\Entry','2019-10-17 10:25:58','2019-10-17 10:25:58',NULL,'8ac273ba-177c-40ef-92b0-fdd5d5b088df'),(15,'craft\\elements\\Entry','2019-12-12 17:56:04','2019-12-12 17:56:04',NULL,'ec7ea6da-f41b-492c-b188-923be792f541'),(16,'craft\\elements\\Entry','2019-12-12 18:15:03','2019-12-12 18:15:03',NULL,'0ace9fa7-4cba-44f5-abb2-ed05b8ba6279'),(17,'craft\\elements\\Entry','2019-12-12 18:32:13','2019-12-12 18:32:13',NULL,'ec76634b-89c9-419e-b80b-740e6ec775bf'),(18,'craft\\elements\\MatrixBlock','2019-12-12 18:42:29','2019-12-12 18:42:29',NULL,'82417801-9adf-44a1-bfa5-81007547c9d3'),(19,'craft\\elements\\MatrixBlock','2019-12-12 18:43:29','2019-12-12 18:43:29',NULL,'46611989-f47d-4102-ac87-a500530cc29c'),(20,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-12-12 18:51:05','2019-12-12 18:51:05',NULL,'881a7d78-9089-4dde-afe2-0cf15f0f3481'),(21,'craft\\elements\\MatrixBlock','2019-12-12 18:51:05','2019-12-12 18:51:05',NULL,'5a58956a-062e-464b-aa49-f19edfff9bce'),(22,'craft\\elements\\MatrixBlock','2019-12-12 18:51:05','2019-12-12 18:51:05',NULL,'49d340d2-2b2b-4c53-8eda-15b1c70cf429'),(23,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-12-12 18:59:24','2019-12-12 18:59:24',NULL,'e61676d9-8671-407b-9c83-0d3a8128a5ab'),(24,'craft\\elements\\MatrixBlock','2019-12-12 18:59:25','2019-12-12 18:59:25',NULL,'a60d947e-a1d4-4e75-bfea-847ff916a52a'),(25,'craft\\elements\\MatrixBlock','2019-12-17 16:16:59','2019-12-17 16:16:59',NULL,'c3a6f958-ffaa-4ff1-b063-5177c8190e01'),(26,'craft\\elements\\Entry','2019-12-17 16:22:20','2019-12-17 16:22:20',NULL,'1ba29cdf-064c-4629-ac02-a0cb543c805c'),(27,'craft\\elements\\MatrixBlock','2019-12-17 16:22:58','2019-12-17 16:22:58',NULL,'d410a2cf-ea98-403a-a19a-589f1fc94354'),(28,'craft\\elements\\MatrixBlock','2019-12-17 17:25:35','2019-12-17 17:25:35',NULL,'fb3258a7-94bb-4454-8c08-5476c90d23a0');
/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `fieldlayouttabs`
--

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `fieldlayouttabs` VALUES (3,2,'Content',1,'2019-09-25 13:43:56','2019-09-25 13:43:56','00400121-83dc-4a81-9429-462a6cd66092'),(4,3,'Content',1,'2019-09-25 13:44:12','2019-09-25 13:44:12','3257c16f-9143-4c44-a024-57c461a7211d'),(5,4,'Content',1,'2019-09-25 13:44:31','2019-09-25 13:44:31','64439836-d8eb-4930-b464-39824e07ea20'),(13,8,'Content',1,'2019-09-25 14:09:21','2019-09-25 14:09:21','b566c6c5-3525-410f-9416-63a631885695'),(14,9,'Content',1,'2019-09-25 14:10:36','2019-09-25 14:10:36','e2188fab-8b57-47d6-9050-7dd95ebbfe97'),(16,7,'Content',1,'2019-09-26 17:18:35','2019-09-26 17:18:35','6f3a093b-ede0-4ba4-ad60-4b1527280280'),(31,5,'Content',1,'2019-10-17 10:25:32','2019-10-17 10:25:32','e62ab0f8-58c0-4906-94aa-f8fed0febd2f'),(32,5,'SEO',2,'2019-10-17 10:25:32','2019-10-17 10:25:32','6b248bfc-76dc-438c-9044-dd4fbfc397eb'),(33,5,'Sharing',3,'2019-10-17 10:25:32','2019-10-17 10:25:32','04157b70-16c8-42c6-b23c-b81cfe45909c'),(34,14,'Default',1,'2019-10-17 10:25:58','2019-10-17 10:25:58','fea383b0-c9a9-43ef-b440-48bd4184e9d1'),(35,14,'SEO',2,'2019-10-17 10:25:58','2019-10-17 10:25:58','28873752-122f-4cad-81fb-0ee7242e84b2'),(36,14,'Sharing',3,'2019-10-17 10:25:58','2019-10-17 10:25:58','47ed3fe9-9c66-4256-8bc8-c26e347d3fd9'),(38,15,'Content',1,'2019-12-12 17:56:20','2019-12-12 17:56:20','50bd82fe-d77d-4ba6-86f6-f5e630db4a0d'),(39,16,'Content',1,'2019-12-12 18:15:03','2019-12-12 18:15:03','f80bf691-d067-4584-909f-c8a12265c18f'),(40,17,'Grants',1,'2019-12-12 18:32:13','2019-12-12 18:32:13','9fffa58e-37a7-4ba5-ba4f-84f14112ee74'),(41,18,'Content',1,'2019-12-12 18:42:29','2019-12-12 18:42:29','f0285ff9-f6c1-429f-80d1-c087f4cd36d0'),(60,6,'Default',1,'2019-12-12 18:56:31','2019-12-12 18:56:31','9b6fb426-9775-4756-b8db-b70f337caf40'),(61,6,'SEO',2,'2019-12-12 18:56:31','2019-12-12 18:56:31','73fbe62a-bc6d-4ee5-bf05-6832cdf25b41'),(62,6,'Sharing',3,'2019-12-12 18:56:31','2019-12-12 18:56:31','c6640502-065a-4c2a-b611-de7f881eed82'),(127,19,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','5501e015-5162-4ea2-b34a-78bbb695c7f5'),(128,23,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','4862e976-0a2c-4aac-a00b-ccfb7cf04fb8'),(129,24,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','6695e71b-d09a-4939-a4b4-4c7c8cfb61eb'),(130,20,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','9d7299dc-d11f-44f4-ad77-8a4df4f8cfeb'),(131,21,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','ecfc5f40-40ed-45a9-a471-bfc0e1ff4722'),(132,22,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','627022b3-4ffb-4df5-aa8b-9de3b69caec9'),(133,1,'Content',1,'2019-12-16 11:56:32','2019-12-16 11:56:32','9de3d1d0-7d7e-43c4-91e9-0f199ee48eb0'),(134,10,'Content',1,'2019-12-16 11:56:33','2019-12-16 11:56:33','1a1fd66d-9df1-4ca5-8bd2-6b1cd3513da2'),(135,11,'Content',1,'2019-12-16 11:56:33','2019-12-16 11:56:33','fc5b3557-1202-40aa-bbb8-a447d989f1b2'),(136,12,'Content',1,'2019-12-16 11:56:33','2019-12-16 11:56:33','cd4df195-c9e1-47a8-a3be-1c420f6ddf1b'),(137,13,'Content',1,'2019-12-16 11:56:33','2019-12-16 11:56:33','7593b954-87c8-443e-a0d9-c14e5dfaa902'),(138,25,'Content',1,'2019-12-17 16:16:59','2019-12-17 16:16:59','d583d624-e365-4da0-9fa2-6ac3a9dd1351'),(141,27,'Content',1,'2019-12-17 16:22:58','2019-12-17 16:22:58','736c2824-7a15-4a25-a35b-fdb9f594c6bf'),(145,28,'Content',1,'2019-12-17 17:25:35','2019-12-17 17:25:35','cb58d294-71f7-4f1f-8533-3806447d7092'),(149,26,'What We Fund',1,'2019-12-17 17:25:47','2019-12-17 17:25:47','aa7b46ac-f9fe-493c-a259-5e8faff63485'),(150,26,'Eligibility',2,'2019-12-17 17:25:47','2019-12-17 17:25:47','409bb86e-e22d-4a50-bff7-0884ad72cec7'),(151,26,'Statistics',3,'2019-12-17 17:25:47','2019-12-17 17:25:47','b7c53348-2483-46b1-8382-02ae2b8e8b98');
/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `fields` VALUES (1,2,'No Follow','noFollow','global','',1,'none',NULL,'craft\\fields\\Lightswitch','{\"default\":\"\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','5946c466-7b6d-4de4-a2c4-c986ae934171'),(2,2,'SEO Meta Description','seoMetaDescription','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','088a1bbb-38fb-455a-8421-7265f9f6bf8a'),(3,2,'SEO Meta Title','seoMetaTitle','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','21df62ef-2d2c-44b6-93d5-09043d88e3d8'),(4,2,'Tag Manager Javascript','tagManagerJavascript','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','ab38326d-4bdd-41db-b7a4-ce2a51a5e219'),(5,2,'Tag Manager Noscript','tagManagerNoscript','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','2ce3e14f-1241-4374-8cae-d86921c61981'),(6,3,'Sharing Description','sharingDescription','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','e3b36e56-21ce-4eaf-931a-1559a77376e5'),(7,3,'Sharing Image','sharingImage','global','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"sharing\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":[\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-09-23 18:29:03','2019-09-25 14:11:21','de99ccf9-7e9e-477f-8de9-3dd869787aec'),(8,3,'Sharing Title','sharingTitle','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-23 18:29:03','2019-09-23 18:29:03','90d6b033-68d0-4870-ba44-b793937bdf8e'),(9,4,'Content Blocks','contentBlocks','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_contentblocks}}\",\"propagationMethod\":\"all\"}','2019-09-23 18:36:27','2019-12-16 11:56:32','c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15'),(10,NULL,'Image','img','matrixBlockType:931ca6dc-d223-4440-8cbb-ea475c74f68a','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-09-23 18:36:27','2019-12-16 11:56:32','caf177da-41dc-491f-a3b1-5f38aac9fbe0'),(11,4,'Copy','copy','global','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-09-25 14:07:56','2019-09-25 14:08:14','242b7ab5-c925-42e2-9bea-2709b685ebf4'),(12,5,'Footer','footer','global','',1,'site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_footer}}\",\"propagationMethod\":\"all\",\"staticField\":\"1\",\"columns\":{\"13\":{\"width\":\"\"},\"14\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"}','2019-09-25 14:09:12','2019-09-26 17:18:35','fab33f57-c399-476a-a1fb-81187789afab'),(13,NULL,'Address','address','superTableBlockType:8d579288-0fa0-4b9d-a72e-8d1f70b14ea9','',0,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-09-25 14:09:12','2019-09-26 17:18:35','2d40f002-a1ac-4a77-a0be-73840894988c'),(14,NULL,'Contact','contact','superTableBlockType:8d579288-0fa0-4b9d-a72e-8d1f70b14ea9','',0,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-09-25 14:09:12','2019-09-26 17:18:35','ab0defb0-2946-4179-813f-b89a1e432ae1'),(15,4,'Image','img','global','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":[\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-09-25 14:10:16','2019-09-25 14:10:45','f9758a90-ee8b-4c8c-9530-185ed2476eb1'),(16,NULL,'Copy','copy','matrixBlockType:55d6a029-72ae-4193-aea3-03503b541ec0','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-09-26 17:23:45','2019-12-16 11:56:33','87d4dac2-c9f0-4f25-8041-a6b2ffadca5e'),(17,NULL,'Heading','heading','matrixBlockType:55d6a029-72ae-4193-aea3-03503b541ec0','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-26 17:23:45','2019-12-16 11:56:33','a9be6edf-c91b-4154-a348-05a79531884a'),(18,NULL,'Heading','heading','matrixBlockType:856ab452-c117-4e63-964c-a2f9ed79b513','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-26 17:23:45','2019-12-16 11:56:33','08cc6d9e-9d42-4df6-a2f4-023dc4471275'),(21,NULL,'CTAs','ctas','matrixBlockType:856ab452-c117-4e63-964c-a2f9ed79b513','',1,'site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_3_ctas}}\",\"propagationMethod\":\"all\",\"staticField\":\"\",\"columns\":{\"3d7281b4-7482-4132-ba0b-9dc9ebafc6f3\":{\"width\":\"\"},\"5c80be3f-0805-4a3e-ab3f-dddf458925d2\":{\"width\":\"\"},\"6b9242c5-0f14-4d13-8878-450a34496844\":{\"width\":\"\"},\"d648955c-8286-4939-821d-59b11a6848b6\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"}','2019-09-26 17:23:45','2019-12-16 11:56:33','adbaaeb0-399d-40a8-858c-2a2d78782bd6'),(22,NULL,'Image','img','superTableBlockType:8d96cd19-0c12-417d-92fc-7e564c84cf4f','',0,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":[\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-09-26 17:23:45','2019-12-16 11:56:33','d648955c-8286-4939-821d-59b11a6848b6'),(23,NULL,'Layout','layout','matrixBlockType:b25958b8-5ce9-417d-acc4-b5e62b474364','',1,'none',NULL,'rias\\positionfieldtype\\fields\\Position','{\"options\":{\"left\":\"\",\"center\":\"\",\"right\":\"1\",\"full\":\"\",\"drop-left\":\"1\",\"drop-right\":\"1\"},\"default\":\"center\"}','2019-09-26 17:24:40','2019-12-16 11:56:33','bd6ad906-0a31-4324-a55a-ff650aee911f'),(24,NULL,'Copy','copy','matrixBlockType:b25958b8-5ce9-417d-acc4-b5e62b474364','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-09-26 17:24:40','2019-12-16 11:56:33','d8ec3b04-96b9-4a13-a37f-8bfaa7294210'),(25,NULL,'Button','button','superTableBlockType:8d96cd19-0c12-417d-92fc-7e564c84cf4f','',0,'none',NULL,'fruitstudios\\linkit\\fields\\LinkitField','{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"More information\",\"allowTarget\":\"1\"}','2019-09-26 17:26:53','2019-12-16 11:56:33','3d7281b4-7482-4132-ba0b-9dc9ebafc6f3'),(26,NULL,'Copy','copy','superTableBlockType:8d96cd19-0c12-417d-92fc-7e564c84cf4f','',0,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-09-26 17:26:53','2019-12-16 11:56:33','5c80be3f-0805-4a3e-ab3f-dddf458925d2'),(27,NULL,'Heading','heading','superTableBlockType:8d96cd19-0c12-417d-92fc-7e564c84cf4f','',0,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-09-26 17:26:53','2019-12-16 11:56:33','6b9242c5-0f14-4d13-8878-450a34496844'),(28,6,'Role','role','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 17:55:25','2019-12-12 17:55:25','963c00e8-3f98-4b6b-812e-431125492c62'),(29,6,'Copy','staffCopy','global','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-12 17:55:54','2019-12-12 17:55:54','46cbbfc1-192d-4b07-a043-b8f0ccb5f05c'),(30,7,'Grant Type','grantType','global','',1,'site',NULL,'craft\\fields\\Entries','{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-12-12 18:29:07','2019-12-12 18:29:07','36d1070d-6586-4b4e-9bf2-ab7e574ce4bc'),(31,7,'Recipient Organisation','recipientOrganisation','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:29:21','2019-12-12 18:29:43','c70b61b7-0cda-4fbc-a673-84ee7a9553e5'),(32,7,'Description','description','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:29:37','2019-12-12 18:29:37','d3927285-2479-4698-b547-cb6bb60b69e1'),(33,7,'Priority Area','priorityArea','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:30:04','2019-12-12 18:30:04','6f5d725e-123a-4e3c-8d72-c414464b6f6e'),(34,7,'Amount Awarded','amountAwarded','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:30:18','2019-12-12 18:30:18','6ac5b4f4-0472-4564-9398-bff600a0cf69'),(35,7,'Priorities','priorities','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:31:38','2019-12-12 18:31:38','f46d0d5f-50cf-4f72-9e1b-bcaba8f9e286'),(36,4,'Banner','banner','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"1\",\"contentTable\":\"{{%matrixcontent_banner}}\",\"propagationMethod\":\"all\"}','2019-12-12 18:42:28','2019-12-12 18:42:28','ea319834-695a-4e29-b18b-3fcf9aec0c89'),(37,NULL,'Copy','copy','matrixBlockType:39ab8299-a798-4dcf-ac66-971ea32d348e','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-12 18:42:29','2019-12-12 18:42:29','3249dbcc-073b-445c-b921-f5c1b53a08b9'),(38,NULL,'Button','button','matrixBlockType:39ab8299-a798-4dcf-ac66-971ea32d348e','',1,'none',NULL,'fruitstudios\\linkit\\fields\\LinkitField','{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"Read more\",\"allowTarget\":\"1\"}','2019-12-12 18:42:29','2019-12-12 18:42:29','342b5df6-2a13-4e36-a646-d216fb474793'),(39,NULL,'Heading','heading','matrixBlockType:39ab8299-a798-4dcf-ac66-971ea32d348e','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:42:29','2019-12-12 18:42:29','47e51004-e580-44bd-b6df-679d9ce2b68c'),(40,NULL,'Heading','heading','matrixBlockType:328a1237-033b-41c0-9791-3bfaf5745aec','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:43:29','2019-12-16 11:56:32','08f3acd5-7455-4906-89ac-068a3cef4e3b'),(41,NULL,'Grants','grants','matrixBlockType:328a1237-033b-41c0-9791-3bfaf5745aec','',1,'site',NULL,'craft\\fields\\Entries','{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-12-12 18:43:29','2019-12-16 11:56:32','f5a170cd-cefe-43c1-b79f-f29022fc3430'),(42,NULL,'Funding Total Heading','fundingTotalHeading','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','6bb48f75-f540-4fd5-a042-bc1d004e93e1'),(43,NULL,'Funding Total','fundingTotal','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','7cb519b9-1eb2-4f22-981f-aeec4954de4e'),(44,NULL,'Grants','grants','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_7_grants}}\",\"propagationMethod\":\"all\",\"staticField\":\"\",\"columns\":{\"1ec122b3-7beb-4f58-ba83-bb3efa75d788\":{\"width\":\"\"},\"a08b7688-8292-4d45-aa4a-2d2d3c223926\":{\"width\":\"\"},\"ab1cc7de-259d-47af-8f3c-525607140740\":{\"width\":\"\"},\"ae011801-7095-44d1-818f-ea3317e67c45\":{\"width\":\"\"},\"bea98579-21f5-4521-ae77-e8b984d4d183\":{\"width\":\"\"},\"d2bc787b-4d4c-4324-ba96-b98a79368cdc\":{\"width\":\"\"},\"da744d55-dcbe-4799-8267-0d12dc4c9717\":{\"width\":\"\"},\"fc320066-0d01-4226-ae66-2a0e31bb9add\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','8c24c5c8-5700-4e3b-90a0-03a2c7b3ce84'),(45,NULL,'Anchor Programme','anchorProgramme','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','1ec122b3-7beb-4f58-ba83-bb3efa75d788'),(46,NULL,'Special Initiatives','specialInitiatives','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','a08b7688-8292-4d45-aa4a-2d2d3c223926'),(47,NULL,'Open Programme','openProgramme','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','ab1cc7de-259d-47af-8f3c-525607140740'),(48,NULL,'Grant','grant','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'site',NULL,'craft\\fields\\Entries','{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','ae011801-7095-44d1-818f-ea3317e67c45'),(49,NULL,'Grants Awarded','grantsAwarded','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','bea98579-21f5-4521-ae77-e8b984d4d183'),(50,NULL,'Total Applications','totalApplications','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','d2bc787b-4d4c-4324-ba96-b98a79368cdc'),(51,NULL,'Eligible Applications','eligibleApplications','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','da744d55-dcbe-4799-8267-0d12dc4c9717'),(52,NULL,'Total','total','superTableBlockType:65bc918b-04dd-494d-a5ea-4895e9df9c33','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','fc320066-0d01-4226-ae66-2a0e31bb9add'),(53,NULL,'Plan Total','planTotal','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','a65951c8-76ea-4ec1-8a1b-954f5f59e285'),(54,NULL,'Heading','heading','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','bfbc9255-d8f7-45a6-8efb-8889fc3f3aae'),(55,NULL,'Plan Heading','planHeading','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','ed972c9d-ee84-430b-b1ba-b9d1f37d2141'),(56,NULL,'Copy','copy','matrixBlockType:e6fd156c-448b-4e4b-8e02-b586c03a0c9c','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','cd7c1b39-92f4-4cf4-831b-c69f461541c8'),(57,NULL,'Button','button','matrixBlockType:e6fd156c-448b-4e4b-8e02-b586c03a0c9c','',1,'none',NULL,'fruitstudios\\linkit\\fields\\LinkitField','{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"Apply\",\"allowTarget\":\"1\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','cefd128d-7dc5-4edb-b53e-0dc8b9695efb'),(58,NULL,'Grants','grants','matrixBlockType:e6fd156c-448b-4e4b-8e02-b586c03a0c9c','',1,'site',NULL,'craft\\fields\\Entries','{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}','2019-12-12 18:51:05','2019-12-16 11:56:32','e6a4afd5-f33e-4a86-a15b-629409ae5a3d'),(59,NULL,'Heading','heading','matrixBlockType:4ec04e59-3125-4b70-8d6a-2d70ef28160d','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:59:24','2019-12-16 11:56:32','2b60df39-1ece-4845-8ba8-a8012f852c6e'),(60,NULL,'Programmes','programmes','matrixBlockType:4ec04e59-3125-4b70-8d6a-2d70ef28160d','',1,'site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_9_programmes}}\",\"propagationMethod\":\"all\",\"staticField\":\"\",\"columns\":{\"aa70b34b-602b-455d-81a4-618becd07fb0\":{\"width\":\"\"},\"b3bcde79-4b62-4d04-bdfe-0dfc96c25a21\":{\"width\":\"\"},\"e1d27be8-0395-42ce-bac2-641e572ca2e1\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"}','2019-12-12 18:59:24','2019-12-16 11:56:32','95862b21-7af5-49d6-93d5-35d7ce2049cd'),(61,NULL,'Copy','copy','superTableBlockType:068d7fcc-4fa4-457a-8265-5a8bee5142ff','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:59:24','2019-12-16 11:56:32','aa70b34b-602b-455d-81a4-618becd07fb0'),(62,NULL,'Subheading','subheading','superTableBlockType:068d7fcc-4fa4-457a-8265-5a8bee5142ff','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:59:24','2019-12-16 11:56:32','b3bcde79-4b62-4d04-bdfe-0dfc96c25a21'),(63,NULL,'Heading','heading','superTableBlockType:068d7fcc-4fa4-457a-8265-5a8bee5142ff','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-12 18:59:24','2019-12-16 11:56:32','e1d27be8-0395-42ce-bac2-641e572ca2e1'),(64,NULL,'Copy','copy','matrixBlockType:4ec04e59-3125-4b70-8d6a-2d70ef28160d','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-12 18:59:25','2019-12-16 11:56:32','9eea90dd-3b87-454d-982d-b150b60fad2c'),(65,NULL,'Button','button','matrixBlockType:4ec04e59-3125-4b70-8d6a-2d70ef28160d','',1,'none',NULL,'fruitstudios\\linkit\\fields\\LinkitField','{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"More information\",\"allowTarget\":\"1\"}','2019-12-12 18:59:25','2019-12-16 11:56:32','f16ec0e1-3a07-4dba-8f04-1832c11b1a8d'),(66,NULL,'Heading Test','fundingHeading','matrixBlockType:89e7ab67-dbff-4d57-a988-093f822c55af','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-16 11:56:32','2019-12-16 11:56:32','41536c28-7da2-4b95-84af-639ab85e1443'),(67,7,'What We Fund','whatWeFund','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"1\",\"contentTable\":\"{{%matrixcontent_whatwefund}}\",\"propagationMethod\":\"all\"}','2019-12-17 16:13:59','2019-12-17 16:16:59','87427681-efbf-4bce-b742-d8e0d5483367'),(68,NULL,'Top Copy','topCopy','matrixBlockType:0ccfd92a-bc4e-4a32-8057-68a2e1fa531a','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-17 16:16:59','2019-12-17 16:16:59','3ddfd8cf-7c9d-4acb-aca5-8071e4c54df2'),(69,NULL,'Bottom Heading','bottomHeading','matrixBlockType:0ccfd92a-bc4e-4a32-8057-68a2e1fa531a','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 16:16:59','2019-12-17 16:16:59','941f88f3-1892-4d6f-9b2d-5d14eacaa49c'),(70,NULL,'Bottom Copy','bottomCopy','matrixBlockType:0ccfd92a-bc4e-4a32-8057-68a2e1fa531a','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-17 16:16:59','2019-12-17 16:16:59','f3febeaa-f35d-494d-b2c5-377fee70fc28'),(71,7,'Eligibility','eligibility','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"1\",\"contentTable\":\"{{%matrixcontent_eligibility}}\",\"propagationMethod\":\"all\"}','2019-12-17 16:22:58','2019-12-17 16:22:58','8e69ab5b-7bf0-4324-8bb9-98062bc33ec3'),(72,NULL,'Heading','heading','matrixBlockType:69d2fc25-47b9-4048-b94e-344974b24070','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 16:22:58','2019-12-17 16:22:58','5862632c-bff4-4154-ba83-bb6cee7dda13'),(73,NULL,'Copy','copy','matrixBlockType:69d2fc25-47b9-4048-b94e-344974b24070','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"}','2019-12-17 16:22:58','2019-12-17 16:22:58','f3d3deaa-6d9d-41bd-b09f-b3e0f5f137d6'),(74,7,'Statstics','statstics','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_statstics}}\",\"propagationMethod\":\"all\"}','2019-12-17 17:25:35','2019-12-17 17:25:35','9a818bed-c24a-4f2c-88ad-a6e4caf2cbc6'),(75,NULL,'Heading','heading','matrixBlockType:d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 17:25:35','2019-12-17 17:25:35','364ac39c-1c1b-4ee7-bb73-bef8e0a664fd'),(76,NULL,'Eligible applications','eligibleApplications','matrixBlockType:d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 17:25:35','2019-12-17 17:25:35','3aeb7725-03e7-48ee-b61f-2ad0e4bbbd8f'),(77,NULL,'Subheading','subheading','matrixBlockType:d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 17:25:35','2019-12-17 17:25:35','416b38dc-725d-4b8a-b66d-10e4aab42f2f'),(78,NULL,'Grants awarded','grantsAwarded','matrixBlockType:d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 17:25:35','2019-12-17 17:25:35','68663995-a2ca-4a37-8190-77a2452a0a31'),(79,NULL,'Total Applications','totalApplications','matrixBlockType:d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-12-17 17:25:35','2019-12-17 17:25:35','ff59fb9e-9251-4e12-ae31-956e36eb9d77');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_crm_fields`
--

LOCK TABLES `freeform_crm_fields` WRITE;
/*!40000 ALTER TABLE `freeform_crm_fields` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_crm_fields` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_export_profiles`
--

LOCK TABLES `freeform_export_profiles` WRITE;
/*!40000 ALTER TABLE `freeform_export_profiles` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_export_profiles` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_export_settings`
--

LOCK TABLES `freeform_export_settings` WRITE;
/*!40000 ALTER TABLE `freeform_export_settings` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_export_settings` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_fields`
--

LOCK TABLES `freeform_fields` WRITE;
/*!40000 ALTER TABLE `freeform_fields` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `freeform_fields` VALUES (1,'text','firstName','First Name',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','f8ce2fa9-3ce4-4e54-89c9-0e1305ee9744'),(2,'text','lastName','Last Name',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','40dd96f2-5c63-4e04-b309-ee53c06a2e36'),(3,'email','email','Email',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','8d04e36d-e608-42bf-9149-8130b1819952'),(4,'text','website','Website',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','8432fcee-6aae-4d05-a1aa-abeb73e954a2'),(5,'text','cellPhone','Cell Phone',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','04394cc5-de28-4a4f-9178-fabea424ff59'),(6,'text','homePhone','Home Phone',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','73dfd130-2258-42d8-a501-2165099ea536'),(7,'text','companyName','Company Name',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','1fead553-2efc-4e4f-8c69-848e4a0aca06'),(8,'textarea','address','Address',0,NULL,'{\"rows\":2}','2019-09-23 18:18:58','2019-09-23 18:18:58','0d188bbc-7182-468b-b971-325a2673e789'),(9,'text','city','City',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','2af2b68b-3262-4fb1-a58a-3945313bd518'),(10,'select','state','State',0,NULL,'{\"options\":[{\"value\":\"\",\"label\":\"Select a State\"},{\"value\":\"AL\",\"label\":\"Alabama\"},{\"value\":\"AK\",\"label\":\"Alaska\"},{\"value\":\"AZ\",\"label\":\"Arizona\"},{\"value\":\"AR\",\"label\":\"Arkansas\"},{\"value\":\"CA\",\"label\":\"California\"},{\"value\":\"CO\",\"label\":\"Colorado\"},{\"value\":\"CT\",\"label\":\"Connecticut\"},{\"value\":\"DE\",\"label\":\"Delaware\"},{\"value\":\"DC\",\"label\":\"District of Columbia\"},{\"value\":\"FL\",\"label\":\"Florida\"},{\"value\":\"GA\",\"label\":\"Georgia\"},{\"value\":\"HI\",\"label\":\"Hawaii\"},{\"value\":\"ID\",\"label\":\"Idaho\"},{\"value\":\"IL\",\"label\":\"Illinois\"},{\"value\":\"IN\",\"label\":\"Indiana\"},{\"value\":\"IA\",\"label\":\"Iowa\"},{\"value\":\"KS\",\"label\":\"Kansas\"},{\"value\":\"KY\",\"label\":\"Kentucky\"},{\"value\":\"LA\",\"label\":\"Louisiana\"},{\"value\":\"ME\",\"label\":\"Maine\"},{\"value\":\"MD\",\"label\":\"Maryland\"},{\"value\":\"MA\",\"label\":\"Massachusetts\"},{\"value\":\"MI\",\"label\":\"Michigan\"},{\"value\":\"MN\",\"label\":\"Minnesota\"},{\"value\":\"MS\",\"label\":\"Mississippi\"},{\"value\":\"MO\",\"label\":\"Missouri\"},{\"value\":\"MT\",\"label\":\"Montana\"},{\"value\":\"NE\",\"label\":\"Nebraska\"},{\"value\":\"NV\",\"label\":\"Nevada\"},{\"value\":\"NH\",\"label\":\"New Hampshire\"},{\"value\":\"NJ\",\"label\":\"New Jersey\"},{\"value\":\"NM\",\"label\":\"New Mexico\"},{\"value\":\"NY\",\"label\":\"New York\"},{\"value\":\"NC\",\"label\":\"North Carolina\"},{\"value\":\"ND\",\"label\":\"North Dakota\"},{\"value\":\"OH\",\"label\":\"Ohio\"},{\"value\":\"OK\",\"label\":\"Oklahoma\"},{\"value\":\"OR\",\"label\":\"Oregon\"},{\"value\":\"PA\",\"label\":\"Pennsylvania\"},{\"value\":\"RI\",\"label\":\"Rhode Island\"},{\"value\":\"SC\",\"label\":\"South Carolina\"},{\"value\":\"SD\",\"label\":\"South Dakota\"},{\"value\":\"TN\",\"label\":\"Tennessee\"},{\"value\":\"TX\",\"label\":\"Texas\"},{\"value\":\"UT\",\"label\":\"Utah\"},{\"value\":\"VT\",\"label\":\"Vermont\"},{\"value\":\"VA\",\"label\":\"Virginia\"},{\"value\":\"WA\",\"label\":\"Washington\"},{\"value\":\"WV\",\"label\":\"West Virginia\"},{\"value\":\"WI\",\"label\":\"Wisconsin\"},{\"value\":\"WY\",\"label\":\"Wyoming\"}]}','2019-09-23 18:18:58','2019-09-23 18:18:58','e50045a0-9369-41f0-a11e-8cc964cb20af'),(11,'text','zipCode','Zip Code',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','6dc96c19-a5d5-478c-b026-3cdf32245280'),(12,'textarea','message','Message',0,NULL,'{\"rows\":5}','2019-09-23 18:18:58','2019-09-23 18:18:58','ba7de1b2-1e0d-4fa4-919c-b3fc0d80469b'),(13,'number','number','Number',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','85cd1d29-d9f0-4f43-acfb-9ccad56d7d9f'),(14,'cc_details','payment','',0,NULL,NULL,'2019-09-23 18:18:58','2019-09-23 18:18:58','4b1a0004-b062-42f7-9b5c-ca5b4efb3343');
/*!40000 ALTER TABLE `freeform_fields` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_forms`
--

LOCK TABLES `freeform_forms` WRITE;
/*!40000 ALTER TABLE `freeform_forms` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_forms` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_integrations`
--

LOCK TABLES `freeform_integrations` WRITE;
/*!40000 ALTER TABLE `freeform_integrations` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_integrations` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_integrations_queue`
--

LOCK TABLES `freeform_integrations_queue` WRITE;
/*!40000 ALTER TABLE `freeform_integrations_queue` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_integrations_queue` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_mailing_list_fields`
--

LOCK TABLES `freeform_mailing_list_fields` WRITE;
/*!40000 ALTER TABLE `freeform_mailing_list_fields` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_mailing_list_fields` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_mailing_lists`
--

LOCK TABLES `freeform_mailing_lists` WRITE;
/*!40000 ALTER TABLE `freeform_mailing_lists` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_mailing_lists` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_notifications`
--

LOCK TABLES `freeform_notifications` WRITE;
/*!40000 ALTER TABLE `freeform_notifications` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_notifications` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_payment_gateway_fields`
--

LOCK TABLES `freeform_payment_gateway_fields` WRITE;
/*!40000 ALTER TABLE `freeform_payment_gateway_fields` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_payment_gateway_fields` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_payments_payments`
--

LOCK TABLES `freeform_payments_payments` WRITE;
/*!40000 ALTER TABLE `freeform_payments_payments` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_payments_payments` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_payments_subscription_plans`
--

LOCK TABLES `freeform_payments_subscription_plans` WRITE;
/*!40000 ALTER TABLE `freeform_payments_subscription_plans` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_payments_subscription_plans` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_payments_subscriptions`
--

LOCK TABLES `freeform_payments_subscriptions` WRITE;
/*!40000 ALTER TABLE `freeform_payments_subscriptions` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_payments_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_statuses`
--

LOCK TABLES `freeform_statuses` WRITE;
/*!40000 ALTER TABLE `freeform_statuses` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `freeform_statuses` VALUES (1,'Pending','pending','light',NULL,1,'2019-09-23 18:18:58','2019-09-23 18:18:58','1bbb002b-1fa1-464a-8346-12adab55dd8d'),(2,'Open','open','green',1,2,'2019-09-23 18:18:58','2019-09-23 18:18:58','f99ed5d9-937a-4bc8-a0af-c706783b1ec2'),(3,'Closed','closed','grey',NULL,3,'2019-09-23 18:18:58','2019-09-23 18:18:58','29871408-6151-4d79-a679-fd8eba8bba23');
/*!40000 ALTER TABLE `freeform_statuses` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_submission_notes`
--

LOCK TABLES `freeform_submission_notes` WRITE;
/*!40000 ALTER TABLE `freeform_submission_notes` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_submission_notes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_submissions`
--

LOCK TABLES `freeform_submissions` WRITE;
/*!40000 ALTER TABLE `freeform_submissions` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_submissions` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_unfinalized_files`
--

LOCK TABLES `freeform_unfinalized_files` WRITE;
/*!40000 ALTER TABLE `freeform_unfinalized_files` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_unfinalized_files` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_webhooks`
--

LOCK TABLES `freeform_webhooks` WRITE;
/*!40000 ALTER TABLE `freeform_webhooks` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_webhooks` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `freeform_webhooks_form_relations`
--

LOCK TABLES `freeform_webhooks_form_relations` WRITE;
/*!40000 ALTER TABLE `freeform_webhooks_form_relations` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `freeform_webhooks_form_relations` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `globalsets`
--

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `globalsets` VALUES (5,'Default Meta Data','defaultMetaData',2,'2019-09-25 13:43:56','2019-09-25 13:43:56','992e903c-fdc4-4bd8-a01c-e742e285a93a'),(6,'Default Sharing Data','defaultSharingData',3,'2019-09-25 13:44:12','2019-09-25 13:44:12','4fea53e2-95c8-40ad-9189-4a13d50377ef'),(7,'Tag Manager','tagManager',4,'2019-09-25 13:44:31','2019-09-25 13:44:31','2fc0143e-ab96-408d-8105-e447e91bccee'),(9,'Footer','footer',8,'2019-09-25 14:06:39','2019-09-25 14:09:21','69a7e8fa-2d1a-47ed-83fb-e044dfd1debf'),(10,'Logo','logo',9,'2019-09-25 14:10:36','2019-09-25 14:10:36','2ead9b80-a5d1-426a-b211-8ebb72b8228b');
/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `gqlschemas`
--

LOCK TABLES `gqlschemas` WRITE;
/*!40000 ALTER TABLE `gqlschemas` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `gqlschemas` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `info` VALUES (1,'3.3.19','3.3.3',1,'{\"fieldGroups\":{\"76483afb-3281-40c6-987f-d0c1a94bfcf0\":{\"name\":\"SEO\"},\"5941ef4b-875a-44b2-8fdf-7c54bbcdb5be\":{\"name\":\"Sharing\"},\"e113262b-f370-415c-bca6-a906fba2f07c\":{\"name\":\"Default\"},\"028f4556-1943-4e08-bb16-417176659e00\":{\"name\":\"Global\"},\"7976b1a3-64c2-4ad1-bde0-359d215514e8\":{\"name\":\"Staff\"},\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\":{\"name\":\"Grants\"}},\"siteGroups\":{\"08a3e4fc-619a-4d83-949b-9e15ef770283\":{\"name\":\"AB Chairty Trust\"}},\"sites\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"siteGroup\":\"08a3e4fc-619a-4d83-949b-9e15ef770283\",\"name\":\"AB Charity Trust\",\"handle\":\"default\",\"language\":\"en-GB\",\"hasUrls\":true,\"baseUrl\":\"$DEFAULT_SITE_URL\",\"sortOrder\":1,\"primary\":true}},\"email\":{\"fromEmail\":\"sebastian.jordan@gmail.com\",\"fromName\":\"AB Charitable Trust\",\"template\":null,\"transportType\":\"craft\\\\mail\\\\transportadapters\\\\Sendmail\",\"transportSettings\":null},\"system\":{\"edition\":\"pro\",\"name\":\"AB Charity Trust\",\"live\":true,\"schemaVersion\":\"3.3.3\",\"timeZone\":\"Europe/London\"},\"users\":{\"requireEmailVerification\":true,\"allowPublicRegistration\":false,\"defaultGroup\":null,\"photoVolumeUid\":\"d6433ca4-70e0-4884-9992-cac1270c71a7\",\"photoSubpath\":\"\"},\"dateModified\":1576603547,\"plugins\":{\"cheat-sheet\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"},\"feed-me\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.1.2\"},\"freeform\":{\"edition\":\"lite\",\"enabled\":true,\"schemaVersion\":\"3.0.5\"},\"imager\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.0.0\"},\"linkit\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.8\"},\"navigation\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.12\"},\"position-fieldtype\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"},\"redactor\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.3.0\"},\"redirect\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.5\"},\"wordsmith\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"0.0.0.0\"},\"redactor-custom-styles\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"},\"super-table\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.2.1\"},\"field-manager\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"}},\"fields\":{\"5946c466-7b6d-4de4-a2c4-c986ae934171\":{\"name\":\"No Follow\",\"handle\":\"noFollow\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":\"76483afb-3281-40c6-987f-d0c1a94bfcf0\"},\"088a1bbb-38fb-455a-8421-7265f9f6bf8a\":{\"name\":\"SEO Meta Description\",\"handle\":\"seoMetaDescription\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"76483afb-3281-40c6-987f-d0c1a94bfcf0\"},\"21df62ef-2d2c-44b6-93d5-09043d88e3d8\":{\"name\":\"SEO Meta Title\",\"handle\":\"seoMetaTitle\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"76483afb-3281-40c6-987f-d0c1a94bfcf0\"},\"ab38326d-4bdd-41db-b7a4-ce2a51a5e219\":{\"name\":\"Tag Manager Javascript\",\"handle\":\"tagManagerJavascript\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"76483afb-3281-40c6-987f-d0c1a94bfcf0\"},\"2ce3e14f-1241-4374-8cae-d86921c61981\":{\"name\":\"Tag Manager Noscript\",\"handle\":\"tagManagerNoscript\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"76483afb-3281-40c6-987f-d0c1a94bfcf0\"},\"e3b36e56-21ce-4eaf-931a-1559a77376e5\":{\"name\":\"Sharing Description\",\"handle\":\"sharingDescription\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"5941ef4b-875a-44b2-8fdf-7c54bbcdb5be\"},\"de99ccf9-7e9e-477f-8de9-3dd869787aec\":{\"name\":\"Sharing Image\",\"handle\":\"sharingImage\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"sharing\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":[\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"5941ef4b-875a-44b2-8fdf-7c54bbcdb5be\"},\"90d6b033-68d0-4870-ba44-b793937bdf8e\":{\"name\":\"Sharing Title\",\"handle\":\"sharingTitle\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"5941ef4b-875a-44b2-8fdf-7c54bbcdb5be\"},\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\":{\"name\":\"Content Blocks\",\"handle\":\"contentBlocks\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Matrix\",\"settings\":{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_contentblocks}}\",\"propagationMethod\":\"all\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"e113262b-f370-415c-bca6-a906fba2f07c\"},\"242b7ab5-c925-42e2-9bea-2709b685ebf4\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"e113262b-f370-415c-bca6-a906fba2f07c\"},\"fab33f57-c399-476a-a1fb-81187789afab\":{\"name\":\"Footer\",\"handle\":\"footer\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\",\"settings\":{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_footer}}\",\"propagationMethod\":\"all\",\"staticField\":\"1\",\"columns\":{\"2d40f002-a1ac-4a77-a0be-73840894988c\":{\"width\":\"\"},\"ab0defb0-2946-4179-813f-b89a1e432ae1\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"028f4556-1943-4e08-bb16-417176659e00\"},\"f9758a90-ee8b-4c8c-9530-185ed2476eb1\":{\"name\":\"Image\",\"handle\":\"img\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":[\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"e113262b-f370-415c-bca6-a906fba2f07c\"},\"963c00e8-3f98-4b6b-812e-431125492c62\":{\"name\":\"Role\",\"handle\":\"role\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"7976b1a3-64c2-4ad1-bde0-359d215514e8\"},\"46cbbfc1-192d-4b07-a043-b8f0ccb5f05c\":{\"name\":\"Copy\",\"handle\":\"staffCopy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"7976b1a3-64c2-4ad1-bde0-359d215514e8\"},\"36d1070d-6586-4b4e-9bf2-ab7e574ce4bc\":{\"name\":\"Grant Type\",\"handle\":\"grantType\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Entries\",\"settings\":{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"c70b61b7-0cda-4fbc-a673-84ee7a9553e5\":{\"name\":\"Recipient Organisation\",\"handle\":\"recipientOrganisation\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"d3927285-2479-4698-b547-cb6bb60b69e1\":{\"name\":\"Description\",\"handle\":\"description\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"6f5d725e-123a-4e3c-8d72-c414464b6f6e\":{\"name\":\"Priority Area\",\"handle\":\"priorityArea\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"6ac5b4f4-0472-4564-9398-bff600a0cf69\":{\"name\":\"Amount Awarded\",\"handle\":\"amountAwarded\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"f46d0d5f-50cf-4f72-9e1b-bcaba8f9e286\":{\"name\":\"Priorities\",\"handle\":\"priorities\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"ea319834-695a-4e29-b18b-3fcf9aec0c89\":{\"name\":\"Banner\",\"handle\":\"banner\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Matrix\",\"settings\":{\"minBlocks\":\"\",\"maxBlocks\":\"1\",\"contentTable\":\"{{%matrixcontent_banner}}\",\"propagationMethod\":\"all\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"e113262b-f370-415c-bca6-a906fba2f07c\"},\"87427681-efbf-4bce-b742-d8e0d5483367\":{\"name\":\"What We Fund\",\"handle\":\"whatWeFund\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Matrix\",\"settings\":{\"minBlocks\":\"\",\"maxBlocks\":\"1\",\"contentTable\":\"{{%matrixcontent_whatwefund}}\",\"propagationMethod\":\"all\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"8e69ab5b-7bf0-4324-8bb9-98062bc33ec3\":{\"name\":\"Eligibility\",\"handle\":\"eligibility\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Matrix\",\"settings\":{\"minBlocks\":\"\",\"maxBlocks\":\"1\",\"contentTable\":\"{{%matrixcontent_eligibility}}\",\"propagationMethod\":\"all\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"},\"9a818bed-c24a-4f2c-88ad-a6e4caf2cbc6\":{\"name\":\"Statstics\",\"handle\":\"statstics\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Matrix\",\"settings\":{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_statstics}}\",\"propagationMethod\":\"all\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"c5c6e168-9c47-40cf-aaa0-3f5cde11a6dc\"}},\"sections\":{\"ee328c08-2719-4360-a27f-4d3fc4ca60ab\":{\"name\":\"Pages\",\"handle\":\"pages\",\"type\":\"structure\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"enabledByDefault\":true,\"hasUrls\":true,\"uriFormat\":\"{slug}\",\"template\":\"pages/_entry\"}},\"structure\":{\"uid\":\"69cb8d4b-3a17-4386-a202-efe8a8080ac2\",\"maxLevels\":null},\"entryTypes\":{\"c9a31744-8fcd-4d59-a850-2fd1bfbd4df8\":{\"name\":\"Default\",\"handle\":\"default\",\"hasTitleField\":true,\"titleLabel\":\"Title\",\"titleFormat\":\"\",\"sortOrder\":1,\"fieldLayouts\":{\"fbf9461b-5f08-4bcf-804e-27ae8fa856c6\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\":{\"required\":false,\"sortOrder\":1}}},{\"name\":\"SEO\",\"sortOrder\":2,\"fields\":{\"088a1bbb-38fb-455a-8421-7265f9f6bf8a\":{\"required\":false,\"sortOrder\":2},\"21df62ef-2d2c-44b6-93d5-09043d88e3d8\":{\"required\":false,\"sortOrder\":1},\"5946c466-7b6d-4de4-a2c4-c986ae934171\":{\"required\":false,\"sortOrder\":3}}},{\"name\":\"Sharing\",\"sortOrder\":3,\"fields\":{\"90d6b033-68d0-4870-ba44-b793937bdf8e\":{\"required\":false,\"sortOrder\":1},\"de99ccf9-7e9e-477f-8de9-3dd869787aec\":{\"required\":false,\"sortOrder\":3},\"e3b36e56-21ce-4eaf-931a-1559a77376e5\":{\"required\":false,\"sortOrder\":2}}}]}}},\"22415549-5355-4cc9-a0db-3aadd2b6b23d\":{\"name\":\"Apply\",\"handle\":\"apply\",\"hasTitleField\":true,\"titleLabel\":\"Title\",\"titleFormat\":\"\",\"sortOrder\":2,\"fieldLayouts\":{\"8ac273ba-177c-40ef-92b0-fdd5d5b088df\":{\"tabs\":[{\"name\":\"Default\",\"sortOrder\":1,\"fields\":{\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\":{\"required\":false,\"sortOrder\":1}}},{\"name\":\"SEO\",\"sortOrder\":2,\"fields\":{\"088a1bbb-38fb-455a-8421-7265f9f6bf8a\":{\"required\":false,\"sortOrder\":2},\"21df62ef-2d2c-44b6-93d5-09043d88e3d8\":{\"required\":false,\"sortOrder\":1},\"5946c466-7b6d-4de4-a2c4-c986ae934171\":{\"required\":false,\"sortOrder\":3}}},{\"name\":\"Sharing\",\"sortOrder\":3,\"fields\":{\"90d6b033-68d0-4870-ba44-b793937bdf8e\":{\"required\":false,\"sortOrder\":1},\"de99ccf9-7e9e-477f-8de9-3dd869787aec\":{\"required\":false,\"sortOrder\":3},\"e3b36e56-21ce-4eaf-931a-1559a77376e5\":{\"required\":false,\"sortOrder\":2}}}]}}}}},\"a3d293bf-d009-4c39-b43f-8962c13c2008\":{\"name\":\"Home\",\"handle\":\"home\",\"type\":\"single\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"enabledByDefault\":true,\"hasUrls\":true,\"uriFormat\":\"__home__\",\"template\":\"\"}},\"entryTypes\":{\"a93b64d5-1fb1-49f2-9b47-07f9cb3b608d\":{\"name\":\"Home\",\"handle\":\"home\",\"hasTitleField\":false,\"titleLabel\":\"\",\"titleFormat\":\"{section.name|raw}\",\"sortOrder\":1,\"fieldLayouts\":{\"bbc19fb3-2cbb-4ea6-b19f-9db818075b4f\":{\"tabs\":[{\"name\":\"Default\",\"sortOrder\":1,\"fields\":{\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\":{\"required\":false,\"sortOrder\":2},\"ea319834-695a-4e29-b18b-3fcf9aec0c89\":{\"required\":false,\"sortOrder\":1}}},{\"name\":\"SEO\",\"sortOrder\":2,\"fields\":{\"088a1bbb-38fb-455a-8421-7265f9f6bf8a\":{\"required\":false,\"sortOrder\":2},\"21df62ef-2d2c-44b6-93d5-09043d88e3d8\":{\"required\":false,\"sortOrder\":1}}},{\"name\":\"Sharing\",\"sortOrder\":3,\"fields\":{\"90d6b033-68d0-4870-ba44-b793937bdf8e\":{\"required\":false,\"sortOrder\":1},\"de99ccf9-7e9e-477f-8de9-3dd869787aec\":{\"required\":false,\"sortOrder\":3},\"e3b36e56-21ce-4eaf-931a-1559a77376e5\":{\"required\":false,\"sortOrder\":2}}}]}}}}},\"40245d37-4cf6-4080-b358-9aa027534703\":{\"name\":\"Grants Awarded\",\"handle\":\"grantsAwarded\",\"type\":\"channel\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"enabledByDefault\":true,\"hasUrls\":true,\"uriFormat\":\"grants-awarded/{slug}\",\"template\":\"\"}},\"entryTypes\":{\"634ccb16-716f-4e6a-be95-bdd57734a9c5\":{\"name\":\"Grants Awarded\",\"handle\":\"grantsAwarded\",\"hasTitleField\":true,\"titleLabel\":\"Title\",\"titleFormat\":\"\",\"sortOrder\":1,\"fieldLayouts\":{\"ec76634b-89c9-419e-b80b-740e6ec775bf\":{\"tabs\":[{\"name\":\"Grants\",\"sortOrder\":1,\"fields\":{\"36d1070d-6586-4b4e-9bf2-ab7e574ce4bc\":{\"required\":false,\"sortOrder\":3},\"6ac5b4f4-0472-4564-9398-bff600a0cf69\":{\"required\":false,\"sortOrder\":5},\"6f5d725e-123a-4e3c-8d72-c414464b6f6e\":{\"required\":false,\"sortOrder\":4},\"c70b61b7-0cda-4fbc-a673-84ee7a9553e5\":{\"required\":false,\"sortOrder\":1},\"d3927285-2479-4698-b547-cb6bb60b69e1\":{\"required\":false,\"sortOrder\":2}}}]}}}}},\"5f9ddfa5-840d-422b-9d15-69d77baa68fd\":{\"name\":\"Staff\",\"handle\":\"staff\",\"type\":\"channel\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"enabledByDefault\":true,\"hasUrls\":false,\"uriFormat\":\"\",\"template\":null}},\"entryTypes\":{\"8ce3cc02-02ed-4ef4-b20a-5082a3395f46\":{\"name\":\"Staff\",\"handle\":\"staff\",\"hasTitleField\":true,\"titleLabel\":\"Title\",\"titleFormat\":\"\",\"sortOrder\":1,\"fieldLayouts\":{\"ec7ea6da-f41b-492c-b188-923be792f541\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"46cbbfc1-192d-4b07-a043-b8f0ccb5f05c\":{\"required\":false,\"sortOrder\":3},\"963c00e8-3f98-4b6b-812e-431125492c62\":{\"required\":false,\"sortOrder\":2},\"f9758a90-ee8b-4c8c-9530-185ed2476eb1\":{\"required\":false,\"sortOrder\":1}}}]}}}}},\"3c91ce22-6de2-4079-9273-1472dd23dc27\":{\"name\":\"Trustees\",\"handle\":\"trustees\",\"type\":\"channel\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"enabledByDefault\":true,\"hasUrls\":true,\"uriFormat\":\"trustees/{slug}\",\"template\":\"\"}},\"entryTypes\":{\"17aec3eb-db2b-488d-8fde-0aaf9c2b3c1e\":{\"name\":\"Trustees\",\"handle\":\"trustees\",\"hasTitleField\":true,\"titleLabel\":\"Title\",\"titleFormat\":\"\",\"sortOrder\":1,\"fieldLayouts\":{\"0ace9fa7-4cba-44f5-abb2-ed05b8ba6279\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"46cbbfc1-192d-4b07-a043-b8f0ccb5f05c\":{\"required\":false,\"sortOrder\":3},\"963c00e8-3f98-4b6b-812e-431125492c62\":{\"required\":false,\"sortOrder\":2},\"f9758a90-ee8b-4c8c-9530-185ed2476eb1\":{\"required\":false,\"sortOrder\":1}}}]}}}}},\"e74dd188-369d-4999-be24-4a6a51e9ba53\":{\"name\":\"Grants\",\"handle\":\"grants\",\"type\":\"channel\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"enabledByDefault\":true,\"hasUrls\":true,\"uriFormat\":\"grants/{slug}\",\"template\":\"\"}},\"entryTypes\":{\"ccdc057b-8c99-44bf-9cc1-560510bb41c1\":{\"name\":\"Grants\",\"handle\":\"grants\",\"hasTitleField\":true,\"titleLabel\":\"Title\",\"titleFormat\":\"\",\"sortOrder\":1,\"fieldLayouts\":{\"1ba29cdf-064c-4629-ac02-a0cb543c805c\":{\"tabs\":[{\"name\":\"What We Fund\",\"sortOrder\":1,\"fields\":{\"87427681-efbf-4bce-b742-d8e0d5483367\":{\"required\":false,\"sortOrder\":1}}},{\"name\":\"Eligibility\",\"sortOrder\":2,\"fields\":{\"8e69ab5b-7bf0-4324-8bb9-98062bc33ec3\":{\"required\":false,\"sortOrder\":1}}},{\"name\":\"Statistics\",\"sortOrder\":3,\"fields\":{\"9a818bed-c24a-4f2c-88ad-a6e4caf2cbc6\":{\"required\":false,\"sortOrder\":1}}}]}}}}}},\"matrixBlockTypes\":{\"931ca6dc-d223-4440-8cbb-ea475c74f68a\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Banner\",\"handle\":\"banner\",\"sortOrder\":5,\"fields\":{\"caf177da-41dc-491f-a3b1-5f38aac9fbe0\":{\"name\":\"Image\",\"handle\":\"img\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null}},\"fieldLayouts\":{\"2472b3fb-ea06-497f-9cb3-65e6ab5dcac0\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"caf177da-41dc-491f-a3b1-5f38aac9fbe0\":{\"required\":false,\"sortOrder\":1}}}]}}},\"55d6a029-72ae-4193-aea3-03503b541ec0\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Heading Intro\",\"handle\":\"headingIntro\",\"sortOrder\":6,\"fields\":{\"87d4dac2-c9f0-4f25-8041-a6b2ffadca5e\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"a9be6edf-c91b-4154-a348-05a79531884a\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"1b3ed2ea-891b-451f-bdc4-02c7e5c13b99\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"87d4dac2-c9f0-4f25-8041-a6b2ffadca5e\":{\"required\":false,\"sortOrder\":2},\"a9be6edf-c91b-4154-a348-05a79531884a\":{\"required\":false,\"sortOrder\":1}}}]}}},\"856ab452-c117-4e63-964c-a2f9ed79b513\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Call To Actions\",\"handle\":\"callToActions\",\"sortOrder\":7,\"fields\":{\"08cc6d9e-9d42-4df6-a2f4-023dc4471275\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"adbaaeb0-399d-40a8-858c-2a2d78782bd6\":{\"name\":\"CTAs\",\"handle\":\"ctas\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\",\"settings\":{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_3_ctas}}\",\"propagationMethod\":\"all\",\"staticField\":\"\",\"columns\":{\"3d7281b4-7482-4132-ba0b-9dc9ebafc6f3\":{\"width\":\"\"},\"5c80be3f-0805-4a3e-ab3f-dddf458925d2\":{\"width\":\"\"},\"6b9242c5-0f14-4d13-8878-450a34496844\":{\"width\":\"\"},\"d648955c-8286-4939-821d-59b11a6848b6\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null}},\"fieldLayouts\":{\"51fd2220-8d81-4cd1-a53a-a0c03ca05c19\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"08cc6d9e-9d42-4df6-a2f4-023dc4471275\":{\"required\":false,\"sortOrder\":1},\"adbaaeb0-399d-40a8-858c-2a2d78782bd6\":{\"required\":false,\"sortOrder\":2}}}]}}},\"b25958b8-5ce9-417d-acc4-b5e62b474364\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Plain Text\",\"handle\":\"plainText\",\"sortOrder\":8,\"fields\":{\"bd6ad906-0a31-4324-a55a-ff650aee911f\":{\"name\":\"Layout\",\"handle\":\"layout\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"rias\\\\positionfieldtype\\\\fields\\\\Position\",\"settings\":{\"options\":{\"left\":\"\",\"center\":\"\",\"right\":\"1\",\"full\":\"\",\"drop-left\":\"1\",\"drop-right\":\"1\"},\"default\":\"center\"},\"contentColumnType\":\"string\",\"fieldGroup\":null},\"d8ec3b04-96b9-4a13-a37f-8bfaa7294210\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"06ad3361-a9dc-4884-85fe-97f420b4e366\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"bd6ad906-0a31-4324-a55a-ff650aee911f\":{\"required\":false,\"sortOrder\":2},\"d8ec3b04-96b9-4a13-a37f-8bfaa7294210\":{\"required\":false,\"sortOrder\":1}}}]}}},\"39ab8299-a798-4dcf-ac66-971ea32d348e\":{\"field\":\"ea319834-695a-4e29-b18b-3fcf9aec0c89\",\"name\":\"Slide\",\"handle\":\"slide\",\"sortOrder\":1,\"fields\":{\"3249dbcc-073b-445c-b921-f5c1b53a08b9\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"342b5df6-2a13-4e36-a646-d216fb474793\":{\"name\":\"Button\",\"handle\":\"button\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"fruitstudios\\\\linkit\\\\fields\\\\LinkitField\",\"settings\":{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"Read more\",\"allowTarget\":\"1\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"47e51004-e580-44bd-b6df-679d9ce2b68c\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"82417801-9adf-44a1-bfa5-81007547c9d3\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"3249dbcc-073b-445c-b921-f5c1b53a08b9\":{\"required\":false,\"sortOrder\":2},\"342b5df6-2a13-4e36-a646-d216fb474793\":{\"required\":false,\"sortOrder\":3},\"47e51004-e580-44bd-b6df-679d9ce2b68c\":{\"required\":false,\"sortOrder\":1}}}]}}},\"328a1237-033b-41c0-9791-3bfaf5745aec\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Priority Area\",\"handle\":\"priorityArea\",\"sortOrder\":1,\"fields\":{\"08f3acd5-7455-4906-89ac-068a3cef4e3b\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"f5a170cd-cefe-43c1-b79f-f29022fc3430\":{\"name\":\"Grants\",\"handle\":\"grants\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Entries\",\"settings\":{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null}},\"fieldLayouts\":{\"46611989-f47d-4102-ac87-a500530cc29c\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"08f3acd5-7455-4906-89ac-068a3cef4e3b\":{\"required\":false,\"sortOrder\":1},\"f5a170cd-cefe-43c1-b79f-f29022fc3430\":{\"required\":false,\"sortOrder\":2}}}]}}},\"89e7ab67-dbff-4d57-a988-093f822c55af\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Prior Funding\",\"handle\":\"priorFunding\",\"sortOrder\":3,\"fields\":{\"41536c28-7da2-4b95-84af-639ab85e1443\":{\"name\":\"Heading Test\",\"handle\":\"fundingHeading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"6bb48f75-f540-4fd5-a042-bc1d004e93e1\":{\"name\":\"Funding Total Heading\",\"handle\":\"fundingTotalHeading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"7cb519b9-1eb2-4f22-981f-aeec4954de4e\":{\"name\":\"Funding Total\",\"handle\":\"fundingTotal\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"8c24c5c8-5700-4e3b-90a0-03a2c7b3ce84\":{\"name\":\"Grants\",\"handle\":\"grants\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\",\"settings\":{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_7_grants}}\",\"propagationMethod\":\"all\",\"staticField\":\"\",\"columns\":{\"1ec122b3-7beb-4f58-ba83-bb3efa75d788\":{\"width\":\"\"},\"a08b7688-8292-4d45-aa4a-2d2d3c223926\":{\"width\":\"\"},\"ab1cc7de-259d-47af-8f3c-525607140740\":{\"width\":\"\"},\"ae011801-7095-44d1-818f-ea3317e67c45\":{\"width\":\"\"},\"bea98579-21f5-4521-ae77-e8b984d4d183\":{\"width\":\"\"},\"d2bc787b-4d4c-4324-ba96-b98a79368cdc\":{\"width\":\"\"},\"da744d55-dcbe-4799-8267-0d12dc4c9717\":{\"width\":\"\"},\"fc320066-0d01-4226-ae66-2a0e31bb9add\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null},\"a65951c8-76ea-4ec1-8a1b-954f5f59e285\":{\"name\":\"Plan Total\",\"handle\":\"planTotal\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"bfbc9255-d8f7-45a6-8efb-8889fc3f3aae\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"ed972c9d-ee84-430b-b1ba-b9d1f37d2141\":{\"name\":\"Plan Heading\",\"handle\":\"planHeading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"5a58956a-062e-464b-aa49-f19edfff9bce\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"41536c28-7da2-4b95-84af-639ab85e1443\":{\"required\":false,\"sortOrder\":2},\"6bb48f75-f540-4fd5-a042-bc1d004e93e1\":{\"required\":false,\"sortOrder\":4},\"7cb519b9-1eb2-4f22-981f-aeec4954de4e\":{\"required\":false,\"sortOrder\":5},\"8c24c5c8-5700-4e3b-90a0-03a2c7b3ce84\":{\"required\":false,\"sortOrder\":3},\"a65951c8-76ea-4ec1-8a1b-954f5f59e285\":{\"required\":false,\"sortOrder\":7},\"bfbc9255-d8f7-45a6-8efb-8889fc3f3aae\":{\"required\":false,\"sortOrder\":1},\"ed972c9d-ee84-430b-b1ba-b9d1f37d2141\":{\"required\":false,\"sortOrder\":6}}}]}}},\"e6fd156c-448b-4e4b-8e02-b586c03a0c9c\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Apply\",\"handle\":\"apply\",\"sortOrder\":4,\"fields\":{\"cd7c1b39-92f4-4cf4-831b-c69f461541c8\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"cefd128d-7dc5-4edb-b53e-0dc8b9695efb\":{\"name\":\"Button\",\"handle\":\"button\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"fruitstudios\\\\linkit\\\\fields\\\\LinkitField\",\"settings\":{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"Apply\",\"allowTarget\":\"1\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"e6a4afd5-f33e-4a86-a15b-629409ae5a3d\":{\"name\":\"Grants\",\"handle\":\"grants\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Entries\",\"settings\":{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null}},\"fieldLayouts\":{\"49d340d2-2b2b-4c53-8eda-15b1c70cf429\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"cd7c1b39-92f4-4cf4-831b-c69f461541c8\":{\"required\":false,\"sortOrder\":1},\"cefd128d-7dc5-4edb-b53e-0dc8b9695efb\":{\"required\":false,\"sortOrder\":3},\"e6a4afd5-f33e-4a86-a15b-629409ae5a3d\":{\"required\":false,\"sortOrder\":2}}}]}}},\"4ec04e59-3125-4b70-8d6a-2d70ef28160d\":{\"field\":\"c1ce3e6c-9ad3-42bd-81d1-ee16c3e83e15\",\"name\":\"Funding Programmes\",\"handle\":\"fundingProgrammes\",\"sortOrder\":2,\"fields\":{\"2b60df39-1ece-4845-8ba8-a8012f852c6e\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"95862b21-7af5-49d6-93d5-35d7ce2049cd\":{\"name\":\"Programmes\",\"handle\":\"programmes\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\",\"settings\":{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_9_programmes}}\",\"propagationMethod\":\"all\",\"staticField\":\"\",\"columns\":{\"aa70b34b-602b-455d-81a4-618becd07fb0\":{\"width\":\"\"},\"b3bcde79-4b62-4d04-bdfe-0dfc96c25a21\":{\"width\":\"\"},\"e1d27be8-0395-42ce-bac2-641e572ca2e1\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null},\"9eea90dd-3b87-454d-982d-b150b60fad2c\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"f16ec0e1-3a07-4dba-8f04-1832c11b1a8d\":{\"name\":\"Button\",\"handle\":\"button\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"fruitstudios\\\\linkit\\\\fields\\\\LinkitField\",\"settings\":{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"More information\",\"allowTarget\":\"1\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"a60d947e-a1d4-4e75-bfea-847ff916a52a\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"2b60df39-1ece-4845-8ba8-a8012f852c6e\":{\"required\":false,\"sortOrder\":1},\"95862b21-7af5-49d6-93d5-35d7ce2049cd\":{\"required\":false,\"sortOrder\":3},\"9eea90dd-3b87-454d-982d-b150b60fad2c\":{\"required\":false,\"sortOrder\":2},\"f16ec0e1-3a07-4dba-8f04-1832c11b1a8d\":{\"required\":false,\"sortOrder\":4}}}]}}},\"0ccfd92a-bc4e-4a32-8057-68a2e1fa531a\":{\"field\":\"87427681-efbf-4bce-b742-d8e0d5483367\",\"name\":\"Add Content\",\"handle\":\"addContent\",\"sortOrder\":1,\"fields\":{\"3ddfd8cf-7c9d-4acb-aca5-8071e4c54df2\":{\"name\":\"Top Copy\",\"handle\":\"topCopy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"941f88f3-1892-4d6f-9b2d-5d14eacaa49c\":{\"name\":\"Bottom Heading\",\"handle\":\"bottomHeading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"f3febeaa-f35d-494d-b2c5-377fee70fc28\":{\"name\":\"Bottom Copy\",\"handle\":\"bottomCopy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"c3a6f958-ffaa-4ff1-b063-5177c8190e01\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"3ddfd8cf-7c9d-4acb-aca5-8071e4c54df2\":{\"required\":false,\"sortOrder\":1},\"941f88f3-1892-4d6f-9b2d-5d14eacaa49c\":{\"required\":false,\"sortOrder\":2},\"f3febeaa-f35d-494d-b2c5-377fee70fc28\":{\"required\":false,\"sortOrder\":3}}}]}}},\"69d2fc25-47b9-4048-b94e-344974b24070\":{\"field\":\"8e69ab5b-7bf0-4324-8bb9-98062bc33ec3\",\"name\":\"Add Content\",\"handle\":\"addContent\",\"sortOrder\":1,\"fields\":{\"5862632c-bff4-4154-ba83-bb6cee7dda13\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"f3d3deaa-6d9d-41bd-b09f-b3e0f5f137d6\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"d410a2cf-ea98-403a-a19a-589f1fc94354\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"5862632c-bff4-4154-ba83-bb6cee7dda13\":{\"required\":false,\"sortOrder\":1},\"f3d3deaa-6d9d-41bd-b09f-b3e0f5f137d6\":{\"required\":false,\"sortOrder\":2}}}]}}},\"d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b\":{\"field\":\"9a818bed-c24a-4f2c-88ad-a6e4caf2cbc6\",\"name\":\"Add Content\",\"handle\":\"addContent\",\"sortOrder\":1,\"fields\":{\"364ac39c-1c1b-4ee7-bb73-bef8e0a664fd\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"3aeb7725-03e7-48ee-b61f-2ad0e4bbbd8f\":{\"name\":\"Eligible applications\",\"handle\":\"eligibleApplications\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"416b38dc-725d-4b8a-b66d-10e4aab42f2f\":{\"name\":\"Subheading\",\"handle\":\"subheading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"68663995-a2ca-4a37-8190-77a2452a0a31\":{\"name\":\"Grants awarded\",\"handle\":\"grantsAwarded\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"ff59fb9e-9251-4e12-ae31-956e36eb9d77\":{\"name\":\"Total Applications\",\"handle\":\"totalApplications\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"fb3258a7-94bb-4454-8c08-5476c90d23a0\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"364ac39c-1c1b-4ee7-bb73-bef8e0a664fd\":{\"required\":false,\"sortOrder\":1},\"3aeb7725-03e7-48ee-b61f-2ad0e4bbbd8f\":{\"required\":false,\"sortOrder\":4},\"416b38dc-725d-4b8a-b66d-10e4aab42f2f\":{\"required\":false,\"sortOrder\":2},\"68663995-a2ca-4a37-8190-77a2452a0a31\":{\"required\":false,\"sortOrder\":5},\"ff59fb9e-9251-4e12-ae31-956e36eb9d77\":{\"required\":false,\"sortOrder\":3}}}]}}}},\"volumes\":{\"d6433ca4-70e0-4884-9992-cac1270c71a7\":{\"name\":\"Uploads\",\"handle\":\"uploads\",\"type\":\"craft\\\\volumes\\\\Local\",\"hasUrls\":true,\"url\":\"@baseUrl/uploads/\",\"settings\":{\"path\":\"@basePath/uploads/\"},\"sortOrder\":1}},\"globalSets\":{\"992e903c-fdc4-4bd8-a01c-e742e285a93a\":{\"name\":\"Default Meta Data\",\"handle\":\"defaultMetaData\",\"fieldLayouts\":{\"eae20891-5e05-422e-9beb-b4dca976d1ef\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"088a1bbb-38fb-455a-8421-7265f9f6bf8a\":{\"required\":false,\"sortOrder\":2},\"21df62ef-2d2c-44b6-93d5-09043d88e3d8\":{\"required\":false,\"sortOrder\":1}}}]}}},\"4fea53e2-95c8-40ad-9189-4a13d50377ef\":{\"name\":\"Default Sharing Data\",\"handle\":\"defaultSharingData\",\"fieldLayouts\":{\"e06d29f7-eee8-4a9f-a285-bd5a598eabdb\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"90d6b033-68d0-4870-ba44-b793937bdf8e\":{\"required\":false,\"sortOrder\":1},\"de99ccf9-7e9e-477f-8de9-3dd869787aec\":{\"required\":false,\"sortOrder\":3},\"e3b36e56-21ce-4eaf-931a-1559a77376e5\":{\"required\":false,\"sortOrder\":2}}}]}}},\"2fc0143e-ab96-408d-8105-e447e91bccee\":{\"name\":\"Tag Manager\",\"handle\":\"tagManager\",\"fieldLayouts\":{\"ae427693-7c58-44fc-aee3-86a048f3e7c3\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"2ce3e14f-1241-4374-8cae-d86921c61981\":{\"required\":false,\"sortOrder\":2},\"ab38326d-4bdd-41db-b7a4-ce2a51a5e219\":{\"required\":false,\"sortOrder\":1}}}]}}},\"69a7e8fa-2d1a-47ed-83fb-e044dfd1debf\":{\"name\":\"Footer\",\"handle\":\"footer\",\"fieldLayouts\":{\"ded61cda-d38a-4764-b23d-ba184c20784b\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"fab33f57-c399-476a-a1fb-81187789afab\":{\"required\":false,\"sortOrder\":1}}}]}}},\"2ead9b80-a5d1-426a-b211-8ebb72b8228b\":{\"name\":\"Logo\",\"handle\":\"logo\",\"fieldLayouts\":{\"31c0ca5d-ce9c-4266-a818-daca2fc979bc\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"f9758a90-ee8b-4c8c-9530-185ed2476eb1\":{\"required\":false,\"sortOrder\":1}}}]}}}},\"navigation\":{\"navs\":{\"ff6e8234-030e-45df-b853-038a1596d0d6\":{\"name\":\"Main\",\"handle\":\"main\",\"structure\":{\"uid\":\"c7e6bf8f-64c8-4519-ab60-71b713e1b952\",\"maxLevels\":null},\"instructions\":\"\",\"propagateNodes\":false,\"sortOrder\":1},\"29ad6ae7-f297-4df5-a060-2dd77f791715\":{\"name\":\"Footer\",\"handle\":\"footer\",\"structure\":{\"uid\":\"91a55e8a-5840-443a-945a-ca70b2c8fa55\",\"maxLevels\":null},\"instructions\":\"\",\"propagateNodes\":false,\"sortOrder\":2}}},\"superTableBlockTypes\":{\"8d579288-0fa0-4b9d-a72e-8d1f70b14ea9\":{\"field\":\"fab33f57-c399-476a-a1fb-81187789afab\",\"fields\":{\"2d40f002-a1ac-4a77-a0be-73840894988c\":{\"name\":\"Address\",\"handle\":\"address\",\"instructions\":\"\",\"searchable\":false,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"ab0defb0-2946-4179-813f-b89a1e432ae1\":{\"name\":\"Contact\",\"handle\":\"contact\",\"instructions\":\"\",\"searchable\":false,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Full.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"142eedd9-6032-4808-9f46-0bb81a9ba721\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"2d40f002-a1ac-4a77-a0be-73840894988c\":{\"required\":false,\"sortOrder\":1},\"ab0defb0-2946-4179-813f-b89a1e432ae1\":{\"required\":false,\"sortOrder\":2}}}]}}},\"8d96cd19-0c12-417d-92fc-7e564c84cf4f\":{\"field\":\"adbaaeb0-399d-40a8-858c-2a2d78782bd6\",\"fields\":{\"3d7281b4-7482-4132-ba0b-9dc9ebafc6f3\":{\"name\":\"Button\",\"handle\":\"button\",\"instructions\":\"\",\"searchable\":false,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"fruitstudios\\\\linkit\\\\fields\\\\LinkitField\",\"settings\":{\"selectLinkText\":\"\",\"types\":{\"fruitstudios\\\\linkit\\\\models\\\\Email\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Phone\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Url\":{\"enabled\":\"1\",\"customLabel\":\"\",\"customPlaceholder\":\"\",\"allowAlias\":\"1\",\"allowMailto\":\"1\",\"allowHash\":\"1\",\"allowPaths\":\"1\"},\"fruitstudios\\\\linkit\\\\models\\\\Twitter\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Facebook\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Instagram\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\LinkedIn\":{\"enabled\":\"\",\"customLabel\":\"\",\"customPlaceholder\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Entry\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Category\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\Asset\":{\"enabled\":\"1\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\"},\"fruitstudios\\\\linkit\\\\models\\\\User\":{\"enabled\":\"\",\"customLabel\":\"\",\"sources\":\"*\",\"customSelectionLabel\":\"\",\"userPath\":\"\"}},\"allowCustomText\":\"1\",\"defaultText\":\"More information\",\"allowTarget\":\"1\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"5c80be3f-0805-4a3e-ab3f-dddf458925d2\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":false,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":true,\"removeInlineStyles\":\"1\",\"removeEmptyTags\":\"1\",\"removeNbsp\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"6b9242c5-0f14-4d13-8878-450a34496844\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":false,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"d648955c-8286-4939-821d-59b11a6848b6\":{\"name\":\"Image\",\"handle\":\"img\",\"instructions\":\"\",\"searchable\":false,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":[\"volume:d6433ca4-70e0-4884-9992-cac1270c71a7\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null}},\"fieldLayouts\":{\"0c4e1b75-dc36-453e-a835-10b7aebcafaf\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"3d7281b4-7482-4132-ba0b-9dc9ebafc6f3\":{\"required\":false,\"sortOrder\":4},\"5c80be3f-0805-4a3e-ab3f-dddf458925d2\":{\"required\":false,\"sortOrder\":3},\"6b9242c5-0f14-4d13-8878-450a34496844\":{\"required\":false,\"sortOrder\":2},\"d648955c-8286-4939-821d-59b11a6848b6\":{\"required\":false,\"sortOrder\":1}}}]}}},\"65bc918b-04dd-494d-a5ea-4895e9df9c33\":{\"field\":\"8c24c5c8-5700-4e3b-90a0-03a2c7b3ce84\",\"fields\":{\"1ec122b3-7beb-4f58-ba83-bb3efa75d788\":{\"name\":\"Anchor Programme\",\"handle\":\"anchorProgramme\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"a08b7688-8292-4d45-aa4a-2d2d3c223926\":{\"name\":\"Special Initiatives\",\"handle\":\"specialInitiatives\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"ab1cc7de-259d-47af-8f3c-525607140740\":{\"name\":\"Open Programme\",\"handle\":\"openProgramme\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"ae011801-7095-44d1-818f-ea3317e67c45\":{\"name\":\"Grant\",\"handle\":\"grant\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Entries\",\"settings\":{\"sources\":[\"section:e74dd188-369d-4999-be24-4a6a51e9ba53\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null},\"bea98579-21f5-4521-ae77-e8b984d4d183\":{\"name\":\"Grants Awarded\",\"handle\":\"grantsAwarded\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"d2bc787b-4d4c-4324-ba96-b98a79368cdc\":{\"name\":\"Total Applications\",\"handle\":\"totalApplications\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"da744d55-dcbe-4799-8267-0d12dc4c9717\":{\"name\":\"Eligible Applications\",\"handle\":\"eligibleApplications\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"fc320066-0d01-4226-ae66-2a0e31bb9add\":{\"name\":\"Total\",\"handle\":\"total\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"881a7d78-9089-4dde-afe2-0cf15f0f3481\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"1ec122b3-7beb-4f58-ba83-bb3efa75d788\":{\"required\":false,\"sortOrder\":8},\"a08b7688-8292-4d45-aa4a-2d2d3c223926\":{\"required\":false,\"sortOrder\":7},\"ab1cc7de-259d-47af-8f3c-525607140740\":{\"required\":false,\"sortOrder\":3},\"ae011801-7095-44d1-818f-ea3317e67c45\":{\"required\":true,\"sortOrder\":1},\"bea98579-21f5-4521-ae77-e8b984d4d183\":{\"required\":false,\"sortOrder\":4},\"d2bc787b-4d4c-4324-ba96-b98a79368cdc\":{\"required\":false,\"sortOrder\":6},\"da744d55-dcbe-4799-8267-0d12dc4c9717\":{\"required\":false,\"sortOrder\":5},\"fc320066-0d01-4226-ae66-2a0e31bb9add\":{\"required\":false,\"sortOrder\":2}}}]}}},\"068d7fcc-4fa4-457a-8265-5a8bee5142ff\":{\"field\":\"95862b21-7af5-49d6-93d5-35d7ce2049cd\",\"fields\":{\"aa70b34b-602b-455d-81a4-618becd07fb0\":{\"name\":\"Copy\",\"handle\":\"copy\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"b3bcde79-4b62-4d04-bdfe-0dfc96c25a21\":{\"name\":\"Subheading\",\"handle\":\"subheading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"e1d27be8-0395-42ce-bac2-641e572ca2e1\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"e61676d9-8671-407b-9c83-0d3a8128a5ab\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"aa70b34b-602b-455d-81a4-618becd07fb0\":{\"required\":false,\"sortOrder\":3},\"b3bcde79-4b62-4d04-bdfe-0dfc96c25a21\":{\"required\":false,\"sortOrder\":2},\"e1d27be8-0395-42ce-bac2-641e572ca2e1\":{\"required\":false,\"sortOrder\":1}}}]}}}},\"categoryGroups\":{\"89827e85-e074-46eb-b2d7-1a2ca2a6be33\":{\"name\":\"Priorities\",\"handle\":\"priorities\",\"structure\":{\"uid\":\"c2e462ae-43d0-4166-877d-4d68ac77c369\",\"maxLevels\":null},\"siteSettings\":{\"07417ec7-13a1-46da-9b82-bb6dde53d07d\":{\"hasUrls\":false,\"uriFormat\":null,\"template\":null}}}}}','[]','NSTVjUaibsgE','2019-09-23 17:41:02','2019-09-23 17:41:02','c6fb6a82-1644-4721-9777-7a717237e3c8');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixblocks`
--

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `matrixblocks` VALUES (78,2,36,5,1,NULL,'2019-12-12 19:01:06','2019-12-12 19:01:06','5ed70f6f-e9ca-41dc-b20b-4ea0b816ad42'),(79,2,9,6,1,NULL,'2019-12-12 19:01:06','2019-12-12 19:01:06','8b9266f4-e53b-4da1-8c8f-53084ac8ee35'),(81,80,36,5,1,NULL,'2019-12-12 19:01:06','2019-12-12 19:01:06','0c87ffd9-2f0f-4820-a6fd-4f535517c4ff'),(82,80,9,6,1,NULL,'2019-12-12 19:01:06','2019-12-12 19:01:06','663a73ca-f22f-4a7c-a0da-c9724fe104c7'),(84,83,36,5,1,NULL,'2019-12-12 19:01:17','2019-12-12 19:01:17','b80e05e3-8aaf-4dd8-a2d2-fcdb98824b0a'),(85,83,9,6,1,NULL,'2019-12-12 19:01:17','2019-12-12 19:01:17','a6f3ca08-ea38-4e01-acc2-27a2687f8426'),(86,2,9,9,2,NULL,'2019-12-12 19:01:48','2019-12-12 19:01:48','bbb73a74-fc79-4255-8b3b-a146366aada5'),(88,87,36,5,1,NULL,'2019-12-12 19:01:48','2019-12-12 19:01:48','70beff67-dbdf-4bd6-acb5-e791f1f7839b'),(89,87,9,6,1,NULL,'2019-12-12 19:01:48','2019-12-12 19:01:48','73e03a0c-fddc-4f63-b69a-3f698130c9c8'),(90,87,9,9,2,NULL,'2019-12-12 19:01:48','2019-12-12 19:01:48','11156131-bc6a-49df-ad3e-c716e38dd9dc'),(95,94,36,5,1,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','206d5e3e-77d6-4410-8103-2072eb34d413'),(96,94,9,6,1,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','7e90b6dd-dd6b-46c1-9992-c21dd332e0c1'),(97,94,9,9,2,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','811c09f4-980d-4283-88cd-1a101d9ce8d5'),(101,2,9,7,3,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','ae409dd3-e222-4f43-9340-5c5e32faa32e'),(106,105,36,5,1,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','473b570f-6b6c-48cc-b998-d02af1e70a27'),(107,105,9,6,1,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','989ffaac-82d7-4c92-8982-a2de8717a2e9'),(108,105,9,9,2,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','887ba329-a079-4fd2-a696-74b694e66c3e'),(112,105,9,7,3,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','f5a4907a-a21d-4d2e-bec0-49f239c40195'),(116,2,9,8,4,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','5b7cf03a-41d3-451f-862f-a94622e9f407'),(118,117,36,5,1,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','cc1ed725-0418-469b-a6c3-02bd0f06845f'),(119,117,9,6,1,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','be4db747-991e-48ac-845e-1ec7efd335a6'),(120,117,9,9,2,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','e484c47c-259b-4d4b-81fa-44e4bf11eaa4'),(124,117,9,7,3,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','43c64acf-6aa7-413d-82c6-37972bb5b331'),(128,117,9,8,4,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','07070809-ee6c-45d4-bfba-9b5e2c47cd82'),(130,129,36,5,1,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','7520dcc6-d239-4ea6-b277-60c4f0bf12f6'),(131,129,9,6,1,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','2deea8d7-c39b-4d49-9179-37be9f9d9178'),(132,129,9,9,2,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','f231140d-c920-4d40-b918-60c6a55f95f7'),(136,129,9,7,3,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','7278f079-b300-4b99-8da8-3b010dbe30a7'),(140,129,9,8,4,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','8388a916-ee8c-44b3-8829-924f12837f40');
/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixblocktypes`
--

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `matrixblocktypes` VALUES (1,9,1,'Banner','banner',5,'2019-09-23 18:36:27','2019-12-12 18:59:25','931ca6dc-d223-4440-8cbb-ea475c74f68a'),(2,9,10,'Heading Intro','headingIntro',6,'2019-09-26 17:23:45','2019-12-12 18:59:25','55d6a029-72ae-4193-aea3-03503b541ec0'),(3,9,12,'Call To Actions','callToActions',7,'2019-09-26 17:23:45','2019-12-12 18:59:25','856ab452-c117-4e63-964c-a2f9ed79b513'),(4,9,13,'Plain Text','plainText',8,'2019-09-26 17:24:40','2019-12-12 18:59:25','b25958b8-5ce9-417d-acc4-b5e62b474364'),(5,36,18,'Slide','slide',1,'2019-12-12 18:42:29','2019-12-12 18:42:29','39ab8299-a798-4dcf-ac66-971ea32d348e'),(6,9,19,'Priority Area','priorityArea',1,'2019-12-12 18:43:29','2019-12-12 18:43:29','328a1237-033b-41c0-9791-3bfaf5745aec'),(7,9,21,'Prior Funding','priorFunding',3,'2019-12-12 18:51:05','2019-12-12 19:02:36','89e7ab67-dbff-4d57-a988-093f822c55af'),(8,9,22,'Apply','apply',4,'2019-12-12 18:51:05','2019-12-12 18:59:25','e6fd156c-448b-4e4b-8e02-b586c03a0c9c'),(9,9,24,'Funding Programmes','fundingProgrammes',2,'2019-12-12 18:59:25','2019-12-12 18:59:25','4ec04e59-3125-4b70-8d6a-2d70ef28160d'),(10,67,25,'Add Content','addContent',1,'2019-12-17 16:16:59','2019-12-17 16:16:59','0ccfd92a-bc4e-4a32-8057-68a2e1fa531a'),(11,71,27,'Add Content','addContent',1,'2019-12-17 16:22:58','2019-12-17 16:22:58','69d2fc25-47b9-4048-b94e-344974b24070'),(12,74,28,'Add Content','addContent',1,'2019-12-17 17:25:35','2019-12-17 17:25:35','d5ed11bd-cf0a-4d36-9de0-aa82a155fa7b');
/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixcontent_banner`
--

LOCK TABLES `matrixcontent_banner` WRITE;
/*!40000 ALTER TABLE `matrixcontent_banner` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `matrixcontent_banner` VALUES (1,78,1,'2019-12-12 19:01:06','2019-12-16 11:57:28','6bca3a1c-4454-4c5f-8ff8-57f2fb07d992','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(2,81,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','362b30bd-864e-497d-81e0-d0be15f26475','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(3,84,1,'2019-12-12 19:01:17','2019-12-12 19:01:17','b02f8b73-ff02-4d80-ac2f-17240699769e','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(4,88,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','ea1fa71e-27f1-45c2-b8fc-acf40de215c9','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(5,95,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','fff7af1d-efa5-42ca-89de-0e3faf5ae523','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(6,106,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','1bc0dc03-07a6-4948-b7b5-83488b1dfd7d','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(7,118,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','66e2d218-25e6-4af8-82d6-d5b4a536446e','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity'),(8,130,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','cea6adcc-c704-473a-a2a2-d65e6971d568','<p>We promote human dignity and defend human rights for marginalised and excluded people.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"16\",\"customText\":\"\",\"target\":\"\"}','Promoting Human Dignity');
/*!40000 ALTER TABLE `matrixcontent_banner` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixcontent_contentblocks`
--

LOCK TABLES `matrixcontent_contentblocks` WRITE;
/*!40000 ALTER TABLE `matrixcontent_contentblocks` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `matrixcontent_contentblocks` VALUES (1,79,1,'2019-12-12 19:01:06','2019-12-16 11:57:28','350d17f5-85c3-4a81-854d-9c443fd64b7e',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,82,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','cbf031cf-e210-4a67-9a67-ee2f8d570e5c',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,85,1,'2019-12-12 19:01:17','2019-12-12 19:01:17','51f76e7c-17c5-4d8f-9da6-78e5b1b33ba9',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,86,1,'2019-12-12 19:01:48','2019-12-16 11:57:28','7e750484-8764-4b93-a796-cffa9a652818',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Our funding programmes','<p>We don’t reach people in need directly – instead, we pursue our purpose by funding partner organisations. We have three grant-making programmes.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"24\",\"customText\":\"\",\"target\":\"\"}',NULL),(5,89,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','c54ab707-4216-4762-881c-eda191085c63',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,90,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','61d5fab9-2f51-48c5-abf1-c9b5cc860038',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]',NULL),(7,96,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','5e068893-d19f-4a9b-a133-5530836d769a',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,97,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','a97e8cce-d2d8-4ca2-a3e4-0113e6f80ce9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Our funding programmes','<p>We don’t reach people in need directly – instead, we pursue our purpose by funding partner organisations. We have three grant-making programmes.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"24\",\"customText\":\"\",\"target\":\"\"}',NULL),(9,101,1,'2019-12-12 19:05:20','2019-12-16 11:57:28','100515dd-38a3-48e4-a3f8-360eef1c8258',NULL,NULL,NULL,NULL,NULL,NULL,'Funding total for 2018/19','£1,663,225','£1,750,000','Grants awarded in 2018/19','What we plan to give in 2020',NULL,NULL,NULL,NULL,NULL,NULL),(10,107,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','7c268e1b-dbe7-4d1d-a24a-5efd5e049bc8',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,108,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','51183038-46d9-4c85-9262-dd8a4b011672',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Our funding programmes','<p>We don’t reach people in need directly – instead, we pursue our purpose by funding partner organisations. We have three grant-making programmes.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"24\",\"customText\":\"\",\"target\":\"\"}',NULL),(12,112,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','c769d370-202c-42b1-896f-287b5da1231f',NULL,NULL,NULL,NULL,NULL,NULL,'Funding total for 2018/19','£1,663,225','£1,750,000',NULL,'What we plan to give in 2020',NULL,NULL,NULL,NULL,NULL,NULL),(13,116,1,'2019-12-12 19:05:53','2019-12-16 11:57:28','88c91934-efc3-413e-ac07-fa29f3be4b77',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Find out more about our priority areas below, or check your eligibility in our Apply section</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"27\",\"customText\":\"\",\"target\":\"\"}',NULL,NULL,NULL,NULL),(14,119,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','cb8b6a59-72a5-4897-b98a-decaa15f1cdb',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,120,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','5ac4a413-6654-4fe2-80a3-76318dc680c8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Our funding programmes','<p>We don’t reach people in need directly – instead, we pursue our purpose by funding partner organisations. We have three grant-making programmes.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"24\",\"customText\":\"\",\"target\":\"\"}',NULL),(16,124,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','cc0026f9-cbb9-47df-9ccb-3f744db2596c',NULL,NULL,NULL,NULL,NULL,NULL,'Funding total for 2018/19','£1,663,225','£1,750,000',NULL,'What we plan to give in 2020',NULL,NULL,NULL,NULL,NULL,NULL),(17,128,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','5a2a6fee-679b-44ae-af90-dd90eaeb4819',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Find out more about our priority areas below, or check your eligibility in our Apply section</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"27\",\"customText\":\"\",\"target\":\"\"}',NULL,NULL,NULL,NULL),(18,131,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','dd57092f-31f7-4321-8684-66ce190f1e7c',NULL,NULL,NULL,NULL,NULL,'Our three priority areas:',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,132,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','aa61a73b-135b-4de4-b0f4-37382483731e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Our funding programmes','<p>We don’t reach people in need directly – instead, we pursue our purpose by funding partner organisations. We have three grant-making programmes.</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"24\",\"customText\":\"\",\"target\":\"\"}',NULL),(20,136,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','28f084e8-15d6-43e6-b0c6-e47686beb6e8',NULL,NULL,NULL,NULL,NULL,NULL,'Funding total for 2018/19','£1,663,225','£1,750,000','Grants awarded in 2018/19','What we plan to give in 2020',NULL,NULL,NULL,NULL,NULL,NULL),(21,140,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','b38f96fd-a555-4230-953a-e70f061521df',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Find out more about our priority areas below, or check your eligibility in our Apply section</p>','{\"type\":\"fruitstudios\\\\linkit\\\\models\\\\Entry\",\"value\":\"27\",\"customText\":\"\",\"target\":\"\"}',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `matrixcontent_contentblocks` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixcontent_eligibility`
--

LOCK TABLES `matrixcontent_eligibility` WRITE;
/*!40000 ALTER TABLE `matrixcontent_eligibility` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `matrixcontent_eligibility` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixcontent_statstics`
--

LOCK TABLES `matrixcontent_statstics` WRITE;
/*!40000 ALTER TABLE `matrixcontent_statstics` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `matrixcontent_statstics` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `matrixcontent_whatwefund`
--

LOCK TABLES `matrixcontent_whatwefund` WRITE;
/*!40000 ALTER TABLE `matrixcontent_whatwefund` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `matrixcontent_whatwefund` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `migrations` VALUES (1,NULL,'app','Install','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','209ebdee-f5a6-4d3d-b370-1e64e8159247'),(2,NULL,'app','m150403_183908_migrations_table_changes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f7f13982-6adf-4c2f-aba8-ffd2f0587f02'),(3,NULL,'app','m150403_184247_plugins_table_changes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','25c613e0-4349-4001-a16f-7c582f47b08d'),(4,NULL,'app','m150403_184533_field_version','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','861539fd-9c5e-4b54-b7cf-c032f5e3a275'),(5,NULL,'app','m150403_184729_type_columns','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','dd20ced7-cf26-47d4-b6a3-af94a4d25d05'),(6,NULL,'app','m150403_185142_volumes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2438e5db-278d-4dc8-bd67-b89f3d13fe75'),(7,NULL,'app','m150428_231346_userpreferences','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b75a0d6f-d612-4267-bb65-badea3cb557b'),(8,NULL,'app','m150519_150900_fieldversion_conversion','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','75e47d21-9dce-43bf-a77e-c5fcf06c75d4'),(9,NULL,'app','m150617_213829_update_email_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','cedf0d9b-efcb-4c34-85a6-a18bb43577c5'),(10,NULL,'app','m150721_124739_templatecachequeries','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','79d1c43d-2278-4a55-aa13-738c67ff66b3'),(11,NULL,'app','m150724_140822_adjust_quality_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','8f63a5de-afd3-46e9-86eb-1c604865feab'),(12,NULL,'app','m150815_133521_last_login_attempt_ip','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','808f4ac6-4647-4b1b-be50-8dcb624fe511'),(13,NULL,'app','m151002_095935_volume_cache_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','58c9a104-121f-4206-8ff1-94be9e59d09a'),(14,NULL,'app','m151005_142750_volume_s3_storage_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2b970c82-7e2f-43ac-bee0-cc7e8f4bacc6'),(15,NULL,'app','m151016_133600_delete_asset_thumbnails','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','74c9c806-3188-4b8c-8ee4-f5a969f91dfb'),(16,NULL,'app','m151209_000000_move_logo','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','36eadf7c-b52e-44c4-8d17-f70f9b14998f'),(17,NULL,'app','m151211_000000_rename_fileId_to_assetId','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1653c8a8-1aec-4007-8846-9332de43fa09'),(18,NULL,'app','m151215_000000_rename_asset_permissions','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f43c567b-9e84-4f96-8fd0-e13eddde4a13'),(19,NULL,'app','m160707_000001_rename_richtext_assetsource_setting','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','9b11c378-f529-414a-a93a-719649ba6a99'),(20,NULL,'app','m160708_185142_volume_hasUrls_setting','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f95e8631-0dd2-4806-adc8-80a4317c31f2'),(21,NULL,'app','m160714_000000_increase_max_asset_filesize','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','03b93916-2033-41ff-800f-4a5f14af7dad'),(22,NULL,'app','m160727_194637_column_cleanup','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','db8778f6-d7e5-44cf-807b-ce116150fb79'),(23,NULL,'app','m160804_110002_userphotos_to_assets','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d9c45aaf-e1e2-465f-8d5f-60f437b64a82'),(24,NULL,'app','m160807_144858_sites','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','cc82b21b-e348-44b3-b6a8-485ee8f8bcdb'),(25,NULL,'app','m160829_000000_pending_user_content_cleanup','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2ded7307-3da8-4e68-a378-680df9ae3678'),(26,NULL,'app','m160830_000000_asset_index_uri_increase','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','52736fa2-a718-4b9e-acce-454ca7d3e0ea'),(27,NULL,'app','m160912_230520_require_entry_type_id','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','55887fb6-1514-4b6b-a41d-66deee92bcf7'),(28,NULL,'app','m160913_134730_require_matrix_block_type_id','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','223b66a3-df05-4ebf-9164-5f0326f3eb5e'),(29,NULL,'app','m160920_174553_matrixblocks_owner_site_id_nullable','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','666cb888-7ba5-4982-8506-43ff7f475e5f'),(30,NULL,'app','m160920_231045_usergroup_handle_title_unique','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2f06ef72-0081-420a-b5fa-eb83e376f97f'),(31,NULL,'app','m160925_113941_route_uri_parts','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','5e941b37-7393-4b7b-a69c-2d2f055747e0'),(32,NULL,'app','m161006_205918_schemaVersion_not_null','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','6c86d6cf-775b-495e-a9af-333c2039cce1'),(33,NULL,'app','m161007_130653_update_email_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','562b6a11-51e0-4d3b-9009-5322526d50c4'),(34,NULL,'app','m161013_175052_newParentId','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e05e83c0-7774-4daf-802d-b4d3339bdb67'),(35,NULL,'app','m161021_102916_fix_recent_entries_widgets','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','29206231-f70d-4e51-883f-b6117b8225b6'),(36,NULL,'app','m161021_182140_rename_get_help_widget','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','543b511e-a6c0-4363-b5e7-4647658bbfea'),(37,NULL,'app','m161025_000000_fix_char_columns','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','8808679f-991e-457e-8957-5bfe8c2b78e0'),(38,NULL,'app','m161029_124145_email_message_languages','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f1d59710-6b57-408e-80b2-7851a2a12319'),(39,NULL,'app','m161108_000000_new_version_format','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','a03f1bf4-60d6-4840-97a5-adc84e16c75b'),(40,NULL,'app','m161109_000000_index_shuffle','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','91e0150f-c439-4825-bf34-e92f96d64e24'),(41,NULL,'app','m161122_185500_no_craft_app','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','88d79790-b8bb-4e0a-b766-49681d5a3c7d'),(42,NULL,'app','m161125_150752_clear_urlmanager_cache','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','0177440f-dab9-44d7-b80a-756ee4e70038'),(43,NULL,'app','m161220_000000_volumes_hasurl_notnull','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','4ede1fa7-716a-428f-bae7-cbecdcf89d83'),(44,NULL,'app','m170114_161144_udates_permission','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','06e64491-093e-43b5-a761-3745dc30a648'),(45,NULL,'app','m170120_000000_schema_cleanup','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2197e176-cc7c-4e43-8b78-fe1e1178c394'),(46,NULL,'app','m170126_000000_assets_focal_point','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','db2dc082-0677-4f4f-9a9a-457fe58843b1'),(47,NULL,'app','m170206_142126_system_name','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','5e82dbda-add9-4c21-803a-f69545275815'),(48,NULL,'app','m170217_044740_category_branch_limits','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','77c2c0fb-4b39-42ee-9ee9-dfbfcdda216a'),(49,NULL,'app','m170217_120224_asset_indexing_columns','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','828aa237-733f-4ba1-861f-c9a54d9fb7db'),(50,NULL,'app','m170223_224012_plain_text_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b7e2beb0-a779-4922-8376-5c8447b0a5a8'),(51,NULL,'app','m170227_120814_focal_point_percentage','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','af973467-ab6e-486b-94e1-c679129a92d0'),(52,NULL,'app','m170228_171113_system_messages','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','a537e55e-e7ee-4332-a53d-57b6f1f6c02e'),(53,NULL,'app','m170303_140500_asset_field_source_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','51e61b72-4c07-4305-aa4c-8f18948876da'),(54,NULL,'app','m170306_150500_asset_temporary_uploads','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','4d669382-abb7-4d48-9f46-7d721959f53d'),(55,NULL,'app','m170523_190652_element_field_layout_ids','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e625c147-1bad-421c-b3e3-5cadd99c6223'),(56,NULL,'app','m170612_000000_route_index_shuffle','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','42c5ce73-92dc-4a4c-85db-f1474a0aace7'),(57,NULL,'app','m170621_195237_format_plugin_handles','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b4cc1bf2-516a-4118-ad8f-c2ab978d47fa'),(58,NULL,'app','m170630_161027_deprecation_line_nullable','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','3d650a5b-6d74-4fb1-89eb-23163bbd656a'),(59,NULL,'app','m170630_161028_deprecation_changes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','da7a33a3-0ffc-4b48-ac1f-10f2e71f3e10'),(60,NULL,'app','m170703_181539_plugins_table_tweaks','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','c0f465ff-fc2d-484b-9d3c-c0867c331fe5'),(61,NULL,'app','m170704_134916_sites_tables','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b913def1-9025-4957-b99a-f30ae0d3b756'),(62,NULL,'app','m170706_183216_rename_sequences','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2468e00a-e2c3-4f91-b0b9-d68fda813d6a'),(63,NULL,'app','m170707_094758_delete_compiled_traits','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','78d1610e-e0b2-4d89-80d5-05de634c1fc0'),(64,NULL,'app','m170731_190138_drop_asset_packagist','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','7ec66cbc-d1f9-4c45-bfbf-43fd5eb71e16'),(65,NULL,'app','m170810_201318_create_queue_table','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','6b16a7d4-f23b-4071-bdf9-9ea1546b83f0'),(66,NULL,'app','m170816_133741_delete_compiled_behaviors','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','4c3a4e11-05f6-4326-b46a-97fa7a0d0d9f'),(67,NULL,'app','m170903_192801_longblob_for_queue_jobs','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','92a1110e-0c53-43e5-9a6f-8827b89e1e40'),(68,NULL,'app','m170914_204621_asset_cache_shuffle','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f69bdb32-ed59-48c8-9a89-d4395ca88c14'),(69,NULL,'app','m171011_214115_site_groups','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','9a8a067d-2272-4124-860a-189c8b277720'),(70,NULL,'app','m171012_151440_primary_site','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','c6e92ffe-7429-41fd-8749-6518ac8b1d54'),(71,NULL,'app','m171013_142500_transform_interlace','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b993df50-fa2f-4b47-9174-189cc1a3df29'),(72,NULL,'app','m171016_092553_drop_position_select','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','78151490-e157-4b86-b585-812ffa13312d'),(73,NULL,'app','m171016_221244_less_strict_translation_method','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','997c1a4b-8717-4a66-aced-bed201ef7bdc'),(74,NULL,'app','m171107_000000_assign_group_permissions','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','4742bb40-f9ac-4719-8b23-6c3d1bb8db19'),(75,NULL,'app','m171117_000001_templatecache_index_tune','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','77999a89-f488-4d7d-aefa-78768b35810b'),(76,NULL,'app','m171126_105927_disabled_plugins','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','93a927c3-475c-4ca1-a625-b4d6d1a67f41'),(77,NULL,'app','m171130_214407_craftidtokens_table','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','ed9c9789-bdba-4c3f-a75a-00402ca055ec'),(78,NULL,'app','m171202_004225_update_email_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d5802def-3620-4733-b788-59112b61f82d'),(79,NULL,'app','m171204_000001_templatecache_index_tune_deux','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','57bfeab6-c3ab-44a7-8221-fe66bace2bb6'),(80,NULL,'app','m171205_130908_remove_craftidtokens_refreshtoken_column','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','50a3df6d-0fdc-47ac-8ee3-46feb744141a'),(81,NULL,'app','m171218_143135_longtext_query_column','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1d5f3283-1c64-488e-9684-00adc0a85d96'),(82,NULL,'app','m171231_055546_environment_variables_to_aliases','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','83fabea6-1977-43f5-a322-de8c2fa988f6'),(83,NULL,'app','m180113_153740_drop_users_archived_column','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','0f32901b-af27-4258-9c11-33eff99e5bc6'),(84,NULL,'app','m180122_213433_propagate_entries_setting','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1646691b-0bf5-4a32-a205-e28035509190'),(85,NULL,'app','m180124_230459_fix_propagate_entries_values','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','2cb4ee49-d26f-41f9-b914-7bb70879221c'),(86,NULL,'app','m180128_235202_set_tag_slugs','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','0d93f928-d1a7-497a-9fc5-c1893c2e201b'),(87,NULL,'app','m180202_185551_fix_focal_points','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','10c4979f-752b-4b02-a972-0c2171f5f7bd'),(88,NULL,'app','m180217_172123_tiny_ints','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1aa3967f-fa30-4983-b988-a3d55d720a03'),(89,NULL,'app','m180321_233505_small_ints','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','bb7bb40a-a18d-49cb-bd1a-254af60d1ca2'),(90,NULL,'app','m180328_115523_new_license_key_statuses','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1347d99c-f0d1-4070-b155-f05d6dd13426'),(91,NULL,'app','m180404_182320_edition_changes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','6bbdbe50-573f-4a49-8cb3-cf3bd3d7a2be'),(92,NULL,'app','m180411_102218_fix_db_routes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','9b865fef-d5d6-4116-a74a-f50fed6ce22a'),(93,NULL,'app','m180416_205628_resourcepaths_table','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','4ea5af4b-5ded-4941-9449-33edf0730bfa'),(94,NULL,'app','m180418_205713_widget_cleanup','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','48eb652c-f718-4bea-812e-fe4b87f24616'),(95,NULL,'app','m180425_203349_searchable_fields','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','7067936d-51d4-4c28-8083-5c30832ec46d'),(96,NULL,'app','m180516_153000_uids_in_field_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','96d4e384-c078-4e76-8aa8-d09d6f89df86'),(97,NULL,'app','m180517_173000_user_photo_volume_to_uid','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d86e1253-5574-4207-af51-f773011bd9f2'),(98,NULL,'app','m180518_173000_permissions_to_uid','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','33413c01-1478-4241-8ba6-f71dfcf7aabb'),(99,NULL,'app','m180520_173000_matrix_context_to_uids','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','c14a8925-2147-4caf-8848-b93102587f52'),(100,NULL,'app','m180521_173000_initial_yml_and_snapshot','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d2668761-60c4-41bb-b152-65ba63b93f94'),(101,NULL,'app','m180731_162030_soft_delete_sites','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','5216fb6a-2c15-4d26-bb96-452909ebbab5'),(102,NULL,'app','m180810_214427_soft_delete_field_layouts','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','8413ad64-c2b6-4ab9-a226-fac3e884c242'),(103,NULL,'app','m180810_214439_soft_delete_elements','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','639f4ae2-1250-4ba8-99eb-53b588715673'),(104,NULL,'app','m180824_193422_case_sensitivity_fixes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','a1d16229-d6c9-4533-a874-f9654c534a01'),(105,NULL,'app','m180901_151639_fix_matrixcontent_tables','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','78e32d02-ab19-4b47-9d2f-73858fa4e96b'),(106,NULL,'app','m180904_112109_permission_changes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','360153c0-cd9a-467d-95f0-54c8d799225e'),(107,NULL,'app','m180910_142030_soft_delete_sitegroups','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d943a5ff-a125-4e23-a974-d8917faae90a'),(108,NULL,'app','m181011_160000_soft_delete_asset_support','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','fa9f524a-196f-4799-8568-24343b336dca'),(109,NULL,'app','m181016_183648_set_default_user_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1b6b57ba-a638-439f-826d-2a7a3aee8bdf'),(110,NULL,'app','m181017_225222_system_config_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','4e1c2b0c-4581-40dc-81b1-69afd582f5e5'),(111,NULL,'app','m181018_222343_drop_userpermissions_from_config','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f24655d9-f494-4100-8dc0-edf28b614469'),(112,NULL,'app','m181029_130000_add_transforms_routes_to_config','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','27e73c5f-61b7-4144-9fce-2e8f5b4a4858'),(113,NULL,'app','m181112_203955_sequences_table','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','41e2cb74-89bf-404a-9ab6-911470c6bf7c'),(114,NULL,'app','m181121_001712_cleanup_field_configs','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e098ae38-f0e9-425c-bd22-5c25261ec9e9'),(115,NULL,'app','m181128_193942_fix_project_config','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','1ef65f20-3e7a-4cf3-a3cb-4379411a1a22'),(116,NULL,'app','m181130_143040_fix_schema_version','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e5ee3322-f441-499a-98b4-0326bf52bf31'),(117,NULL,'app','m181211_143040_fix_entry_type_uids','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','adc32918-527a-4fae-8238-87b50b5728a1'),(118,NULL,'app','m181213_102500_config_map_aliases','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','cceab459-0ebd-4700-9cf0-081476674067'),(119,NULL,'app','m181217_153000_fix_structure_uids','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','27ca3150-293c-4e06-b5c2-6f1d98253127'),(120,NULL,'app','m190104_152725_store_licensed_plugin_editions','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','8b3a79c2-5b89-49ff-86b1-dadac2baa1d1'),(121,NULL,'app','m190108_110000_cleanup_project_config','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','97866c09-c649-47f5-9405-b6f9d23ec4c2'),(122,NULL,'app','m190108_113000_asset_field_setting_change','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d45af4dd-2348-4ced-b67b-29ccbc6f2ade'),(123,NULL,'app','m190109_172845_fix_colspan','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','20c9b6a1-9305-4e6b-bc62-530cac4624d9'),(124,NULL,'app','m190110_150000_prune_nonexisting_sites','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e67f4002-d541-4235-a255-182243169361'),(125,NULL,'app','m190110_214819_soft_delete_volumes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','7814afd1-6139-46da-b177-09bd4fef608e'),(126,NULL,'app','m190112_124737_fix_user_settings','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e0093584-6293-4f95-92a2-15bc5c8c4a0d'),(127,NULL,'app','m190112_131225_fix_field_layouts','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','140ccfba-a062-4d63-88ff-72beaf7398e1'),(128,NULL,'app','m190112_201010_more_soft_deletes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','3f31336c-a696-4eb9-999c-8d7144b30d29'),(129,NULL,'app','m190114_143000_more_asset_field_setting_changes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','602e883d-e7a7-456c-b576-e5101c746671'),(130,NULL,'app','m190121_120000_rich_text_config_setting','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','050aed9b-8716-43b3-a90c-d19ea418fa89'),(131,NULL,'app','m190125_191628_fix_email_transport_password','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','a346676a-0cce-4e60-b278-dacfdd35a1b2'),(132,NULL,'app','m190128_181422_cleanup_volume_folders','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','d381929a-6996-46fe-a37a-5e7a216ce14a'),(133,NULL,'app','m190205_140000_fix_asset_soft_delete_index','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','09cb5f78-a8a1-4f98-8e07-6e75d6d076ba'),(134,NULL,'app','m190208_140000_reset_project_config_mapping','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','3620dad7-6426-40f4-b382-f72190df1f69'),(135,NULL,'app','m190218_143000_element_index_settings_uid','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','f13b0e8f-7d97-49b1-a572-43100c1d52c3'),(136,NULL,'app','m190312_152740_element_revisions','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','5d3c8306-d65c-4894-8c16-6d8716695377'),(137,NULL,'app','m190327_235137_propagation_method','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','feaf1897-d86c-47a0-9e44-2beeb00a975f'),(138,NULL,'app','m190401_223843_drop_old_indexes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b727616e-fbd3-437d-85ef-a1047391e0d4'),(139,NULL,'app','m190416_014525_drop_unique_global_indexes','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','25efe32c-2589-4395-83fa-dc124da7f6e4'),(140,NULL,'app','m190417_085010_add_image_editor_permissions','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','9805b5f5-ffa4-4d9b-a4f5-df5bca989b1e'),(141,NULL,'app','m190502_122019_store_default_user_group_uid','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','fda322c3-dfd0-4320-9759-d389cfbb4dec'),(142,NULL,'app','m190504_150349_preview_targets','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','6b152aef-f2ae-448b-9141-e924f795bdcc'),(143,NULL,'app','m190516_184711_job_progress_label','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','051a8248-4893-4de7-b9a0-6a31066a2f7d'),(144,NULL,'app','m190523_190303_optional_revision_creators','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','17f5e8e8-9694-4874-8e5a-85024464ab7a'),(145,NULL,'app','m190529_204501_fix_duplicate_uids','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','30a9dd39-3d37-4b7b-8861-8d66af6de4c1'),(146,NULL,'app','m190605_223807_unsaved_drafts','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','38c8a3b3-c0f4-45dc-911b-d38b7eec20fc'),(147,NULL,'app','m190607_230042_entry_revision_error_tables','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','165b35af-dee2-428d-ba96-48478b3390cd'),(148,NULL,'app','m190608_033429_drop_elements_uid_idx','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e0e64b47-d159-4973-bfb9-c7f3b0790ba7'),(149,NULL,'app','m190617_164400_add_gqlschemas_table','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','b084a7a6-956d-4829-ad88-5e092e144502'),(150,NULL,'app','m190624_234204_matrix_propagation_method','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','00dabb66-4fc9-446f-8ea3-b4358b61792c'),(151,NULL,'app','m190711_153020_drop_snapshots','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','bd53233f-c531-4e87-b66a-f7c701de02ad'),(152,NULL,'app','m190712_195914_no_draft_revisions','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','e4856757-d5e0-4c6d-af23-fd7af7ddf642'),(153,NULL,'app','m190723_140314_fix_preview_targets_column','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','19183942-4131-4bc0-ade6-81411b5c8b0b'),(154,NULL,'app','m190820_003519_flush_compiled_templates','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','60cf28d7-59a3-4a8c-8865-f74cc9bed505'),(155,NULL,'app','m190823_020339_optional_draft_creators','2019-09-23 17:41:03','2019-09-23 17:41:03','2019-09-23 17:41:03','87a96075-ed86-44de-beea-cb492a0aa8e4'),(156,2,'plugin','Install','2019-09-23 18:18:50','2019-09-23 18:18:50','2019-09-23 18:18:50','26504911-1873-4047-be51-0019574e8b31'),(157,2,'plugin','m180305_000000_migrate_feeds','2019-09-23 18:18:50','2019-09-23 18:18:50','2019-09-23 18:18:50','2fd94dc7-8b85-4a63-87f3-61ed31178bd3'),(158,2,'plugin','m181113_000000_add_paginationNode','2019-09-23 18:18:50','2019-09-23 18:18:50','2019-09-23 18:18:50','46b6ca34-abf0-437d-b09c-2440ea7f5395'),(159,2,'plugin','m190201_000000_update_asset_feeds','2019-09-23 18:18:50','2019-09-23 18:18:50','2019-09-23 18:18:50','c64752ef-30ca-47e2-9d43-39126f500074'),(160,2,'plugin','m190320_000000_renameLocale','2019-09-23 18:18:50','2019-09-23 18:18:50','2019-09-23 18:18:50','3a98838f-0533-4f44-b386-7c3ff0a07e52'),(161,2,'plugin','m190406_000000_sortOrder','2019-09-23 18:18:50','2019-09-23 18:18:50','2019-09-23 18:18:50','a46058ce-c95e-4334-9c3d-78a5bb4e74e5'),(162,4,'plugin','Install','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','75021c18-0772-4217-aee5-d5d3396e5c7b'),(163,4,'plugin','m180120_140521_CraftUpgrade','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','7273af86-34e3-4bec-91b9-d891ce25d9a2'),(164,4,'plugin','m180125_124339_UpdateForeignKeyNames','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','2b35cdb0-fe7c-4424-8ae2-4dfe8be87ce2'),(165,4,'plugin','m180214_094247_AddUniqueTokenToSubmissionsAndForms','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','94dbc5e8-5aa0-4847-a48e-02114cb7fb39'),(166,4,'plugin','m180220_072652_ChangeFileUploadFieldColumnType','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','b1cdacb4-e194-42d6-8abe-20332c9e8812'),(167,4,'plugin','m180326_094124_AddIsSpamToSubmissions','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','24f4afca-d4cb-4bc3-8f6b-380a6844bf44'),(168,4,'plugin','m180405_101920_AddIpAddressToSubmissions','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','8b4176f0-f4f5-4e56-ba3a-427d1b76ab05'),(169,4,'plugin','m180410_131206_CreateIntegrationsQueue','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','229e2a67-f541-457b-b5b0-97e28da0c73d'),(170,4,'plugin','m180417_134527_AddMultipleSelectTypeToFields','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','24fc8c22-a1b5-404a-af32-c944222096c1'),(171,4,'plugin','m180430_151626_PaymentGateways','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','d173d94d-7c8c-4a8c-839b-acbf187ebff4'),(172,4,'plugin','m180508_095131_CreatePaymentGatewayFieldsTable','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','bcc0858f-4735-4f4e-badf-dd879bf29248'),(173,4,'plugin','m180606_141402_AddConnectionsToFormProperties','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','b9d825cc-5c2d-4b93-82f5-98a9bfb46e29'),(174,4,'plugin','m180730_171628_AddCcDetailsFieldType','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','e0c172aa-8f49-4bb0-80ad-353c5b5e1037'),(175,4,'plugin','m180817_091801_AddRulesToFormProperties','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','9a80078f-dd67-480b-903b-5d25aea2fdc8'),(176,4,'plugin','m181112_152751_ChangeTypeEnumColumnsToIndexedText','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','bbb2eb87-c555-4b3d-80ef-74f5708d2386'),(177,4,'plugin','m181129_083939_ChangeIntegrationFieldTypeColumnTypeToString','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','3d0d52e7-c734-4723-8479-a4712495311b'),(178,4,'plugin','m190501_124050_MergingEditionsMigration','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','c6513454-cdcd-42c5-b011-fa1571cb06d8'),(179,4,'plugin','m190502_155557_AddCCAndBCCToEmailNotifications','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','3633818e-7432-4c7f-965b-663e2e68ca5e'),(180,4,'plugin','m190516_085150_AddPresetAssetsToNotifications','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','607db063-c8e5-42fa-a860-1a946cc3ef76'),(181,4,'plugin','m190529_135307_AddWebhookTables','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','f9787583-3208-482b-adef-500e0caac47a'),(182,4,'plugin','m190603_160423_UpgradeFreeformHoneypotEnhancement','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','10edd05e-8fc6-47f0-b4e1-cd4d08688319'),(183,4,'plugin','m190604_125112_AddFormLimitSubmissionProperty','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','712bce6c-9bb1-4032-945a-4f84e72f9045'),(184,4,'plugin','m190610_074840_MigrateScriptInsertLocation','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','c72a6787-88b7-4e5c-a1ef-99669ff7acf9'),(185,4,'plugin','m190614_103420_AddMissingMetaColumnsToProAndPaymentTables','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','e6b4fd41-a868-48b9-88a7-fdc74f9246c0'),(186,4,'plugin','m190617_122427_RemoveBrokenForeignKeys','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','0ec014db-e6f8-4fc3-9faf-407bb6cc2fa2'),(187,4,'plugin','m190618_142759_AddFixedForeignKeys','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','70047b55-3ae9-45d3-9239-2c4da2dc68c5'),(188,4,'plugin','m190812_125059_AddNotesTable','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','cadcea6a-31ed-4b5a-91c2-5e27c9134f69'),(189,4,'plugin','m190905_113428_FixIntervalCountNotNullColumn','2019-09-23 18:18:58','2019-09-23 18:18:58','2019-09-23 18:18:58','9b3db7a8-2b3c-4a34-9c68-48606343311f'),(190,6,'plugin','Install','2019-09-23 18:19:05','2019-09-23 18:19:05','2019-09-23 18:19:05','7c592a5a-6856-4f08-b9d8-ef603dd0cb16'),(191,6,'plugin','m180423_175007_linkit_craft2','2019-09-23 18:19:05','2019-09-23 18:19:05','2019-09-23 18:19:05','cdca89ff-5d45-4e48-9705-8aa37af2d894'),(192,7,'plugin','Install','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','228e04b6-9954-4414-b488-5b2063ab5bda'),(193,7,'plugin','m180826_000000_propagate_nav_setting','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','e8ea802e-d2a4-43c0-9364-0312397412a2'),(194,7,'plugin','m180827_000000_propagate_nav_setting_additional','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','422299f1-c0fc-41e0-8155-698606ebda7f'),(195,7,'plugin','m181110_000000_add_elementSiteId','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','83ab8d59-c945-4b96-bbab-5f7ea27eae90'),(196,7,'plugin','m181123_000000_populate_elementSiteIds','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','cf52614b-1d5d-46c1-bacd-3868fcab05b7'),(197,7,'plugin','m190203_000000_add_instructions','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','fc0f7d55-b96a-45ab-899a-0e14c2657103'),(198,7,'plugin','m190209_000000_project_config','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','115c7d92-db73-498b-ad62-e61e1252c783'),(199,7,'plugin','m190223_000000_permissions','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','265d69fb-7a72-4824-9721-5551b0ccd728'),(200,7,'plugin','m190307_000000_update_field_content','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','a77534de-b516-4442-8b6d-775cb1f49a58'),(201,7,'plugin','m190310_000000_migrate_elementSiteId','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','f922cb88-1795-45cc-b2a7-5a6dab2ba808'),(202,7,'plugin','m190314_000000_soft_deletes','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','4a32c95d-ca01-41c0-aa41-1fac21a0ad33'),(203,7,'plugin','m190315_000000_project_config','2019-09-23 18:19:08','2019-09-23 18:19:08','2019-09-23 18:19:08','ab11b840-b768-4e9c-95ac-3057c6c83dde'),(204,9,'plugin','m180430_204710_remove_old_plugins','2019-09-23 18:19:20','2019-09-23 18:19:20','2019-09-23 18:19:20','c8d351c9-f59c-4730-8b6c-0f4adf8bb1ef'),(205,9,'plugin','Install','2019-09-23 18:19:20','2019-09-23 18:19:20','2019-09-23 18:19:20','8c1aff28-2926-4baf-baa9-86237f303c78'),(206,9,'plugin','m190225_003922_split_cleanup_html_settings','2019-09-23 18:19:20','2019-09-23 18:19:20','2019-09-23 18:19:20','750884c8-2726-4071-9af4-c90fe9f66485'),(207,10,'plugin','Install','2019-09-23 18:19:26','2019-09-23 18:19:26','2019-09-23 18:19:26','a719a133-6fa3-4f17-a2af-48322b4f44ca'),(208,10,'plugin','m170602_080218_redirect_1_0_1','2019-09-23 18:19:26','2019-09-23 18:19:26','2019-09-23 18:19:26','f444df47-5e4c-4113-b903-5e469eb6af55'),(209,10,'plugin','m170707_211256_count_fix','2019-09-23 18:19:26','2019-09-23 18:19:26','2019-09-23 18:19:26','c5ceccfe-1c5f-4b78-b717-b7376ffec6a6'),(210,10,'plugin','m171003_120604_createmultisiteurls','2019-09-23 18:19:26','2019-09-23 18:19:26','2019-09-23 18:19:26','35e70bdf-44e9-4de8-8057-70f1b876dc3f'),(211,10,'plugin','m180104_143118_c_redirects_catch_all_urls','2019-09-23 18:19:26','2019-09-23 18:19:26','2019-09-23 18:19:26','f4021bfb-14b3-4fff-9cc0-974ebb35590e'),(212,10,'plugin','m190426_121317_change_url_size_to_1000','2019-09-23 18:19:26','2019-09-23 18:19:26','2019-09-23 18:19:26','b63818e1-83d1-4e30-884b-8e9187a70d08'),(213,13,'plugin','Install','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','6db8cbb7-bc81-4995-b65a-8558d7b89f6f'),(214,13,'plugin','m180210_000000_migrate_content_tables','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','2ba3d5a0-6133-4b3a-85eb-0d9d94ba1768'),(215,13,'plugin','m180211_000000_type_columns','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','3621d9e3-d28b-4940-9a9d-e6b4ac487d6e'),(216,13,'plugin','m180219_000000_sites','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','608b4b87-d167-4311-a0b0-bca9a9a73bc0'),(217,13,'plugin','m180220_000000_fix_context','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','7c424575-8bb8-4425-b22a-ff9ba0b65d77'),(218,13,'plugin','m190117_000000_soft_deletes','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','8b49baa3-d297-44af-86c5-cf3eb3dca994'),(219,13,'plugin','m190117_000001_context_to_uids','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','bc40f191-e6e2-458c-828f-c38c16c677b0'),(220,13,'plugin','m190120_000000_fix_supertablecontent_tables','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','cb3314ca-1601-4eaf-a91f-51f79cdafefa'),(221,13,'plugin','m190131_000000_fix_supertable_missing_fields','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','88055e9b-000e-4d5c-9326-48ce7356a552'),(222,13,'plugin','m190227_100000_fix_project_config','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','a8c8467f-6127-4589-989b-ee2700b434dd'),(223,13,'plugin','m190511_100000_fix_project_config','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','952c5986-228f-4f9e-b3bc-aa08da165b94'),(224,13,'plugin','m190520_000000_fix_project_config','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','fc89b1bf-2b07-4dea-b6d5-725623b64fea'),(225,13,'plugin','m190714_000000_propagation_method','2019-09-23 18:19:48','2019-09-23 18:19:48','2019-09-23 18:19:48','ed67ba3b-75e9-45db-8700-ac743a42c674'),(226,7,'plugin','m191127_000000_fix_nav_handle','2019-12-12 11:48:24','2019-12-12 11:48:24','2019-12-12 11:48:24','da1784d4-b09d-4d50-9bcf-a7980d295977'),(227,13,'plugin','m191127_000000_fix_width','2019-12-12 11:48:24','2019-12-12 11:48:24','2019-12-12 11:48:24','45dc1b38-56d5-472f-9e84-96940d8aecb7'),(228,4,'plugin','m191214_093453_AddExtraPostUrlColumnToForm','2019-12-17 13:52:40','2019-12-17 13:52:40','2019-12-17 13:52:40','26516da0-71ef-4eb2-a9bd-6f1a18ae4acd');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `navigation_navs`
--

LOCK TABLES `navigation_navs` WRITE;
/*!40000 ALTER TABLE `navigation_navs` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `navigation_navs` VALUES (1,2,'Main','main','',1,0,'2019-09-25 14:06:18','2019-09-25 14:06:18',NULL,'ff6e8234-030e-45df-b853-038a1596d0d6'),(2,3,'Footer','footer','',2,0,'2019-09-25 14:06:23','2019-09-25 14:06:23',NULL,'29ad6ae7-f297-4df5-a060-2dd77f791715');
/*!40000 ALTER TABLE `navigation_navs` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `navigation_nodes`
--

LOCK TABLES `navigation_nodes` WRITE;
/*!40000 ALTER TABLE `navigation_nodes` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `navigation_nodes` VALUES (56,2,1,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:16:46','2019-12-16 11:57:28','5a0cb581-ea5b-4fa2-aa6a-d2e131967009'),(57,13,1,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:17:00','2019-10-17 10:25:33','05ab7d1e-4707-45fd-93ff-c3da6f15390e'),(58,27,1,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:17:00','2019-10-17 10:36:53','82ec9f9a-aff9-4708-9193-5796575172cc'),(59,21,1,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:17:01','2019-10-17 10:25:33','018edddf-dfed-4400-b30a-fac9ac3f40ee'),(60,24,1,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:17:01','2019-10-17 10:25:33','224602b7-ebf9-436b-b359-b8a2fd009800'),(61,16,1,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:17:01','2019-10-17 10:25:33','5a52b386-0f11-4bfd-bcb3-943920d3333c'),(62,30,2,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:17:59','2019-10-17 10:25:33','15940034-9e96-4cd9-aad0-24d031a8f739'),(63,33,2,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:18:00','2019-10-17 10:25:33','0b33105e-fd51-4b9d-a6c3-c291548789e9'),(64,36,2,NULL,NULL,'craft\\elements\\Entry',NULL,0,NULL,'2019-09-26 17:18:00','2019-10-17 10:25:34','e46c40e8-c0a5-4121-bdaa-5095481d66c2');
/*!40000 ALTER TABLE `navigation_nodes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `plugins` VALUES (1,'cheat-sheet','2.0.3','1.0.0','unknown',NULL,'2019-09-23 18:18:48','2019-09-23 18:18:48','2020-02-10 19:28:54','f483d48c-1123-4148-b72d-02ac8be3c914'),(2,'feed-me','4.2.0','2.1.2','unknown',NULL,'2019-09-23 18:18:50','2019-09-23 18:18:50','2020-02-10 19:28:54','5ad54b41-7190-4b1c-95d0-206594a7bd74'),(4,'freeform','3.5.9','3.0.5','invalid',NULL,'2019-09-23 18:18:57','2019-09-23 18:18:57','2020-02-10 19:28:54','d21087e9-f4e1-4bfe-9fbd-1ef4c96dc5fb'),(5,'imager','v2.3.1','2.0.0','unknown',NULL,'2019-09-23 18:19:01','2019-09-23 18:19:01','2020-02-10 19:28:54','d63f1e7b-c120-42ac-8e97-182e651e3f36'),(6,'linkit','1.1.11','1.0.8','invalid',NULL,'2019-09-23 18:19:05','2019-09-23 18:19:05','2020-02-10 19:28:54','c2cb40c4-3eef-4373-aa96-e681f7c31d10'),(7,'navigation','1.1.14.1','1.0.12','invalid',NULL,'2019-09-23 18:19:08','2019-09-23 18:19:08','2020-02-10 19:28:54','f73eb3f4-3a0c-4257-96a5-22ae8d77f108'),(8,'position-fieldtype','1.0.14','1.0.0','unknown',NULL,'2019-09-23 18:19:13','2019-09-23 18:19:13','2020-02-10 19:28:54','2d4d70ac-ea4b-4729-853e-e164c65350b6'),(9,'redactor','2.4.0','2.3.0','unknown',NULL,'2019-09-23 18:19:20','2019-09-23 18:19:20','2020-02-10 19:28:54','a989d9a8-9fc7-4003-a918-f5fa89828ca4'),(10,'redirect','1.0.23','1.0.5','unknown',NULL,'2019-09-23 18:19:26','2019-09-23 18:19:26','2020-02-10 19:28:54','d7f2f384-0aeb-42cf-a896-4ac90c108508'),(11,'wordsmith','3.1.1','0.0.0.0','unknown',NULL,'2019-09-23 18:19:32','2019-09-23 18:19:32','2020-02-10 19:28:54','210d64df-f0bc-4edb-a4b5-4e1383dafb74'),(12,'redactor-custom-styles','3.0.2','1.0.0','unknown',NULL,'2019-09-23 18:19:41','2019-09-23 18:19:41','2020-02-10 19:28:54','a9e82b8f-8bd9-488d-8420-af947448800e'),(13,'super-table','2.3.1','2.2.1','unknown',NULL,'2019-09-23 18:19:48','2019-09-23 18:19:48','2020-02-10 19:28:54','9ee8af7e-7229-4c9b-81ad-e9b386de779d'),(15,'field-manager','2.1.1','1.0.0','unknown',NULL,'2019-09-23 18:45:19','2019-09-23 18:45:19','2020-02-10 19:28:54','420bf00d-f040-4301-a8f6-79526e7ebb67');
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `relations`
--

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `relations` VALUES (4,41,82,NULL,68,1,'2019-12-12 19:01:06','2019-12-12 19:01:06','6e55da9c-e56e-47ae-9bbe-1f54e09d8848'),(5,41,82,NULL,71,2,'2019-12-12 19:01:06','2019-12-12 19:01:06','6b19945e-f183-48f2-9a7c-f9c0dc1cb19c'),(6,41,82,NULL,74,3,'2019-12-12 19:01:06','2019-12-12 19:01:06','461465cd-e21f-4619-ae52-feb0db68a9f7'),(10,41,85,NULL,68,1,'2019-12-12 19:01:17','2019-12-12 19:01:17','4c56ecf1-3ba5-48aa-b7c9-9f4df2c746ed'),(11,41,85,NULL,71,2,'2019-12-12 19:01:17','2019-12-12 19:01:17','575b33eb-9f5a-4531-acf6-17c24765c854'),(12,41,85,NULL,74,3,'2019-12-12 19:01:17','2019-12-12 19:01:17','2c6eaa8d-9bf9-4a0a-a8db-f88acb497567'),(16,41,89,NULL,68,1,'2019-12-12 19:01:48','2019-12-12 19:01:48','0c0ee704-0eda-4f82-883f-570b0b133747'),(17,41,89,NULL,71,2,'2019-12-12 19:01:48','2019-12-12 19:01:48','da583c0e-e5d3-4e71-a64b-3d04a9eaa8dd'),(18,41,89,NULL,74,3,'2019-12-12 19:01:48','2019-12-12 19:01:48','a28a9d26-4a77-4dd8-92fb-928cfe794858'),(22,41,96,NULL,68,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','99c98bb4-c0c4-440d-b2f0-625529018a6d'),(23,41,96,NULL,71,2,'2019-12-12 19:03:40','2019-12-12 19:03:40','70d97201-3a13-4420-be8c-0c2dcab070b7'),(24,41,96,NULL,74,3,'2019-12-12 19:03:40','2019-12-12 19:03:40','dae969c7-b1a1-46d1-9a84-63612472ee33'),(31,41,107,NULL,68,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','d7e743ea-02ba-4ed5-b572-ecc61306a4f6'),(32,41,107,NULL,71,2,'2019-12-12 19:05:20','2019-12-12 19:05:20','343c1214-143a-4e8b-a31b-57ffe207d0c0'),(33,41,107,NULL,74,3,'2019-12-12 19:05:20','2019-12-12 19:05:20','e22f9ef1-8a5f-4579-aade-3903cc3fb6a6'),(34,48,113,NULL,68,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','372d139a-28e7-43ca-9f7a-cbf3fdc3c8df'),(35,48,114,NULL,71,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','1e19af5b-c2ac-4662-9962-5498eea2d88e'),(36,48,115,NULL,74,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','efb0305f-2875-424e-945c-fb2c53490b43'),(46,41,119,NULL,68,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','1fd9bc61-c653-4fad-9f00-e1d1fc872def'),(47,41,119,NULL,71,2,'2019-12-12 19:05:53','2019-12-12 19:05:53','b0d9322c-422f-4c94-8e13-8876cfd32a3b'),(48,41,119,NULL,74,3,'2019-12-12 19:05:53','2019-12-12 19:05:53','281a6e82-b3a3-4a56-be09-422b3fbb047f'),(49,48,125,NULL,68,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','10467b86-8e92-4398-9b1e-431c77ccd5ce'),(50,48,126,NULL,71,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','c2533725-5521-4c5b-8b31-5289fe78027a'),(51,48,127,NULL,74,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','b0ec3492-4d5f-4b4f-993f-363388689525'),(52,58,128,NULL,68,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','4af81dbd-9439-43e5-9a82-b0b26d680069'),(53,58,128,NULL,71,2,'2019-12-12 19:05:53','2019-12-12 19:05:53','c0f9a433-5f3c-4887-b72e-858f699da812'),(54,58,128,NULL,74,3,'2019-12-12 19:05:53','2019-12-12 19:05:53','94883b55-eee2-439d-a53c-18a602eda2a1'),(55,41,79,NULL,68,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','d8c73a34-681d-4a9e-a7d9-aafb367ec754'),(56,41,79,NULL,71,2,'2019-12-16 11:57:28','2019-12-16 11:57:28','3d416cb9-b5c6-4a59-8325-0cb267995310'),(57,41,79,NULL,74,3,'2019-12-16 11:57:28','2019-12-16 11:57:28','909e03af-6caa-45c8-a38a-161d452e06d2'),(58,48,102,NULL,68,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','c8babfce-b855-496d-919c-9250fe915a43'),(59,48,103,NULL,71,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','62ce9bd3-6b3e-429b-995a-7a9482de2203'),(60,48,104,NULL,74,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','fcd6ea5c-7381-4361-8a4a-fbb1b640c710'),(61,58,116,NULL,68,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','1afc919d-e2fa-4c42-9ff1-dd17fb3be4d0'),(62,58,116,NULL,71,2,'2019-12-16 11:57:28','2019-12-16 11:57:28','45191d4e-9ae9-43ce-85dd-8eaa86282dd2'),(63,58,116,NULL,74,3,'2019-12-16 11:57:28','2019-12-16 11:57:28','5d3f92ab-c2eb-483b-aea9-554b9d4b4f07'),(64,41,131,NULL,68,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','a8c0e668-d1c9-4033-8997-cd125122eb22'),(65,41,131,NULL,71,2,'2019-12-16 11:57:28','2019-12-16 11:57:28','6eea5359-063f-49a3-a1ec-e2ceda8bd4e4'),(66,41,131,NULL,74,3,'2019-12-16 11:57:28','2019-12-16 11:57:28','8a9a81c6-d68c-4d20-b8c1-377c17a9ad76'),(67,48,137,NULL,68,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','a158e4b0-1c63-4ac3-ab6a-723fbe9ba559'),(68,48,138,NULL,71,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','75829e6b-83a4-460f-9a19-4d1df807845b'),(69,48,139,NULL,74,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','71fe4078-308a-4f53-abbf-e66168c9dce3'),(70,58,140,NULL,68,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','6d4e400f-9eee-4bd6-b79b-730251af14bb'),(71,58,140,NULL,71,2,'2019-12-16 11:57:28','2019-12-16 11:57:28','2a57d887-5a17-48d1-8710-eb92164604ac'),(72,58,140,NULL,74,3,'2019-12-16 11:57:28','2019-12-16 11:57:28','bc5d4f28-2929-4e81-b05c-e81f996b5d79');
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `resourcepaths`
--

LOCK TABLES `resourcepaths` WRITE;
/*!40000 ALTER TABLE `resourcepaths` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `resourcepaths` VALUES ('1003bf9','@lib/timepicker'),('11040ed2','@craft/web/assets/utilities/dist'),('11e1dc99','@lib/jquery-touch-events'),('12c127cf','@craft/web/assets/updates/dist'),('1645e05b','@lib/picturefill'),('167899b0','@craft/web/assets/recententries/dist'),('18540e6a','@lib/fileupload'),('18e0c00a','@lib/d3'),('1944800c','@craft/web/assets/updates/dist'),('1a5c7d90','@verbb/navigation/resources/dist'),('1bd95b27','@fruitstudios/linkit/assetbundles/field/build'),('1c557181','@storage/rebrand/logo'),('1d1980fc','@craft/web/assets/fields/dist'),('1ddc4911','@lib/fabric'),('1fbbdfef','@craft/web/assets/updater/dist'),('2095df5a','@craft/web/assets/dashboard/dist'),('23324882','@verbb/fieldmanager/resources/dist'),('25422b07','@bower/jquery/dist'),('28c2dbde','@craft/web/assets/updateswidget/dist'),('28e2ec7d','@lib/fabric'),('28ea7392','@lib/jquery.payment'),('29d703ba','@craft/web/assets/plugins/dist'),('2b107899','@craft/web/assets/dashboard/dist'),('2b5dd78c','@lib/garnishjs'),('2d6aab06','@lib/fileupload'),('2f0e5fa8','@craft/web/assets/editsection/dist'),('30a09cb3','@lib/prismjs'),('3226963a','@lib/jquery-touch-events'),('32bc024f','@craft/web/assets/cp/dist'),('346a383d','@craft/web/assets/clearcaches/dist'),('3473f845','@craft/web/assets/feed/dist'),('34932706','@lib/selectize'),('34cddc7a','@lib/velocity'),('34da9bf5','@craft/web/assets/matrixsettings/dist'),('350b3289','@craft/web/assets/tablesettings/dist'),('3582aaf8','@lib/picturefill'),('35fa4e86','@craft/web/assets/matrix/dist'),('36072cc','@craft/web/assets/login/dist'),('3769b20b','@lib/jquery-ui'),('3b278aa9','@lib/d3'),('3c6a051f','@vendor/craftcms/redactor/lib/redactor-plugins/fullscreen'),('3d219aff','@craft/web/assets/utilities/dist'),('3da43843','@lib/garnishjs'),('3e1b03b2','@lib/fabric'),('3eff638c','@lib/xregexp'),('41a83300','@lib/fileupload'),('423bcddf','@lib/selectize'),('431739ca','@craft/web/assets/recententries/dist'),('47df7526','@lib/element-resize-detector'),('48578955','@lib/xregexp'),('48762086','@craft/web/assets/fields/dist'),('48b3e96b','@lib/fabric'),('4ad47f95','@craft/web/assets/updater/dist'),('4d30de8e','@craft/web/assets/dashboard/dist'),('50651f2b','@lib'),('5188ef89','@craft/web/assets/updateswidget/dist'),('52535e52','@craft/web/assets/feed/dist'),('52ed0759','@craft/web/assets/cp/dist'),('52fa3de2','@craft/web/assets/matrixsettings/dist'),('546f9b83','@lib/timepicker'),('54a80465','@vendor/craftcms/redactor/lib/redactor-plugins/fontcolor'),('54c22210','@lib/selectize'),('5637ddfd','@craft/web/assets/edituser/dist'),('57e512af','@lib/d3'),('594032fe','@lib/picturefill'),('59d6f991','@craft/web/assets/feed/dist'),('59e39df','@lib/prismjs'),('5df53d55','@lib/garnishjs'),('5eae669a','@lib/xregexp'),('5eba5a2c','@craft/web/assets/routes/dist'),('5ee40e3c','@lib/jquery-touch-events'),('5f954005','@craft/feedme/web/assets/feedme/dist'),('606492f3','@craft/web/assets/tablesettings/dist'),('6095eefc','@craft/web/assets/matrix/dist'),('60bae821','@Solspace/Freeform/Resources'),('60ed0a82','@lib/picturefill'),('61513eef','@lib/timepicker'),('61cc6865','@craft/web/assets/editcategory/dist'),('61fc877c','@lib/selectize'),('64580529','@lib/garnishjs'),('6540d4db','@craft/web/assets/login/dist'),('65cf3cc9','@lib/prismjs'),('65e78646','@lib/jquery.payment'),('66692bb7','@carlcs/redactorcustomstyles/assets/redactorplugin/dist'),('66c3c4d5','@craft/web/assets/craftsupport/dist'),('66f1ebb8','@craft/web/assets/updater/dist'),('67493640','@lib/jquery-touch-events'),('67d3a235','@craft/web/assets/cp/dist'),('684e3a85','@craft/web/assets/utilities/dist'),('684fded3','@bower/jquery/dist'),('6b90c3f6','@lib/xregexp'),('6d466316','@craft/web/assets/craftsupport/dist'),('6d961c8e','@craft/web/assets/editentry/dist'),('6e482ad3','@lib/d3'),('6ec57318','@craft/web/assets/login/dist'),('6f32ade7','@craft/web/assets/recententries/dist'),('711ed117','@lib/fabric'),('7183356','@lib/jquery-touch-events'),('74e181d8','@craft/web/assets/updates/dist'),('7525bf0','@lib/xregexp'),('78050b7c','@lib/fileupload'),('781bbbe2','@fruitstudios/linkit/assetbundles/fieldsettings/build'),('79c029ae','@lib/velocity'),('7a6447df','@lib/jquery-ui'),('7b1c68e3','@storage/rebrand/icon'),('7dad7ba4','@craft/web/assets/updateswidget/dist'),('801b22e8','@lib/axios'),('81b6f469','@lib/fabric'),('81bab86f','@vendor/craftcms/redactor/lib/redactor'),('81be6b86','@lib/jquery.payment'),('81e3d387','@craft/web/assets/utilities/dist'),('824cdc32','@craft/web/assets/tablesettings/dist'),('833e1d91','@rias/positionfieldtype/assetbundles/positionfieldtype/dist'),('839d754e','@craft/web/assets/matrixsettings/dist'),('8508d32f','@lib/timepicker'),('87689a1a','@craft/web/assets/login/dist'),('89a35d59','@craft/web/assets/updates/dist'),('89a9d2f','@lib/garnishjs'),('89cff14e','@craft/web/assets/editentry/dist'),('8a667444','@craft/web/assets/utilities/dist'),('8acc62a1','@lib/jquery-ui'),('8b602ba1','@lib/velocity'),('8c163313','@bower/jquery/dist'),('8d02176b','@app/web/assets/plugins/dist'),('8f6e2dd7','@craft/web/assets/craftsupport/dist'),('92707166','@craft/web/assets/recententries/dist'),('94f02057','@lib/garnishjs'),('96b83d8a','@lib/element-resize-detector'),('9731aefb','@verbb/supertable/resources/dist'),('97478449','@lib/jquery.payment'),('9aefdcdc','@bower/jquery/dist'),('9b409e20','@verbb/fieldmanager/resources/dist'),('9bb33739','@craft/web/assets/updater/dist'),('9c579622','@craft/web/assets/dashboard/dist'),('9d99c46e','@lib/velocity'),('a38698e6','@lib/element-resize-detector'),('a586c974','@craft/web/assets/updates/dist'),('a5e56753','@craft/web/assets/generalsettings/dist'),('a74d37d1','@lib/d3'),('a8a76102','@lib/velocity'),('a969334e','@craft/web/assets/dashboard/dist'),('a9e81780','@lib/picturefill'),('aa5e6e47','@craft/web/assets/fields/dist'),('acca3308','@craft/web/assets/updateswidget/dist'),('ad280f2','@lib/element-resize-detector'),('ae4c2b42','@lib/jquery-touch-events'),('b07b1093','@craft/web/assets/feed/dist'),('b09bcfd0','@lib/selectize'),('b100167e','@lib/fileupload'),('b3615add','@lib/jquery-ui'),('b3ffff9','@vendor/craftcms/redactor/lib/redactor-plugins/video'),('b480ceea','@lib/jquery.payment'),('b57f7729','@lib/element-resize-detector'),('b65b9d5e','@craft/feedme/web/assets/feedme/dist'),('b66c501','@craft/web/assets/craftsupport/dist'),('b6b4ea99','@craft/web/assets/cp/dist'),('b928967f','@bower/jquery/dist'),('b9b61a94','@lib/axios'),('baf78b5a','@lib/xregexp'),('bc0f94','@lib/picturefill'),('bcf15422','@craft/web/assets/editentry/dist'),('bfcfdae4','@craft/web/assets/pluginstore/dist'),('c132199b','@lib/vue'),('c16f32dc','@craft/web/assets/updateswidget/dist'),('c38cf459','@craft/web/assets/cp/dist'),('c3d79df0','@lib/element-resize-detector'),('c465f904','@lib/velocity'),('c4ebea','@craft/web/assets/matrix/dist'),('c5c9b004','@lib/jquery-ui'),('c80526fb','@lib/timepicker'),('c9383658','@craft/web/assets/dashboard/dist'),('caea951f','@craft/web/assets/updateswidget/dist'),('cc41434','@app/web/assets/cp/dist'),('cc7ec850','@craft/web/assets/fields/dist'),('ccbb01bd','@lib/fabric'),('cf1ef04d','@lib/axios'),('d02a1585','@craft/web/assets/feed/dist'),('d0944c8e','@craft/web/assets/cp/dist'),('d2073a60','@craft/web/assets/login/dist'),('d3305fcb','@lib/jquery-ui'),('d3e1f7a','@lib/selectize'),('d5ea0e79','@bower/jquery/dist'),('d6f2d534','@craft/web/assets/matrixsettings/dist'),('d84256ec','@lib/jquery.payment'),('d9fdd583','@lib/garnishjs'),('da018dad','@craft/web/assets/craftsupport/dist'),('da4e9532','@vendor/craftcms/redactor/lib/redactor-plugins/fontsize'),('db11eb4d','@craft/web/assets/cp/dist'),('dc14dba7','@verbb/supertable/resources/dist'),('e192fc5','@lib/d3'),('e1c7d41f','@lib/prismjs'),('e1ef6e90','@lib/jquery.payment'),('e341de96','@lib/jquery-touch-events'),('e46c7a25','@craft/web/assets/tablesettings/dist'),('e49d062a','@craft/web/assets/matrix/dist'),('e4e5e254','@lib/picturefill'),('e50d7091','@craft/web/assets/clearcaches/dist'),('e514b0e9','@craft/web/assets/feed/dist'),('e547b951','@carlcs/redactorcustomstyles/assets/redactorplugin/_redactorplugin'),('e60efaa7','@lib/jquery-ui'),('e7399f0c','@craft/web/assets/login/dist'),('e99ef458','@craft/web/assets/editentry/dist'),('ea40c205','@lib/d3'),('eade1a5','@lib/fileupload'),('ec46d253','@craft/web/assets/utilities/dist'),('ec473605','@bower/jquery/dist'),('ecd9baee','@lib/axios'),('ef3f28c1','@craft/web/assets/craftsupport/dist'),('f0e9690e','@craft/web/assets/updates/dist'),('f29e4070','@craft/web/assets/sites/dist'),('f450d771','@craft/web/assets/recententries/dist'),('f61636ed','@craft/web/assets/updater/dist'),('f7fa7e8e','@lib/xregexp'),('fa7aa58c','@lib/element-resize-detector'),('fc0de3aa','@lib/fileupload'),('fce025ef','@craft/redactor/assets/field/dist'),('fd93912e','@craft/web/assets/updater/dist'),('fd963a04','@lib/selectize'),('fdc8c178','@lib/velocity'),('fe691704','@craft/web/assets/editsection/dist'),('ffd570b2','@craft/web/assets/recententries/dist');
/*!40000 ALTER TABLE `resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `revisions`
--

LOCK TABLES `revisions` WRITE;
/*!40000 ALTER TABLE `revisions` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `revisions` VALUES (1,2,1,1,NULL),(2,2,1,2,NULL),(3,2,1,3,NULL),(4,13,1,1,NULL),(5,16,1,1,NULL),(6,16,1,2,NULL),(7,13,1,2,NULL),(8,21,1,1,NULL),(9,24,1,1,NULL),(10,27,1,1,NULL),(11,30,1,1,NULL),(12,33,1,1,NULL),(13,36,1,1,NULL),(14,42,1,1,NULL),(15,45,1,1,NULL),(16,48,1,1,NULL),(17,51,1,1,NULL),(18,54,1,1,NULL),(19,27,1,2,NULL),(20,68,1,1,NULL),(21,71,1,1,NULL),(22,74,1,1,NULL),(23,2,1,4,NULL),(24,2,1,5,NULL),(25,2,1,6,NULL),(26,2,1,7,NULL),(27,2,1,8,NULL),(28,2,1,9,NULL),(29,2,1,10,NULL),(30,2,1,11,NULL),(31,2,1,12,NULL);
/*!40000 ALTER TABLE `revisions` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `searchindex`
--

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `searchindex` VALUES (1,'username',0,1,' webmaster '),(1,'firstname',0,1,''),(1,'lastname',0,1,''),(1,'fullname',0,1,''),(1,'email',0,1,' sebastian jordan gmail com '),(1,'slug',0,1,''),(5,'slug',0,1,''),(5,'field',3,1,''),(5,'field',2,1,''),(6,'slug',0,1,''),(6,'field',8,1,''),(6,'field',6,1,''),(6,'field',7,1,''),(7,'slug',0,1,''),(7,'field',4,1,''),(7,'field',5,1,''),(2,'slug',0,1,' home '),(2,'title',0,1,' home '),(9,'slug',0,1,''),(10,'slug',0,1,''),(10,'field',15,1,''),(11,'filename',0,1,' icon abct png '),(11,'extension',0,1,' png '),(11,'kind',0,1,' image '),(11,'slug',0,1,''),(11,'title',0,1,' icon abct '),(13,'slug',0,1,' about us '),(13,'title',0,1,' about us '),(13,'field',9,1,''),(13,'field',3,1,''),(13,'field',2,1,''),(13,'field',1,1,''),(13,'field',8,1,''),(13,'field',6,1,''),(13,'field',7,1,''),(16,'slug',0,1,' our priorities '),(16,'title',0,1,' our priorities '),(16,'field',9,1,''),(16,'field',3,1,''),(16,'field',2,1,''),(16,'field',1,1,''),(16,'field',8,1,''),(16,'field',6,1,''),(16,'field',7,1,''),(21,'slug',0,1,' how we fund '),(21,'title',0,1,' how we fund '),(21,'field',9,1,''),(21,'field',3,1,''),(21,'field',2,1,''),(21,'field',1,1,''),(21,'field',8,1,''),(21,'field',6,1,''),(21,'field',7,1,''),(24,'slug',0,1,' grantees '),(24,'title',0,1,' grantees '),(24,'field',9,1,''),(24,'field',3,1,''),(24,'field',2,1,''),(24,'field',1,1,''),(24,'field',8,1,''),(24,'field',6,1,''),(24,'field',7,1,''),(27,'slug',0,1,' apply '),(27,'title',0,1,' apply '),(27,'field',9,1,''),(27,'field',3,1,''),(27,'field',2,1,''),(27,'field',1,1,''),(27,'field',8,1,''),(27,'field',6,1,''),(27,'field',7,1,''),(30,'slug',0,1,' help advice '),(30,'title',0,1,' help advice '),(30,'field',9,1,''),(30,'field',3,1,''),(30,'field',2,1,''),(30,'field',1,1,''),(30,'field',8,1,''),(30,'field',6,1,''),(30,'field',7,1,''),(33,'slug',0,1,' faqs '),(33,'title',0,1,' faqs '),(33,'field',9,1,''),(33,'field',3,1,''),(33,'field',2,1,''),(33,'field',1,1,''),(33,'field',8,1,''),(33,'field',6,1,''),(33,'field',7,1,''),(36,'slug',0,1,' contact '),(36,'title',0,1,' contact '),(36,'field',9,1,''),(36,'field',3,1,''),(36,'field',2,1,''),(36,'field',1,1,''),(36,'field',8,1,''),(36,'field',6,1,''),(36,'field',7,1,''),(38,'slug',0,1,' migrants refugees and asylum seekers '),(38,'title',0,1,' migrants refugees and asylum seekers '),(39,'slug',0,1,' criminal justice and penal reform '),(39,'title',0,1,' criminal justice and penal reform '),(40,'slug',0,1,' access to justice '),(40,'title',0,1,' access to justice '),(42,'slug',0,1,' trustees '),(42,'title',0,1,' trustees '),(42,'field',9,1,''),(42,'field',3,1,''),(42,'field',2,1,''),(42,'field',1,1,''),(42,'field',8,1,''),(42,'field',6,1,''),(42,'field',7,1,''),(45,'slug',0,1,' staff '),(45,'title',0,1,' staff '),(45,'field',9,1,''),(45,'field',3,1,''),(45,'field',2,1,''),(45,'field',1,1,''),(45,'field',8,1,''),(45,'field',6,1,''),(45,'field',7,1,''),(48,'slug',0,1,' grants awarded '),(48,'title',0,1,' grants awarded '),(48,'field',9,1,''),(48,'field',3,1,''),(48,'field',2,1,''),(48,'field',1,1,''),(48,'field',8,1,''),(48,'field',6,1,''),(48,'field',7,1,''),(51,'slug',0,1,' information for grantees '),(51,'title',0,1,' information for grantees '),(51,'field',9,1,''),(51,'field',3,1,''),(51,'field',2,1,''),(51,'field',1,1,''),(51,'field',8,1,''),(51,'field',6,1,''),(51,'field',7,1,''),(54,'slug',0,1,' bonavero institute '),(54,'title',0,1,' bonavero institute '),(54,'field',9,1,''),(54,'field',3,1,''),(54,'field',2,1,''),(54,'field',1,1,''),(54,'field',8,1,''),(54,'field',6,1,''),(54,'field',7,1,''),(56,'slug',0,1,' 1 '),(57,'slug',0,1,' 1 '),(57,'title',0,1,' about us '),(58,'slug',0,1,' 1 '),(58,'title',0,1,' apply '),(59,'slug',0,1,' 1 '),(59,'title',0,1,' how we fund '),(60,'slug',0,1,' 1 '),(60,'title',0,1,' grantees '),(61,'slug',0,1,' 1 '),(61,'title',0,1,' our priorities '),(62,'slug',0,1,' 1 '),(62,'title',0,1,' help advice '),(63,'slug',0,1,' 1 '),(63,'title',0,1,' faqs '),(64,'slug',0,1,' 1 '),(64,'title',0,1,' contact '),(65,'slug',0,1,''),(68,'slug',0,1,' migrants refugees and asylum seekers '),(68,'title',0,1,' migrants refugees and asylum seekers '),(71,'slug',0,1,' criminal justice and penal reform '),(71,'title',0,1,' criminal justice and penal reform '),(74,'title',0,1,' human rights particularly access to justice '),(56,'title',0,1,' home '),(2,'field',36,1,' read more we promote human dignity and defend human rights for marginalised and excluded people promoting human dignity '),(78,'slug',0,1,''),(78,'field',39,1,' promoting human dignity '),(78,'field',37,1,' we promote human dignity and defend human rights for marginalised and excluded people '),(79,'slug',0,1,''),(79,'field',40,1,' our three priority areas '),(79,'field',41,1,' migrants refugees and asylum seekers criminal justice and penal reform human rights particularly access to justice '),(78,'field',38,1,' read more '),(86,'slug',0,1,''),(86,'field',59,1,' our funding programmes '),(91,'slug',0,1,''),(91,'field',63,1,' the open programme '),(91,'field',62,1,' by application '),(91,'field',61,1,' lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua '),(92,'slug',0,1,''),(92,'field',63,1,' special initiatives '),(92,'field',62,1,' by invitation '),(92,'field',61,1,' lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua '),(93,'slug',0,1,''),(93,'field',63,1,' the anchor programme '),(93,'field',62,1,' by invitation '),(93,'field',61,1,' lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua '),(86,'field',60,1,' lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua the open programme by application lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua special initiatives by invitation lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua the anchor programme by invitation '),(86,'field',64,1,' we don t reach people in need directly instead we pursue our purpose by funding partner organisations we have three grant making programmes '),(86,'field',65,1,' more information '),(102,'slug',0,1,''),(102,'field',48,1,' migrants refugees and asylum seekers '),(102,'field',52,1,' 746 225 '),(102,'field',47,1,' 696 225 '),(102,'field',49,1,' 50 '),(102,'field',51,1,' 80 '),(103,'slug',0,1,''),(103,'field',48,1,' criminal justice and penal reform '),(103,'field',52,1,' 300 000 '),(103,'field',47,1,' 300 000 '),(103,'field',49,1,' 22 '),(103,'field',51,1,' 80 '),(104,'slug',0,1,''),(104,'field',48,1,' human rights particularly access to justice '),(104,'field',52,1,' 617 000 '),(104,'field',47,1,' 447 000 '),(104,'field',49,1,' 27 '),(104,'field',51,1,' 80 '),(101,'slug',0,1,''),(101,'field',54,1,' grants awarded in 2018 19 '),(101,'field',66,1,''),(101,'field',44,1,' 0 80 migrants refugees and asylum seekers 50 696 225 50 000 746 225 150 0 80 criminal justice and penal reform 22 300 000 0 300 000 110 30 000 80 human rights particularly access to justice 27 447 000 140 000 617 000 110 '),(101,'field',42,1,' funding total for 2018 19 '),(2,'field',3,1,''),(102,'field',50,1,' 150 '),(102,'field',46,1,' 50 000 '),(103,'field',50,1,' 110 '),(103,'field',46,1,' 0 '),(104,'field',50,1,' 110 '),(104,'field',46,1,' 140 000 '),(101,'field',43,1,' 1 663 225 '),(101,'field',55,1,' what we plan to give in 2020 '),(116,'slug',0,1,''),(116,'field',56,1,' find out more about our priority areas below or check your eligibility in our apply section '),(116,'field',58,1,' migrants refugees and asylum seekers criminal justice and penal reform human rights particularly access to justice '),(2,'field',9,1,' migrants refugees and asylum seekers criminal justice and penal reform human rights particularly access to justice our three priority areas more information we don t reach people in need directly instead we pursue our purpose by funding partner organisations we have three grant making programmes our funding programmes lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua the open programme by application lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua special initiatives by invitation lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua the anchor programme by invitation 1 663 225 funding total for 2018 19 0 80 migrants refugees and asylum seekers 50 696 225 50 000 746 225 150 0 80 criminal justice and penal reform 22 300 000 0 300 000 110 30 000 80 human rights particularly access to justice 27 447 000 140 000 617 000 110 grants awarded in 2018 19 what we plan to give in 2020 1 750 000 apply find out more about our priority areas below or check your eligibility in our apply section migrants refugees and asylum seekers criminal justice and penal reform human rights particularly access to justice '),(2,'field',2,1,''),(2,'field',8,1,''),(9,'field',12,1,''),(102,'field',45,1,' 0 '),(103,'field',45,1,' 0 '),(104,'field',45,1,' 30 000 '),(101,'field',53,1,' 1 750 000 '),(116,'field',57,1,' apply '),(2,'field',6,1,''),(2,'field',7,1,''),(74,'slug',0,1,' human rights particularly access to justice '),(68,'field',67,1,''),(68,'field',32,1,''),(71,'field',67,1,''),(71,'field',32,1,''),(74,'field',67,1,''),(74,'field',32,1,'');
/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `sections` VALUES (1,1,'Pages','pages','structure',1,'all',NULL,'2019-09-23 18:29:58','2019-10-17 10:25:58',NULL,'ee328c08-2719-4360-a27f-4d3fc4ca60ab'),(2,NULL,'Home','home','single',1,'all',NULL,'2019-09-23 18:30:14','2019-12-12 18:56:31',NULL,'a3d293bf-d009-4c39-b43f-8962c13c2008'),(3,NULL,'Grants Awarded','grantsAwarded','channel',1,'all',NULL,'2019-12-12 17:38:35','2019-12-12 18:32:13',NULL,'40245d37-4cf6-4080-b358-9aa027534703'),(4,NULL,'Staff','staff','channel',1,'all',NULL,'2019-12-12 17:38:56','2019-12-12 17:56:20',NULL,'5f9ddfa5-840d-422b-9d15-69d77baa68fd'),(5,NULL,'Trustees','trustees','channel',1,'all',NULL,'2019-12-12 17:39:09','2019-12-12 18:15:03',NULL,'3c91ce22-6de2-4079-9273-1472dd23dc27'),(6,NULL,'Grants','grants','channel',1,'all',NULL,'2019-12-12 17:40:10','2019-12-17 17:25:47',NULL,'e74dd188-369d-4999-be24-4a6a51e9ba53');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `sections_sites`
--

LOCK TABLES `sections_sites` WRITE;
/*!40000 ALTER TABLE `sections_sites` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `sections_sites` VALUES (1,1,1,1,'{slug}','pages/_entry',1,'2019-09-23 18:29:58','2019-10-17 10:25:58','92a719fd-4d37-48a1-a1ce-ac93ca511688'),(2,2,1,1,'__home__','',1,'2019-09-23 18:30:14','2019-12-12 18:56:31','eb1bee6b-57ef-4d14-8f71-77a2be3121cf'),(3,3,1,1,'grants-awarded/{slug}','',1,'2019-12-12 17:38:35','2019-12-12 18:32:13','e1edc81e-6057-467c-acfe-4b9f72f3f554'),(4,4,1,0,NULL,NULL,1,'2019-12-12 17:38:56','2019-12-12 17:56:20','7eff632e-8f74-43e9-b216-2ec2a01271bb'),(5,5,1,1,'trustees/{slug}','',1,'2019-12-12 17:39:09','2019-12-12 18:15:03','397a772b-ba53-4d14-abe2-5d02951e57d7'),(6,6,1,1,'grants/{slug}','',1,'2019-12-12 17:40:10','2019-12-17 17:25:47','98005873-b0d9-417e-82fd-fc62313a9798');
/*!40000 ALTER TABLE `sections_sites` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `sequences`
--

LOCK TABLES `sequences` WRITE;
/*!40000 ALTER TABLE `sequences` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `sequences` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `shunnedmessages`
--

LOCK TABLES `shunnedmessages` WRITE;
/*!40000 ALTER TABLE `shunnedmessages` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `shunnedmessages` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `sitegroups`
--

LOCK TABLES `sitegroups` WRITE;
/*!40000 ALTER TABLE `sitegroups` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `sitegroups` VALUES (1,'AB Chairty Trust','2019-09-23 17:41:02','2019-09-25 14:20:05',NULL,'08a3e4fc-619a-4d83-949b-9e15ef770283');
/*!40000 ALTER TABLE `sitegroups` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `sites` VALUES (1,1,1,'AB Charity Trust','default','en-GB',1,'$DEFAULT_SITE_URL',1,'2019-09-23 17:41:02','2019-09-25 14:19:54',NULL,'07417ec7-13a1-46da-9b82-bb6dde53d07d');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `stc_3_ctas`
--

LOCK TABLES `stc_3_ctas` WRITE;
/*!40000 ALTER TABLE `stc_3_ctas` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `stc_3_ctas` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `stc_7_grants`
--

LOCK TABLES `stc_7_grants` WRITE;
/*!40000 ALTER TABLE `stc_7_grants` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `stc_7_grants` VALUES (1,102,1,'2019-12-12 19:05:20','2019-12-16 11:57:28','9216e163-cf19-43c7-a54c-1111d13224da','£0','£50,000','£696,225','50','150','80','£746,225'),(2,103,1,'2019-12-12 19:05:20','2019-12-16 11:57:28','210756ab-b57b-4604-8c04-5af14eec5c7c','£0','£0','£300,000','22','110','80','£300,000'),(3,104,1,'2019-12-12 19:05:20','2019-12-16 11:57:28','e667d04e-4e97-4bfd-9846-78e26828b318','£30,000','£140,000','£447,000','27','110','80','£617,000'),(4,113,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','3bbb81ba-8a6a-4a4d-8a05-ce28bd5c45ed','£0','£50,000','£696,225','50','150','80','£746,225'),(5,114,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','2f4371a2-8ee2-4748-a9d7-83721b99759b','£0','£0','£300,000','22','110','80','£300,000'),(6,115,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','08cb0628-aa53-4881-b47c-e4c6719562b5','£30,000','£140,000','£447,000','27','110','80','£617,000'),(7,125,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','5772ebaa-7d34-41ca-a6f2-22f45fedbe96','£0','£50,000','£696,225','50','150','80','£746,225'),(8,126,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','71034d40-e5a2-487e-9854-bd4052c52265','£0','£0','£300,000','22','110','80','£300,000'),(9,127,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','434a0584-d548-4587-a161-4904ed059ca3','£30,000','£140,000','£447,000','27','110','80','£617,000'),(10,137,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','5e5f90de-3771-4c78-9e73-e9520eeefd57','£0','£50,000','£696,225','50','150','80','£746,225'),(11,138,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','8561a995-c207-4e61-84ea-6c7a0fa6abb8','£0','£0','£300,000','22','110','80','£300,000'),(12,139,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','b1047664-ddd0-41c3-b86e-95c7dad1be12','£30,000','£140,000','£447,000','27','110','80','£617,000');
/*!40000 ALTER TABLE `stc_7_grants` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `stc_9_programmes`
--

LOCK TABLES `stc_9_programmes` WRITE;
/*!40000 ALTER TABLE `stc_9_programmes` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `stc_9_programmes` VALUES (1,91,1,'2019-12-12 19:03:40','2019-12-16 11:57:28','9a59f212-58a5-4dea-9fb9-b0eb572f8cb2','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by application)','The Open Programme'),(2,92,1,'2019-12-12 19:03:40','2019-12-16 11:57:28','ac41ffea-9431-4956-adb2-8f03c6b91c1f','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','Special Initiatives'),(3,93,1,'2019-12-12 19:03:40','2019-12-16 11:57:28','d1b50a47-d319-4de7-a6f3-3ec5be758b1a','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','The Anchor Programme'),(4,98,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','2ac886e7-fdad-45ad-a06a-0c5881a96843','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by application)','The Open Programme'),(5,99,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','c44b8ea4-6ed3-4bc5-bb31-0aa24ec15c76','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','Special Initiatives'),(6,100,1,'2019-12-12 19:03:40','2019-12-12 19:03:40','3a0dd747-e191-44c4-b798-c093b5ac30df','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','The Anchor Programme'),(7,109,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','a45173ff-1ea5-41e0-9de6-1967f2cfb506','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by application)','The Open Programme'),(8,110,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','c4059024-5497-4474-85c6-5f6517dd55d9','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','Special Initiatives'),(9,111,1,'2019-12-12 19:05:20','2019-12-12 19:05:20','4efa4ce1-317a-4649-a4c5-c98182a508d4','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','The Anchor Programme'),(10,121,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','f68926ed-59b0-49d6-875f-bf41c7974bd3','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by application)','The Open Programme'),(11,122,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','82ba9075-91d9-42de-a644-58b58c8b0a5f','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','Special Initiatives'),(12,123,1,'2019-12-12 19:05:53','2019-12-12 19:05:53','42c02ca4-f80b-4990-aeec-4f9e1599ac05','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','The Anchor Programme'),(13,133,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','aa2a9712-f048-404b-b0f6-79f1691cbdeb','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by application)','The Open Programme'),(14,134,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','71940537-2eec-4805-8990-2bcea13e9671','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','Special Initiatives'),(15,135,1,'2019-12-16 11:57:28','2019-12-16 11:57:28','ca1e5955-c375-4fe4-8a26-9d8af6ce44fa','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','(by invitation)','The Anchor Programme');
/*!40000 ALTER TABLE `stc_9_programmes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `stc_footer`
--

LOCK TABLES `stc_footer` WRITE;
/*!40000 ALTER TABLE `stc_footer` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `stc_footer` VALUES (1,65,1,'2019-09-26 17:19:47','2019-12-13 15:23:35','5fed7e7d-ee8d-47bd-a84f-e6ee05a3f038','<p>The A B Charitable Trust <br />c/o Woodsford 3rd Floor <br />8 Bloomsbury St <br />London WC1B 3SR</p>','<ul>						<li>T. <a href=\"tel:020207243209486\">020 7243 9486</a></li>						<li>E. <a href=\"mailto:\">mail@abcharitabletrust.org.uk</a></li>					</ul><p>Registered charity number 100147</p>');
/*!40000 ALTER TABLE `stc_footer` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `structureelements`
--

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `structureelements` VALUES (1,1,NULL,1,1,60,0,'2019-09-25 14:23:47','2019-10-17 10:36:53','0b517e9f-cdb3-4742-b0d0-137b4677188f'),(3,1,13,1,2,11,1,'2019-09-25 14:23:53','2019-09-25 14:57:14','621b3ff6-aff8-4e5c-93c0-c4571a5c0212'),(4,1,14,1,12,13,1,'2019-09-25 14:23:53','2019-09-25 14:57:14','712209a2-1de2-44ed-97ca-d2fbbca9b20e'),(6,1,16,1,14,15,1,'2019-09-25 14:24:14','2019-09-25 14:57:14','ab81d804-7e35-48cf-ab11-50a6f0ec9eb9'),(7,1,17,1,16,17,1,'2019-09-25 14:24:14','2019-09-25 14:57:14','df81696d-e973-4b5f-b3ef-643a00de7bf8'),(8,1,18,1,18,19,1,'2019-09-25 14:24:20','2019-09-25 14:57:14','d4e4dde9-6fcd-4efc-a39e-b0d4d7dc390a'),(9,1,19,1,20,21,1,'2019-09-25 14:24:27','2019-09-25 14:57:14','2c45deb7-40bb-4cfe-8409-0c032154e816'),(11,1,21,1,22,23,1,'2019-09-25 14:24:35','2019-09-25 14:57:14','8c9dd764-b102-4361-9548-f400bc3e45e2'),(12,1,22,1,24,25,1,'2019-09-25 14:24:35','2019-09-25 14:57:14','71f875b8-a68c-42c1-870c-d54853e4bb86'),(14,1,24,1,26,39,1,'2019-09-25 14:24:51','2019-09-25 14:58:01','179c59fc-4b32-4f8d-a9cb-b716259ce82a'),(15,1,25,1,40,41,1,'2019-09-25 14:24:51','2019-09-25 14:58:01','3c396760-56ea-40ae-9d36-e55d270f067f'),(17,1,27,1,42,43,1,'2019-09-25 14:24:57','2019-09-25 14:58:01','9d043699-47a8-43d5-8e28-7793e5c32f89'),(18,1,28,1,44,45,1,'2019-09-25 14:24:57','2019-09-25 14:58:01','49662887-ff98-4a49-89d1-12004478fc2b'),(20,1,30,1,46,47,1,'2019-09-25 14:34:00','2019-09-25 14:58:01','b704b04c-5bda-40a0-8b3d-0a6f096a4767'),(21,1,31,1,48,49,1,'2019-09-25 14:34:00','2019-09-25 14:58:01','60ece34c-b597-4eff-858b-e95dbbaccfa8'),(23,1,33,1,50,51,1,'2019-09-25 14:34:05','2019-09-25 14:58:01','b52043c7-2548-40e0-8b86-b5494370f112'),(24,1,34,1,52,53,1,'2019-09-25 14:34:05','2019-09-25 14:58:01','744c3848-0864-4952-b5b4-f5cb841bb479'),(26,1,36,1,54,55,1,'2019-09-25 14:34:11','2019-09-25 14:58:01','84dfeac8-c5a8-4d53-b83e-55f72306afd4'),(27,1,37,1,56,57,1,'2019-09-25 14:34:11','2019-09-25 14:58:01','902c3f6a-911b-4eca-985f-2b054c5e617e'),(28,4,NULL,28,1,8,0,'2019-09-25 14:56:09','2019-09-25 14:56:21','abf508fd-eed3-4920-95cc-8ae2ba3baf62'),(29,4,38,28,2,3,1,'2019-09-25 14:56:09','2019-09-25 14:56:09','780e69c3-39ee-4805-95da-bcedd9a9f9d2'),(30,4,39,28,4,5,1,'2019-09-25 14:56:16','2019-09-25 14:56:16','801c12e0-98a6-422d-bc51-adf850ca344e'),(31,4,40,28,6,7,1,'2019-09-25 14:56:21','2019-09-25 14:56:21','14c7233e-ba90-4057-bbc4-4c5584dffa8e'),(33,1,42,1,3,4,2,'2019-09-25 14:57:05','2019-09-25 14:57:05','bdfef2b1-d3a7-4385-9f87-d8b596bfa4e6'),(34,1,43,1,5,6,2,'2019-09-25 14:57:05','2019-09-25 14:57:05','4e19e19b-ab83-4e82-bfaa-87a45c76d976'),(36,1,45,1,7,8,2,'2019-09-25 14:57:14','2019-09-25 14:57:14','8eafcd88-aa36-467a-86aa-2510b2eb2ee2'),(37,1,46,1,9,10,2,'2019-09-25 14:57:14','2019-09-25 14:57:14','121cadcd-9ea2-49d2-ace6-88d6a233a841'),(39,1,48,1,27,28,2,'2019-09-25 14:57:45','2019-09-25 14:57:45','3a1436da-8f6e-4728-a1cd-b0cb322bb6e3'),(40,1,49,1,29,30,2,'2019-09-25 14:57:45','2019-09-25 14:57:45','84625671-0685-4734-af9e-79849639aeed'),(42,1,51,1,31,32,2,'2019-09-25 14:57:56','2019-09-25 14:57:56','2fff429a-a7dc-480c-9d7c-3ab5137f298f'),(43,1,52,1,33,34,2,'2019-09-25 14:57:56','2019-09-25 14:57:56','0e7049b5-2446-47be-8776-76940011d044'),(45,1,54,1,35,36,2,'2019-09-25 14:58:01','2019-09-25 14:58:01','2140d3eb-9308-4990-a95b-595396d523c2'),(46,1,55,1,37,38,2,'2019-09-25 14:58:01','2019-09-25 14:58:01','046c2603-bdd9-4f85-9488-a56448e5b5e9'),(47,2,NULL,47,1,14,0,'2019-09-26 17:16:46','2019-09-26 17:17:35','3046da9c-ff24-4835-8858-c57c7f7488fa'),(48,2,56,47,2,3,1,'2019-09-26 17:16:46','2019-09-26 17:16:46','90bb8c0d-be41-4c72-9ca6-d83916da3959'),(49,2,57,47,4,5,1,'2019-09-26 17:17:00','2019-09-26 17:17:00','b2b0f351-0f10-4996-b4fc-9fc04763e8c8'),(50,2,58,47,12,13,1,'2019-09-26 17:17:00','2019-09-26 17:17:35','c544d37b-feeb-4d79-8bbb-9543fe02f6ef'),(51,2,59,47,8,9,1,'2019-09-26 17:17:01','2019-09-26 17:17:33','d958a94a-f4a9-4ab0-8c9c-b1a3cd85e4e9'),(52,2,60,47,10,11,1,'2019-09-26 17:17:01','2019-09-26 17:17:35','08cebc95-cf99-475c-9987-9dc2adddbf49'),(53,2,61,47,6,7,1,'2019-09-26 17:17:01','2019-09-26 17:17:24','a2d0db15-dc6d-45cf-a982-f08bcd7c75bc'),(54,3,NULL,54,1,8,0,'2019-09-26 17:17:59','2019-09-26 17:18:00','bbd6491f-73e5-42a2-a576-f40c3ddf2d73'),(55,3,62,54,2,3,1,'2019-09-26 17:17:59','2019-09-26 17:17:59','2b981112-b782-4fbe-84bc-d3fdd6af596c'),(56,3,63,54,4,5,1,'2019-09-26 17:18:00','2019-09-26 17:18:00','9a8dd559-e9f0-47ac-ae2b-44790feb0441'),(57,3,64,54,6,7,1,'2019-09-26 17:18:00','2019-09-26 17:18:00','edf94fae-da14-49fc-926c-82792eeb3f36'),(58,1,66,1,58,59,1,'2019-10-17 10:36:53','2019-10-17 10:36:53','6b07567f-4a71-4ffa-aca2-27918604b163');
/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `structures` VALUES (1,NULL,'2019-09-23 18:29:58','2019-09-23 18:29:58',NULL,'69cb8d4b-3a17-4386-a202-efe8a8080ac2'),(2,NULL,'2019-09-25 14:06:18','2019-09-25 14:06:18',NULL,'c7e6bf8f-64c8-4519-ab60-71b713e1b952'),(3,NULL,'2019-09-25 14:06:23','2019-09-25 14:06:23',NULL,'91a55e8a-5840-443a-945a-ca70b2c8fa55'),(4,NULL,'2019-09-25 14:35:12','2019-09-25 14:35:12',NULL,'c2e462ae-43d0-4166-877d-4d68ac77c369');
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `supertableblocks`
--

LOCK TABLES `supertableblocks` WRITE;
/*!40000 ALTER TABLE `supertableblocks` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `supertableblocks` VALUES (65,9,12,1,1,NULL,'2019-09-26 17:19:47','2019-09-26 17:19:47','b050e693-1fdc-4bb8-9561-e6a3ca998184'),(91,86,60,4,1,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','b4db2a8a-cbe8-42fa-af41-37e7d9872418'),(92,86,60,4,2,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','80190d26-8fa8-4f50-ac6d-296372028c4d'),(93,86,60,4,3,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','419078da-385f-408e-8cb1-1a8b8a07d2f5'),(98,97,60,4,1,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','7019f953-0d97-4b39-acb1-a76e7152df02'),(99,97,60,4,2,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','59ef2956-6bbe-48b3-8040-2c6dca1785dc'),(100,97,60,4,3,NULL,'2019-12-12 19:03:40','2019-12-12 19:03:40','e9fdca2b-de04-4f1b-b52a-22125be4813e'),(102,101,44,3,1,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','165f117b-b0e8-4d25-b4ad-3a53cf02f89a'),(103,101,44,3,2,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','f0a576ae-e417-4f4b-b742-28217d07ee05'),(104,101,44,3,3,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','dde1908d-80ed-4b64-8653-a934d1337c95'),(109,108,60,4,1,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','f19dedda-03a8-4a40-b687-ec581789259f'),(110,108,60,4,2,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','0050b362-0fb3-4513-a42f-fc7a8175b9cd'),(111,108,60,4,3,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','d5c991fa-f93a-4773-ae26-62529caaa9ac'),(113,112,44,3,1,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','ace7f3c7-eeac-48cb-8f88-017b213b2ccf'),(114,112,44,3,2,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','f886e6de-40e1-4a29-b4ec-0a3ef4015051'),(115,112,44,3,3,NULL,'2019-12-12 19:05:20','2019-12-12 19:05:20','f4e64f7e-083b-4483-afd9-1f467b0b4be3'),(121,120,60,4,1,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','06a90491-0a8c-424c-8d2a-b775a0c54fab'),(122,120,60,4,2,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','a1a6c7de-b512-4590-9978-d8e00d7e7bf4'),(123,120,60,4,3,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','6d0613f9-4474-4716-8337-4fea7d452a1c'),(125,124,44,3,1,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','fe5e8bc3-03bc-44f1-b412-523b927fc9bc'),(126,124,44,3,2,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','a48cda67-caa1-4a7f-9d7a-1e1163808b6e'),(127,124,44,3,3,NULL,'2019-12-12 19:05:53','2019-12-12 19:05:53','e34ec500-fd70-45ac-a793-04bec1c409ec'),(133,132,60,4,1,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','bd76f507-697a-4ed7-abc9-df22a703acbc'),(134,132,60,4,2,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','c8ac369c-f4c5-4bf0-ba8c-4c5139d742c8'),(135,132,60,4,3,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','fbe86890-c2ce-4467-a97e-fd0d2348dcf1'),(137,136,44,3,1,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','5e4e2495-5237-4171-99bc-5512fe1a4ff5'),(138,136,44,3,2,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','9d0b217e-2c6c-4ff5-8f31-8ffb820086a4'),(139,136,44,3,3,NULL,'2019-12-16 11:57:28','2019-12-16 11:57:28','0d61f4f0-71b2-40b6-bf7d-908d3d95e465');
/*!40000 ALTER TABLE `supertableblocks` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `supertableblocktypes`
--

LOCK TABLES `supertableblocktypes` WRITE;
/*!40000 ALTER TABLE `supertableblocktypes` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `supertableblocktypes` VALUES (1,12,7,'2019-09-25 14:09:12','2019-09-25 14:09:12','8d579288-0fa0-4b9d-a72e-8d1f70b14ea9'),(2,21,11,'2019-09-26 17:23:45','2019-09-26 17:23:45','8d96cd19-0c12-417d-92fc-7e564c84cf4f'),(3,44,20,'2019-12-12 18:51:05','2019-12-12 18:51:05','65bc918b-04dd-494d-a5ea-4895e9df9c33'),(4,60,23,'2019-12-12 18:59:24','2019-12-12 18:59:24','068d7fcc-4fa4-457a-8265-5a8bee5142ff');
/*!40000 ALTER TABLE `supertableblocktypes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `systemmessages`
--

LOCK TABLES `systemmessages` WRITE;
/*!40000 ALTER TABLE `systemmessages` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `systemmessages` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `taggroups`
--

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `usergroups_users`
--

LOCK TABLES `usergroups_users` WRITE;
/*!40000 ALTER TABLE `usergroups_users` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `usergroups_users` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `userpermissions`
--

LOCK TABLES `userpermissions` WRITE;
/*!40000 ALTER TABLE `userpermissions` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `userpermissions` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `userpermissions_usergroups`
--

LOCK TABLES `userpermissions_usergroups` WRITE;
/*!40000 ALTER TABLE `userpermissions_usergroups` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `userpermissions_usergroups` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `userpermissions_users`
--

LOCK TABLES `userpermissions_users` WRITE;
/*!40000 ALTER TABLE `userpermissions_users` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `userpermissions_users` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `userpreferences`
--

LOCK TABLES `userpreferences` WRITE;
/*!40000 ALTER TABLE `userpreferences` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `userpreferences` VALUES (1,'{\"language\":\"en\",\"weekStartDay\":\"1\",\"enableDebugToolbarForSite\":false,\"enableDebugToolbarForCp\":false,\"showExceptionView\":false}');
/*!40000 ALTER TABLE `userpreferences` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `users` VALUES (1,'webmaster',11,'','','sebastian.jordan@gmail.com','$2y$13$v6gqQdhYNEBoMl33qm7MZ.FdP2Da5VK3mkFDiLztALKCj0sW6IT9a',1,0,0,0,'2020-02-10 19:28:01',NULL,NULL,NULL,'2019-09-27 11:19:45',NULL,1,NULL,NULL,NULL,0,'2019-09-25 13:42:31','2019-09-23 17:41:03','2020-02-10 19:28:01','b77b18ec-00a1-43ae-8336-04651108734c');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `volumefolders`
--

LOCK TABLES `volumefolders` WRITE;
/*!40000 ALTER TABLE `volumefolders` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `volumefolders` VALUES (1,NULL,NULL,'Temporary source',NULL,'2019-09-23 18:35:58','2019-09-23 18:35:58','356f7862-05bb-4322-b817-204b52d0c843'),(2,1,NULL,'user_1','user_1/','2019-09-23 18:35:58','2019-09-23 18:35:58','d511350c-6535-4e61-a0b5-75738039b2f5'),(3,NULL,1,'Uploads','','2019-09-23 18:37:52','2019-09-23 18:37:52','7b4a3dfe-3f63-4053-8bb9-214474eb36e2'),(4,3,1,'sharing','sharing/','2019-09-25 14:11:06','2019-09-25 14:11:06','d39baf28-5c57-472e-92e2-d113e1b597f8');
/*!40000 ALTER TABLE `volumefolders` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `volumes` VALUES (1,NULL,'Uploads','uploads','craft\\volumes\\Local',1,'@baseUrl/uploads/','{\"path\":\"@basePath/uploads/\"}',1,'2019-09-23 18:37:52','2019-09-23 18:37:52',NULL,'d6433ca4-70e0-4884-9992-cac1270c71a7');
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `widgets` VALUES (1,1,'craft\\widgets\\RecentEntries',1,NULL,'{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}',1,'2019-09-23 17:44:43','2019-09-23 17:44:43','c77e6281-2751-47ea-9d8d-5199ec16c149'),(2,1,'craft\\widgets\\CraftSupport',2,NULL,'[]',1,'2019-09-23 17:44:43','2019-09-23 17:44:43','22893293-8506-4f77-bc81-faa6df94e094'),(3,1,'craft\\widgets\\Updates',3,NULL,'[]',1,'2019-09-23 17:44:43','2019-09-23 17:44:43','679e9e16-4b7e-4008-9412-262f5c3c07b5'),(4,1,'craft\\widgets\\Feed',4,NULL,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2019-09-23 17:44:43','2019-09-23 17:44:43','59dc5b53-54dc-4ceb-b155-a526afe918bd');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'aoSvDcEU'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-10 19:31:03
